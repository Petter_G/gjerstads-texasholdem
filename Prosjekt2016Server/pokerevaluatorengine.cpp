#include "protocolenums.h"
#include "pokerevaluatorengine.h"
#include "exceptions/communityemptyexception.h"

#include <iostream>
#include <algorithm>
#include <string>
#include <set>

using namespace std;

ScoreObject PokerEvaluatorEngine::evaluateHand(std::vector<card> &community, playerRepresentation& player)
{
    //returverdien selv
    ScoreObject score(player);

    //hjelpevariabeler som må instansieres.
    royalflush = false;
    straightflush = false;
    straight = false;

    vector<card>& playerHand = player.getHand();

    //lagrer combinedHand i class-scope, da det gjør ting enkelt for øvrige funksjoner. Må huske på å rense mellom hver bruk av evaluateHand.
    combinedHand.clear();
    bestHand.clear();

    combinedHand.reserve( community.size() + playerHand.size() );
    combinedHand.insert( combinedHand.end(), community.begin(),community.end() );
    combinedHand.insert( combinedHand.end(), playerHand.begin(),playerHand.end() );

    //sorterer den kombinerte hånden. Blir nyttig ved deteksjon av straights.
    std::sort(combinedHand.begin(),combinedHand.end(),
        [](const card& a, const card& b) -> bool
        {
            return a.getValue() < b.getValue();
        });

    if(community.size() >= 3)//royalstraightflushanalyser er ikke designet for det ekstreme tilfelle hvor alle går allin allerede i preflop.
        RoyalStraightFlushAnalyzer();

    //returverdier, vi må her avgjøre om vi skal gjøre testene under eller ikke, dvs hvis vi har RoyalFlush eller StraightFlush, er det ingen vits med noe under.
    if (royalflush)
    {
        score.setPrimary(ROYALFLUSH);
        return score;
    }
    else if (straightflush)
    {
        score.setPrimary(STRAIGHTFLUSH);
        score.setSecondary(bestHand[4].getValue() );
        return score;
    }
    //ok, de neste to før flush må nå testes.
    four = fourCheck();
    if (four)
    {
        score.setPrimary(FOUROFAKIND);
        score.setSecondary(secondary);
        score.setTertiary(tertiary);
        return score;
    }
    house = houseCheck();
    if (house)
    {
        score.setPrimary(HOUSE);
        score.setSecondary(secondary);
        score.setTertiary(tertiary);
        return score;
    }
    //neste, flush, er alt satt
    flush = rawFlushCheck();
    if (flush)
    {
        score.setPrimary(FLUSH);
        score.setSecondary(combinedHand[flushHighCardIndex].getValue() );
        return score;
    }
    //samme med straight
    if (straight)
    {
        score.setPrimary(STRAIGHT);
        if (bestHand[4].getValue() == ACELOW )
            score.setSecondary(ACEHIGH);
        else
            score.setSecondary(bestHand[4].getValue() );
        return score;
    }

    three = threeCheck();
    if (three)
    {
        score.setPrimary(THREEOFAKIND);
        score.setSecondary(secondary);
        score.setTertiary(tertiary);
        score.setQuaternary(quaternary);
        return score;
    }
    dpair = dpairCheck();
    if (dpair)
    {
        score.setPrimary(TWOPAIR);
        score.setSecondary(secondary);
        score.setTertiary(tertiary);
        score.setQuaternary(quaternary);
        return score;
    }
    pair = pairCheck();
    if (pair)
    {
        score.setPrimary(PAIR);
        score.setSecondary(secondary);
        score.setTertiary(tertiary);
        return score;
    }
    //elendig hånd hvis vi kom så langt.
    highcardDeterminator();
    score.setPrimary(HIGHCARD);
    score.setSecondary(secondary);
    return score;
}

bool PokerEvaluatorEngine::rawFlushCheck()
{
    //antallet kort med samme suit i hver suit type.
    short suitcount[4];
    //index til det til nå høyeste kortet av hver type.
    short suitHighIndex[4];
    //må huske å initialisere, ellers får vi noe udefinert.
    std::fill(suitcount,suitcount+4,0);
    std::fill(suitHighIndex,suitHighIndex+4,0);

    for (uint i = 0;i<combinedHand.size();++i)
    {

        int suit = combinedHand[i].getType();
        switch (suit)
        {
            case CLUBS: suitcount[CLUBS]++;
                    suitHighIndex[CLUBS]=i;
                    break;
            case DIAMONDS: suitcount[DIAMONDS]++;
                    suitHighIndex[DIAMONDS]=i;
                    break;
            case HEARTS: suitcount[HEARTS]++;
                    suitHighIndex[HEARTS] = i;
                    break;
            case SPADES: suitcount[SPADES]++;
                    suitHighIndex[SPADES]=i;
                    break;
        }
    }

    //finne ut om den sorten med flest kort faktisk er fem eller høyere.
    if (suitcount[CLUBS] >=5)
    {
        flushHighCardIndex=suitHighIndex[CLUBS];
        return true;
    }
    else if (suitcount[DIAMONDS] >=5)
    {
        flushHighCardIndex=suitHighIndex[DIAMONDS];
        return true;
    }
    else if (suitcount[HEARTS] >=5)
    {
        flushHighCardIndex=suitHighIndex[HEARTS];
        return true;
    }
    else if (suitcount[SPADES] >=5)
    {
        flushHighCardIndex=suitHighIndex[SPADES];
        return true;
    }
    else
        return false;
}

bool PokerEvaluatorEngine::flushCheck(std::vector<card> hand)
{
    if (hand.size() != 5)
    {
        cout<<"Error: FlushCheck(), size of hand is nonsensical(size is: "<<hand.size()<<"."<<endl;
        for(auto& card : hand)
            cout<<"  |"<<card.describeValue()<<" "<<card.describeType()<<"|";

        cout<<endl;
        abort();
    }

    for(uint i = 1;i<hand.size();i++)
    {
        if (hand[i].getType() != hand[i-1].getType() )
            return false;
    }
    return true;
}

//Erstatter en rekke tidligere funksjoner, sjekker hvorvidt du har en royalflush, en straightflush, og straight,
//merker tilhørende flag deretter.
void PokerEvaluatorEngine::RoyalStraightFlushAnalyzer()
{
    //må settes til noe som markerer at vi ikke har noen straight by default.
    straightHighcardindex= -1;
    straightflush = false;

    //brukes til å holde øye med eventuelle ess i combinedHand, uten å endre noen andre vektorer.
    vector<card*> acePTR;

    //Greit å jobbe på en kopi her.
    vector<card> CHCopy;
    CHCopy.assign(combinedHand.begin(),combinedHand.end() );

    //vector av subsett av inkrementerende rekker.
    //F.eks si en kombinert hånd består av h2,h3,s6,s7,s9,s10,s11, da vil subsettene være: [h2,h3],[s6,s7],[s9,s10,s11].
    vector<vector<card> > subsets;
    vector<card> temp;

    //før vi fortsetter må vi vite hvor mange, og hvilke, ess vi har.
    for (auto& card : CHCopy)
    {
        if(card.getValue() == ACELOW)//hvis ett ess er tilstede, legg til en peker til denne i vår ess vector.
            acePTR.push_back(&card);
    }

    //Vi må nå løpe igjennom vektoren på jakt etter inkrementerende rekker, og legge disse i subsets.
    for (uint i = 0; i< CHCopy.size()-1;i++)
    {
        if (CHCopy[i].getValue()+1 == CHCopy[i+1].getValue() )//altså er neste kort ett hakk større.
        {
            if(temp.size() == 0)
            {
                temp.push_back(CHCopy[i]);
                temp.push_back(CHCopy[i+1]);
            }
            else
                temp.push_back(CHCopy[i+1]);
        }
        else if (CHCopy[i].getValue() == CHCopy[i+1].getValue() )
        {
            if(temp.size() == 0)
            {
                temp.push_back(CHCopy[i]);
                temp.push_back(CHCopy[i+1]);
            }
            else
                temp.push_back(CHCopy[i+1]);
        }
        else if( CHCopy[i].getValue() != CHCopy.at(i+1).getValue() )//subsettet er ferdig, legg til.
        {
            if(temp.size() != 0)
            {
                subsets.push_back(temp);
                temp.clear();
                temp.push_back(CHCopy[i+1]);
            }
            else
            {
                temp.push_back(CHCopy[i]);
                subsets.push_back(temp);
                temp.clear();
            }
        }
    }
    subsets.push_back(temp);

    //Vi har nå ett eller flere subsett, nå får vi se om det er noe brukbart her.
    for(auto& subset : subsets)
    {
        cout<<"subset: ";
        for(auto& card : subset)
        {
            cout<<"|"<<card.describeType()<<"   "<<card.describeValue()<<"|    ";
        }
        cout<<endl;

        vector<card> uniqueSet;
        uniqueSet.assign(subset.begin(),subset.end() );
        uniqueSet.erase( unique(uniqueSet.begin(),uniqueSet.end(),
                                [] (card& a, card& b) ->bool
                                {
                                    return a.getValue()==b.getValue();
                                }),uniqueSet.end() );

        short uniqueOffset = uniqueSet.size()-5;

        //Hvis vi har har 10,J,Q,K, lurer vi på om vi også har et ess, i så fall har vi en royalstraight, dvs en god straight.
        if (uniqueSet[1+uniqueOffset].getValue() == TEN && uniqueSet[2+uniqueOffset].getValue()==JACK
             && uniqueSet[3+uniqueOffset].getValue()==QUEEN && uniqueSet[4+uniqueOffset].getValue()==KING && acePTR.size() != 0)
        {
            cout<<"IF"<<endl;
            straight=true;
            //Vi må sjekke om vi har noe enda bedre, dvs en royalflush.
            for(auto ptr : acePTR)
            {
                bestHand.assign(subset.end()-4,subset.end() );
                bestHand.push_back(*ptr);

                royalflush = flushCheck(bestHand);

                if (royalflush)//hvis vi alt vet at vi har en royalflush, gidder vi ikke mer.
                    return;
            }

        }
        //Her ser vi etter straightflush, eller vanlige straights.
        //Vi vet at eventuelle spesialtilstander av typen royalflush eller royalstraight alt er fanget opp, altså ess kan behandles som en 1er.
        else if (uniqueSet.size() >= 5)
        {
            cout<<"ELSE"<<endl;
            //kan tenkes subsettet består av 5,6 eller 7 kort, vi må sjekke alle kombininasjoner
            //da ingen, en, eller flere også kan være gyldige straightflusher.
            const short BESTHANDSIZE = 5;
            short SubsubsetCount = uniqueSet.size()-BESTHANDSIZE;//antallet mulige flusher og straighter i en.

            straight = true; //vet dette med sikkerhet siden subset er organisert stigende, inkrementert med 1 for hvert ledd.
            bestHand.assign(uniqueSet.end()-5,uniqueSet.end() );//vil være den beste straighten, men ikke nødvendigvis noe mer.

            vector<card> copy;//(uniqueSet.begin(),uniqueSet.end());

            //nå må vi se om vi har noe bedre enn en straight, dvs straightflush.
            for(int i = SubsubsetCount; i >= 0;i--)
            {
                copy.assign(uniqueSet.begin()+i,uniqueSet.begin()+i+5);

                straightflush = flushCheck(copy);
                if (straightflush)//Oppdater bestHand, og returner.
                {
                    bestHand.assign( uniqueSet.begin()+i,uniqueSet.begin()+i+5 );
                    return;
                }
            }

        }
    }
}


//sjekker om du har fire like, igjen,siden combinedHand er sortert stigende, får vi det lettere.
bool PokerEvaluatorEngine::fourCheck()
{
    short count = 1;
    short value = 0;

    for (uint i=1;i<combinedHand.size();++i)
    {
        if (combinedHand[i-1].getValue() == combinedHand[i].getValue())
        {
            ++count;
            value = combinedHand[i].getValue();

            if (count == 4)
                break;
        }
        else
        {
            count = 1;
        }
    }
    if (count == 4)
    {
        if (value == ACELOW)
            secondary = ACEHIGH;
        else
            secondary=value;

        vector<card>::iterator it = combinedHand.end()-5;
        tertiary=(*it).getValue();
        return true;
    }
    else
        return false;
}

//ser først etter største tripps, så ser den etter mest verdifulle par.
bool PokerEvaluatorEngine::houseCheck()
{
    short trippscount= 1;
    short trippsvalue=0;
    short paircount= 1;
    short pairvalue=0;

    //finn største trips.
    for (int i=combinedHand.size()-1; i > 0;--i)
    {
        if (combinedHand[i].getValue()  == combinedHand[i-1].getValue())
        {
            ++trippscount;
            trippsvalue=combinedHand[i-1].getValue();
            if (trippscount == 3)
                break;
        }
        else
        {
            trippscount = 1;
        }
    }
    //ny løkke, ser nå etter mest verdifulle par.
    for (int i=combinedHand.size()-1; i > 0;--i)
    {
        if ( combinedHand[i].getValue()  == combinedHand[i-1].getValue()  &&  combinedHand[i].getValue() != trippsvalue )
        {
            ++paircount;
            pairvalue=combinedHand[i-1].getValue();
            if (paircount == 2)
                break;
        }
        else
        {
            pairvalue = 1;
        }
    }
    //hvis trippscount og paircount != henholdsvis 3 og 2, return true;
    if (trippscount == 3 && paircount == 2)
    {
        if (trippsvalue == ACELOW)
            secondary=ACEHIGH;
        else
            secondary=trippsvalue;

        if (pairvalue == ACELOW)
            tertiary = ACEHIGH;
        else
            tertiary = pairvalue;

        return true;
    }
    else return false;
}

//nærmest en kopi av house
bool PokerEvaluatorEngine::threeCheck()
{
    short count = 1;
    short value = 0;
    for (int i=combinedHand.size()-1; i > 0;--i)
    {
        if (combinedHand[i].getValue() ==combinedHand[i-1].getValue())
        {
            ++count;
            value= combinedHand[i].getValue();

            if (count == 3)
                break;
        }
        else
        {
            count = 1;
        }
    }
    if (count == 3)
    {
        if (value == ACELOW)
            secondary=ACEHIGH;
        else
            secondary=value;

        //for å få tak i tertiary og quaternary info, trenger vi litt hjelp
        vector<card> help(combinedHand.begin(),combinedHand.end());
        auto it = help.begin();
        //let igjennom combinedhand kopi, finn verdien som er de tre like, og fjern dem.
        for (int i = 0;i<3;i++)
        {
            it = find_if(help.begin(),help.end(),[=]( card& obj) -> bool
            {
                return obj.getValue() == value;
            });

            if (it == help.end())
                break;
            else
                help.erase(it);

        }
        //so far so good, siste verdi er da håndens største, og dermed tertiary.
        it = --help.end();
        tertiary= (*it).getValue();

        //samme historie med tertiary, finn og fjern.
        it = help.begin();
        while (it != help.end() )
        {
            it = find_if(help.begin(),help.end(),[=]( card& obj) -> bool
            {
                return obj.getValue() == tertiary;
            });
            help.erase(it);
        }

        //igjen, siste verdi er nå håndens største og dermed quaternary.
        it = --help.end();
        quaternary= (*it).getValue();
        return true;
    }
    else
        return false;
}

//nærmest en kopi av house.
bool PokerEvaluatorEngine::dpairCheck()
{
    short paircount1 = 1;
    short pairvalue1 = 0;
    short paircount2 = 1;
    short pairvalue2 = 0;

    //finn største par
    for (int i=combinedHand.size()-1; i > 0;--i)
    {
        if (combinedHand[i].getValue()  == combinedHand[i-1].getValue())
        {
            ++paircount1;
            pairvalue1=combinedHand[i-1].getValue();
            if (paircount1 == 2)
                break;
        }
        else
        {
            paircount1 = 1;
        }
    }
    //ny løkke, ser nå etter nest mest verdifulle par.
    for (int i=combinedHand.size()-1; i > 0;--i)
    {
        if ( combinedHand[i].getValue()  == combinedHand[i-1].getValue()  &&  combinedHand[i].getValue() != pairvalue1 )
        {
            ++paircount2;
            pairvalue2=combinedHand[i-1].getValue();
            if (paircount2 == 2)
                break;
        }
        else
        {
            paircount2 = 1;
        }
    }

    //hvis trippscount og paircount != henholdsvis 3 og 2, return true;
    if (paircount1 == 2 && paircount2 == 2)
    {
        if (pairvalue1 == ACELOW)
            secondary=ACEHIGH;
        else
            secondary=pairvalue1;

        tertiary=pairvalue2;

        //Vi må og sette quaternary, hvis det blir uavgjort på to par, brukes høyeste gjenværende kort som kicker.
        vector<card> help(combinedHand.begin(),combinedHand.end());
        auto it = help.begin();
        //let igjennom kopien, finn og fjern begge parene.

        while (it != help.end() )
        {
            it = find_if(help.begin(),help.end(),[=]( card& obj) -> bool
            {
                return obj.getValue() == secondary || obj.getValue() == tertiary;
            });
            if (it == help.end())
                break;

            help.erase(it);
        }
        //høyeste kort er nå quaternary, og kicker for to par.
        it = --help.end();
        quaternary = it->getValue();
        return true;

    }
    else return false;
}

//Finner høyeste par
bool PokerEvaluatorEngine::pairCheck()
{
    ushort paircount = 1;
    ushort pairvalue = 0;

    for (int i=combinedHand.size()-1; i > 0;--i)
    {
        if (combinedHand[i].getValue()  == combinedHand[i-1].getValue())
        {
            ++paircount;
            pairvalue=combinedHand[i-1].getValue();

            if (paircount == 2)
                break;
        }
        else
        {
            paircount = 1;
        }
    }

    //hvis parcount = 2, true
    if (paircount == 2)
    {
        if (pairvalue == ACELOW)
            secondary = ACEHIGH;

        secondary = pairvalue;

        //Vi trenger en kicker, her vil det si en tertiary
        vector<card> help(combinedHand.begin(),combinedHand.end());
        auto it = help.begin();

        //let igjennom kopi, finn alle instanser av pairvalue, fjern dem.
        while (it != help.end())
        {
            it = find_if(help.begin(),help.end(),[=] (card& obj) -> bool
            {
                return obj.getValue() == pairvalue;
            });

            if (it == help.end())
                break;
            help.erase(it);
        }
        //høyeste kort er nå kicker, altså tertiary
        it = --help.end();
        tertiary = it->getValue();

        return true;
    }
    else return false;
}

//elementært nok, gir den største verdien.
void PokerEvaluatorEngine::highcardDeterminator()
{
    short highcard = combinedHand[combinedHand.size()-1].getValue();
    //sjekk for ess
    if (combinedHand[0].getValue() == ACELOW)
        secondary = ACEHIGH;

    else
        secondary = highcard;

}

std::vector<ScoreObject*> PokerEvaluatorEngine::determineWinners(std::vector<playerRepresentation*>& contenders, std::vector<card>& communityCards)
{
    //there is a unique case where this function is called with no community cards currently on the table, this can happen if a
    //player leaves a table that never got initalized due to halt state. This happens if a player leaves a game that didn't get play a single round.
    if (communityCards.size() == 0)
        throw CommunityEmptyException();

    vector<ScoreObject> handData;//Istedet for vector<vector<shorts>>

    for(auto* player : contenders)
    {
        ScoreObject data = evaluateHand(communityCards,*player);
        handData.push_back(data);
    }

    //Da får vi sammenligne verdier for å se hvem som vant.
    ushort highestPrimary = 0;
    vector<ScoreObject*> WinnerByPrimary;
    for (uint i = 0;i< handData.size();i++)
    {
        ScoreObject& score = handData[i];
        if (score.getPrimary() > highestPrimary )
        {
            WinnerByPrimary.clear();
            highestPrimary = score.getPrimary();
            WinnerByPrimary.push_back( &score );
        }
        else if (score.getPrimary() == highestPrimary)
            WinnerByPrimary.push_back( &score );
    }

    //Hvis høyeste primary kun er representert blant en spiller, har vi allerede nå vinneren.
    if (WinnerByPrimary.size() == 1 )
        return WinnerByPrimary;

    //Hvis size ikke er 1 kan det likevel hende at secondary er uinteressant, hvis denne er 0.
    if (WinnerByPrimary[0]->getPrimary() == 0 )
        return WinnerByPrimary;

    //Hvis ikke, må vi fortsette.
    ushort highestSecondary = 0;
    vector<ScoreObject*> WinnerBySecondary;
    for (auto* score : WinnerByPrimary)
    {
        if (score->getSecondary() > highestSecondary)
        {
            WinnerBySecondary.clear();
            highestSecondary = score->getSecondary();
            WinnerBySecondary.push_back(score);
        }
        else if (score->getSecondary() == highestSecondary)
            WinnerBySecondary.push_back(score);
    }

    if (WinnerBySecondary.size() == 1)
        return WinnerBySecondary;

    if (WinnerBySecondary[0]->getTertiary() == 0 )
        return WinnerBySecondary;

    ushort highestTertiary = 0;
    vector<ScoreObject*> WinnerByTertiary;
    for (auto* score : WinnerByPrimary)
    {
        if (score->getTertiary() > highestTertiary )
        {
            WinnerByTertiary.clear();
            highestTertiary = score->getTertiary();
            WinnerByTertiary.push_back(score);
        }
        else if (score->getTertiary() == highestTertiary)
            WinnerByTertiary.push_back(score);
    }

    if (WinnerByTertiary.size() == 1 )
        return WinnerByTertiary;

    if (WinnerByTertiary[0]->getQuaternary() == 0 )
        return WinnerByTertiary;

    ushort highestQuaternary = 0;
    vector<ScoreObject*> WinnerByQuaternary;
    for (auto* score : WinnerByPrimary)
    {
        if (score->getQuaternary() > highestQuaternary )
        {
            WinnerByQuaternary.clear();
            highestQuaternary = score->getQuaternary();
            WinnerByQuaternary.push_back(score);
        }
        else if (score->getQuaternary() == highestQuaternary)
            WinnerByQuaternary.push_back(score);
    }

    if (WinnerByQuaternary.size() == 1)
        return WinnerByQuaternary;

    if (WinnerByQuaternary[0]->getQuaternary() == 0)
        return WinnerByQuaternary;

    //kom vi hit, har noe gått galt, dump debug data
    cerr<<"Error in determineWinners, no winners were determined. Dumping data.\n";
    cerr<<"HighestPrimary: "<<highestPrimary<<"  HighestSecondary: "<<highestSecondary;
    cerr<<"  HighestTertiary: "<<highestTertiary<<"  HighestQuaternary: "<<highestQuaternary<<endl;

    cerr<<"Now dumping communitycards"<<endl;
    for (int i = 0; i< communityCards.size();i++)
    {
        cerr<<"Type of card "<<i<<": "<<communityCards[i].getType()<<"  Value: "<<communityCards[i].getValue()<<endl;
    }
    cerr<<"Now dumping contenders hands: \n";
    for(auto* contender : contenders)
    {
        std::vector<card>& hand = contender->getHand();
        cerr<<"holecards: Type of left card: "<<hand[0].getType()<<"   Value: "<<hand[0].getValue()<<"   ";
        cerr<<"Type of right card 2: "<<hand[1].getType()<<"   Value: "<<hand[1].getValue()<<endl;
    }
    abort();

}

/*int main()
{
    PokerEvaluatorEngine eng;

    vector<card> com;
    vector<short> points;
    com.push_back(card(1,2) );
    com.push_back(card(3,2) );
    com.push_back(card(2,11) );
    com.push_back(card(1,6) );
    com.push_back(card(2,10) );

    vector<playerRepresentation*> players;
    playerRepresentation test1(3,100);
    playerRepresentation test2(4,101);

    test1.addCard(card(2,3));
    test1.addCard(card(2,3));

    test2.addCard(card(2,1));
    test2.addCard(card(2,3));

    players.push_back( &test1);
    players.push_back( &test2);

    vector<ScoreObject*> results= eng.determineWinners(players,com);

    //vector<short> svar = eng.evaluateHand(com,test1.getHand() );
    //cout<<"svar ist: "<<svar[0]<<" "<<svar[1]<<"  "<<svar[2]<<"  "<<svar[3]<<endl;
    cout<<"sizeof results: "<<results.size()<<endl;
    for(auto& winner : results)
    {
        pointsContainer* data = winner->getPointsPTR();
        cout<<"players data are: "<<data->primary<<"  "<<data->secondary<<"  "<<data->tertiary<<"  "<<data->quaternary<<endl;
    }

    cout<<"printing communityhand:"<<endl;
    for(auto& card : com)
    {
        cout<<"|"<<card.describeType()<<"   "<<card.describeValue()<<"|    ";
    }
    cout<<endl;
    cout<<"printing score:"<<endl;

    cout<<"Primary: "<<points[0]<<"\tSecondary: "<<points[1]<<"\tTertiary: "<<points[2]<<"\tQuaternary: "<<points[3]<<endl;
}*/
