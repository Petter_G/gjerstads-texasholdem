#ifndef CARD_H
#define CARD_H

#include <string>

#include "protocolenums.h"

class card
{
    short type;
    short value;

public:
    card(short typeinput,short valueinput);
    short getValue() const;
    short getType();
    std::string describeType();
    std::string describeValue();

    //ny, gjør det mulig å bruke card i std::set
    friend bool operator<(card lhs,card rhs)
    {
        return lhs.getValue() < rhs.getValue();
    };
};

#endif
