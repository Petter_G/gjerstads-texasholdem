#include "card.h"

#include <string>
#include <stdexcept>
#include <iostream>
#include <sstream>

using namespace std;


    short type;										//0=kløver,1=ruter,2=hjerter,3=spar;
    short value;									//1=ace, 13=konge.

    card::card(short typeinput,short valueinput)
    {
        type = typeinput;
        value = valueinput;
    }

    short card::getValue() const
    {
        return value;
    }

    short card::getType()
    {
        return type;
    }

    string card::describeType()
    {
        //stygt? ja visst.
        const char heart[] ="\xe2\x99\xa5";
        const char diamonds[] ="\xe2\x99\xa6";
        const char spades[] ="\xe2\x99\xa4";
        const char clubs[] ="\xe2\x99\xa2";
        if (type == 0)
            return clubs;
        else if (type == 1)
            return diamonds;
        else if (type == 2)
            return heart;
        else if (type == 3)
            return spades;
        else//fatal error
        {
            cout<<"error: type<"<<type<<"> is NOT defined, investigate immediately. Shutting down."<<endl;
            exit(1);
        }
    }

    string card::describeValue()
    {
        switch(value)
        {
        case 1:
            return "A";
            break;
        case 2:
            return "2";
            break;
        case 3:
            return "3";
            break;
        case 4:
            return "4";
            break;
        case 5:
            return "5";
            break;
        case 6:
            return "6";
            break;
        case 7:
            return "7";
            break;
        case 8:
            return "8";
            break;
        case 9:
            return "9";
            break;
        case 10:
            return "10";
            break;
        case 11:
            return "J";
            break;
        case 12:
            return "Q";
            break;
        case 13:
            return "K";
            break;
        default:
            stringstream ss;
            ss << "Error: case \"" << value << "\" is not a valid card value."<<endl;
            throw std::runtime_error( ss.str() );
        }

    }
