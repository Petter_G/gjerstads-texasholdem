#ifdef _WIN32

//This implementation file is used solely for windows compilers, effectively MSVC on windows 10.
//This approach lets me keep the two potentially dramatically different implementations totally seperate.
//Originally I played around with a mixed file, ie both windows and unix code in the same file, using the preprocessor to seperate them. It became
//clear that this was a less than optimal approach.
#include "server.h"
#include "protocolenums.h"
#include "holdempokergame.h"
#include "profilemanager.h"
#include "configloader.h"

#include <mutex>
#include <algorithm>
#include <exception>

using namespace std;

Server::Server()
{
    //set up listeningport. Will as of this writing mean nothing more than the copying of the default port(ie 3500);
    designatedListeningPort = ServConst::DEFAULT_PORT;

    //fyller opp AvailableGameSlotStack
    for(short i = ServConst::MAXTABLES; i != 0; i--)
    {
        AvailableGameSlotStack.push(i);
    }

    //load config.cfg.
    cfg_args = ConfigLoading::loadConfig();

    //initialize();

    //trenger trådIDer, kan ordne "mainthread" allerede her
    mainThreadID = std::this_thread::get_id();
    //cout<<"mainthreadID: "<<mainThreadID<<endl;//debugging

    cleanupLock = std::unique_ptr<std::mutex> (new std::mutex);

    FD_ZERO(&master);
    FD_ZERO(&read_fds);
    fdmax=0;

    if (cfg_args[5] == "true")
    {
        cout<<"AutoListen is on, starting server."<<endl;
        storageInit();
        sockInit();
    }
    else
        cout<<"AutoListen is NOT on, manual server start required."<<endl;

    mainProcessLoop();

}

void Server::sender(uint fd, int data[], uint bufferSize)
{
    uint total = 0;
    int bytesleft = bufferSize;
    int count;

    //this bit is necessary  for the windows version because winsocks send() explicitly requires a char*. The unix one could take a
    //pointer to, presumably, any datastructure.
    //so in short: convert the data[] to a bitwise identical char[].
    char temp[sizeof(int)];
    vector<char> parcel;

    for (int i = 0; i < bufferSize/sizeof(int);i++ )
    {
        memcpy( &temp,&(data[i]),sizeof(int ));

        parcel.push_back(temp[0] );
        parcel.push_back(temp[1] );
        parcel.push_back(temp[2] );
        parcel.push_back(temp[3] );
    }

    while( total < bufferSize )//gjenta til alt er sendt.
    {
        count = send(fd,parcel.data(),bytesleft,0);//unknown if this will work as intended
        if (count == -1)//betyr i praksis MSG_NOSIGNAL, bryt.
            break;

        total += count;
        bytesleft -= count;
    }
}

short Server::addGame(string& name,string& psw)
{
    if ( AvailableGameSlotStack.size() == 0)
        return -1;

    short id = AvailableGameSlotStack.top();
    AvailableGameSlotStack.pop();

    games.push_back( HoldemPokerGame( id,*this,managerOwner,name,psw ) );
    return id;
}

void Server::removeGame(short gameid)
{
    AvailableGameSlotStack.push(gameid);

    auto it = find_if(games.begin(),games.end(),[=]( HoldemPokerGame& obj) -> bool
    {
        return obj.getGameId() == gameid;
    });

    if (it == games.end() )//objektet finnes ikke
    {
        cout<<"Fatal error removing game with id "<<gameid<<": game doesn't exist."<<endl;
        abort();
    }
    else
    {
        games.erase(it);
    }
}

const std::thread::id& Server::getMainThreadId()
{
    return mainThreadID;
}

playerRepresentation& Server::getRepViaFd(short sockfd)
{
    auto it = repmap.find(sockfd);
    if (it != repmap.end() )
        return it->second;
    else
    {
        //exception
        throw MapDoesNotContainObjectException(sockfd);
    }
}

HoldemPokerGame& Server::getGameViaFd(short sockfd)
{
    auto it = repmap.find(sockfd);

    if (it != repmap.end() )
    {
        playerRepresentation& rep = it->second;
        if (rep.getGameId() != -1)
        {
            return getGameViaGameID(rep.getGameId() );
        }
        else//error, sockfd finnes, men er ikke assosiert med noe spill
        {
            cout<<"fatal error in function getGameViaFd: klient's descriptor("<<sockfd<<") is registered, "
                "but does not seem to be associated with any game"<<endl;
            abort();
        }
    }
    else//error, sockfd er ikke registerert overhodet.
    {
        cout<<"fatal error in function getGameViaFd: Sockfd is not registered."<<endl;
        abort();
    }

}

HoldemPokerGame& Server::getGameViaGameID(short gameid)
{
    for (auto& game : games)
    {
        if (game.getGameId() == gameid )
            return game;
    }

    //kommer vi hit, er noe riv ruskende galt.
    cout<<"fatal error in function getGameViaGameID: the game specified("<<gameid<<") does not seem to exist."<<endl;
    abort();
}

void Server::chatHelper(short sockfd,std::vector<int> command)
{
    //husk, det som kommer inn her er en SHORTCHAT
    playerRepresentation& rep = getRepViaFd(sockfd);

    //lag en pakke
    vector<int> parcel;

    parcel.push_back(CHAT);
    parcel.push_back( rep.getGameId());
    parcel.push_back( sockfd);
    parcel.push_back( (int)command.size() - 1);

    for (uint i = 1;i<command.size();i++)
    {
        parcel.push_back(command[i]);
    }

    //finn ut hvorvidt det er en lounger eller ikke
    if (rep.getGameId() == -1)//aka lounger
    {
        parcel[1] = -1;
        //finn alle som er i lounge, og send til dem.
        for( auto it : repmap )
        {
            if (it->second.getGameId() == -1)
                sender(it->first,parcel.data(),sizeof(int) * (int)parcel.size() );
        }
    }
    else //aka in play
    {
        parcel[1] = rep.getTableSlot();
        //finn de som er same game, og send til dem
        for( auto it : repmap )
        {
            if (it->second.getGameId() == rep.getGameId() )
                sender(it->first,parcel.data(),sizeof(uint) * (int)parcel.size() );
        }
    }
}

//alle kommandoer kommer innom her.
void Server::processRequest(vector<int> request,int fd)
{
    int opcode = request[0];
    cout<<"opcode is: "<<opcode<<endl;
    auto it  = repmap.find(fd);

    //This first test is has the purpose of denying the player if he tries an action that is temporarily denied due to timeouts.
    if (it != repmap.end() )//player is in the repmap, ie already logged in.
    {
        playerRepresentation& rep = getRepViaFd(fd);
        if ( rep.getGameId() != -1 )//dvs klienten er med i et spill
        {
            HoldemPokerGame& game = getGameViaFd(fd);
            //Hvis server i skrivende stund er i pause modus, send en notis tilbake, returner.
            game.showdownLockManager->lock();
            if( game.do_not_process_requests )
            {
                if (request[0] == SHORTCHAT )//untak fra noprocess regelen
                {
                    chatHelper(fd,request);
                }
                else
                {
                    int dataparcel[1] = { GAMEPAUSEDNOTIFY };
                    sender(fd,dataparcel,sizeof(dataparcel));
                }
                game.showdownLockManager->unlock();
                return;
            }
            game.showdownLockManager->unlock();
        }
    }
    else //fd er ikke registrert i repmap, da er det enten snakk om en som prøver å logge inn, eller en bug.
    {
        if ( opcode == LOGIN || opcode == REGISTER)
        {
            switch (opcode)
            {
            case LOGIN:
            {
                ushort userlength = request[1];
                ushort passwordlength = request[2];

                //les så data inn i lokale variabler
                string username;
                string password;

                //username
                for(int i = 0; i< userlength; i++)
                    username.push_back( request[3+i] );

                //password
                for(int i = 0; i< passwordlength; i++)
                    password.push_back( request[3+i+userlength] );

                //forsøke å logge inn
                uint cookieid = managerOwner->loginAttempt(username,password);

                if ( cookieid != -1)//hvis cookieid = -1, misslykket, hvis ikke suksess.
                {
                    //fyr av et authenticationsuccess signal
                    cout<<"authentication success"<<endl;
                    vector<int> success = { AUTHENTICATIONSUCCESSNOTIFY,(int)cookieid };
                    sender(fd,success.data(), sizeof(int) * success.size() );

                    //Ny klient må bli gjort oppmerksom på eksisterende games, if any. Dette gjøres tidlig da eksisterende spillere alt kan være
                    //assosiert med dem, dvs, NEWPLAYERNOTIFY kan måtte trenge å henvise til disse spillene.
                    for(auto& game : games)
                    {
                        string gamename = game.getGameName();

                        vector<int> newgameParcel;
                        newgameParcel.push_back(NEWGAMENOTIFY);
                        newgameParcel.push_back(game.getGameId());
                        newgameParcel.push_back( gamename.size() );

                        for( int i = 0; i < gamename.size();i++ )
                            newgameParcel.push_back( gamename[i] );

                        sender(fd,newgameParcel.data(),sizeof(int) * newgameParcel.size() );
                    }

                    //må fortelle samtlige klienter at en ny klient er å finne i lounge området.
                    //int newguyparcel[4 + username.length()];
                    vector<int> newguyparcel;

                    newguyparcel.push_back( NEWPLAYERNOTIFY );
                    newguyparcel.push_back( -1 );
                    newguyparcel.push_back( fd );
                    newguyparcel.push_back( userlength );

                    for( int i = 0; i < userlength; i++ )
                        newguyparcel.push_back( username[i] );

                    //vi må huske å oppdatere rep, kan opprette newguys sin rep allerede her.
                    //vi setter balance her også.
                    string name = managerOwner->getUsername(cookieid);
                    uint balance = managerOwner->getBalance(cookieid);
                    playerRepresentation rep(fd,cookieid,name,balance);

                    //send så til alle
                    for(auto it = repmap.begin(); it != repmap.end();it++)
                    {
                        //send newguyparcel til oldguy, og oldguy parcel til newguy
                        playerRepresentation& oldguy = getRepViaFd(it->first);
                        string oldguyname = oldguy.getName();
                        //pakk oldguyparcel full av data.
                        vector<int> oldguyparcel;

                        oldguyparcel.push_back( NEWPLAYERNOTIFY );
                        oldguyparcel.push_back( oldguy.getGameId() );
                        oldguyparcel.push_back( oldguy.getSocketNr() );
                        oldguyparcel.push_back( oldguyname.length() );

                        for ( uint i = 0;i< oldguyname.length();i++ )
                            oldguyparcel.push_back( oldguyname[i] );

                        //send så til begge parter
                        sender(it->first,newguyparcel.data(),sizeof(int) * newguyparcel.size());
                        sender(fd,oldguyparcel.data(),sizeof(int) * oldguyparcel.size() );
                    }

                    //så sender vi en pakke om newguy, til newguy.
                    sender(fd,newguyparcel.data(),sizeof(int) * newguyparcel.size() );
                    //nå som vi er ferdig med å oppdatere de gamle, bind fd til en splitter ny rep
                    repmap.insert(std::pair<short,playerRepresentation>(fd,rep));
                }
                else
                {
                    cout<<"authentication failure"<<endl;
                    int failure = AUTHENTICATIONFAILURENOTIFY;
                    sender(fd,&failure,sizeof(failure));
                }
                break;
            }
            case REGISTER:
            {
                ushort userlength = request[1];
                ushort passwordlength = request[2];

                //les så data inn i lokale variabler
                string username;
                string password;

                //lets get the username and password in more appropriate containers
                for(int i = 0; i< userlength; i++)
                    username.push_back( request[3+i] );

                for(int i = 0; i< passwordlength; i++)
                    password.push_back( request[3+i+userlength]);

                //forsøker her å legge til ny profil, hvis den alt eksisterer, dvs brukernavn finnes alt, returneres nullptr
                bool result = managerOwner->addProfile(username,password);

                if (!result)//IE profile already exists, or length is invalid.
                {
                    cout<<"profile creation failed, already exists or wrong length."<<endl;
                    int failureParcel = INVALIDPROFILECREATION;
                    sender(fd,&failureParcel,sizeof(failureParcel));
                }
                else //opprettelsen var vellykket, send tilbakemelding
                {
                    //great om bruker får bekreftelse på at opprettelsen faktisk var vellykket.
                    int success = VALIDPROFILECREATION;
                    sender(fd,&success,sizeof(success));
                }
                break;
            }
            }
        }
        else
            cout<<"Error: Unlogged peer attempts to communicate via secured or undefined protocols. Ignoring"<<endl;
        return;
    }

    //Bruker er logget inn, og kan kommunisere via øvrige protokoller.
    switch (opcode)
    {
        case SETREADY:
            {
                playerRepresentation& rep = this->getRepViaFd(fd);
                rep.setReady();
                cout<<"setready called by: "<<fd<<endl;
                break;
            }
        case CLEARREADY:
            {
                playerRepresentation& rep = this->getRepViaFd(fd);
                rep.clearReady();
                cout<<"ClearReady called by: "<<fd<<endl;
                break;
            }
        case SETNAME:
            {
                string name;
                for(int i = 1; i < request.size(); i++)
                    name.push_back((char)request[i] );

                playerRepresentation& rep = getRepViaFd(fd);
                bool result = managerOwner->changeUsername(name,rep.getName() );

                if ( !result )//Name change was invalid due to invalid characters/or someone else already using it.
                {
                    int invalid = NAMECHANGEINVALIDNOTIFY;
                    sender( fd,&invalid,sizeof(invalid) );
                }
                else
                {
                    rep.setName(name);

                    //Fylle opp og informere samtlige klienter om navnebytte
                    vector<int> dataparcel;
                    dataparcel.push_back( NAMECHANGEDNOTIFY );
                    dataparcel.push_back( rep.getSocketNr() );
                    dataparcel.push_back( (int)false);
                    dataparcel.push_back( rep.getTableSlot() );
                    dataparcel.push_back( name.size() );

                    for(uint i = 0; i< name.size();i++)
                        dataparcel.push_back( name[i] );

                    for (auto it = repmap.begin();it != repmap.end();it++)
                        sender(it->first,dataparcel.data(),sizeof(int) * dataparcel.size() );

                    //i tillegg må rommet klienten er i, om noen, gjøres oppmerksom på at spilleren har skiftet navn.
                    if ( rep.getTableSlot() == -1 )//dvs er en lounger.
                        break;
                    else
                    {
                        //gjør klar pakka
                        vector<int> holdemparcel;
                        holdemparcel.push_back( TABLESLOTCHANGEDNAMENOTIFY );
                        holdemparcel.push_back( rep.getTableSlot() );
                        holdemparcel.push_back( (int)false );
                        holdemparcel.push_back( name.size() );

                        for(uint i = 0; i< name.size();i++)
                            holdemparcel.push_back( name[i] );

                        //finn riktig spill, send så til alle reps i denne
                        HoldemPokerGame& game = getGameViaFd(fd);

                        for (auto& rep : game.getReps() )
                            sender( rep.second->getSocketNr(), holdemparcel.data(),sizeof(int) * holdemparcel.size() );
                    }
                }
                break;
            }
        case REFILL:
            {
                uint refill = request[1];
                getGameViaFd(fd).increaseEscrowCheat(fd,refill);
                break;
            }
        case TRANSFER:
            {
                uint transferamount = request[1];
                getGameViaFd(fd).tranfer_From_Escrow_Request(fd,transferamount);
                break;
            }
        case CHECK:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).checkRequest(tablenr);
                break;
            }
        case RAISE:
            {
                int raisevalue= request[1];
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).raiseRequest(raisevalue,tablenr);
                break;
            }
        case FOLD:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).foldRequest(tablenr);
                break;
            }
        case CALL:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).callRequest(tablenr);
                break;
            }
        case ALLIN:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).AllinRequest(tablenr);
                break;
            }
        case JOIN://opcode-gameid-usingPSW-pswCount-psw
        {
            //Sjekk om spiller alt er registrert ved et spill, i så fall send feilmelding.
            //spiller kan ikke gå inn i et nytt rom, før det forrige har ferdigbehandlet spillerens status.
            //alternativt er at koden knyttet til aktive med frakoblede spillere fjernes, noe som blant annet
            //vil si at hvis en disconnect etter å ha gått allin, så mister du hele veddemålet automatisk.

            cout<<"ENTERED JOIN: id:"<<request[1]<<endl;
            playerRepresentation& rep = getRepViaFd(fd);

            //Mandatory fields
            short gameid = request[1];
            bool usingPSW = (bool)request[2];

            //Optional fields
            short pswCount;
            string psw;

            //DEBUG
            cout<<"Entered JOIN, gameid:  "<<gameid<<"\tusingPSW: "<<usingPSW<<endl;

            //if usingPSW is true, load optional value(the password, or psw for short)
            if ( usingPSW )
            {
                //Load pswCount, then use it to load psw itself
                pswCount = request[3];

                for( int i = 0;i < pswCount;i++ )
                    psw.push_back( (char)request[i + 4] );
            }

            if (rep.getGameId() != -1) //dvs spiller er fortsatt formelt inne i et annet rom.
            {
                int joinfailureparcel = JOINFAILURENOTIFY;
                sender(fd,&joinfailureparcel,sizeof(joinfailureparcel));
                break;
            }

            cout<<"further JOIN data: "<<pswCount<<"\tpsw: "<<psw<<endl;
            HoldemPokerGame& game = getGameViaGameID(gameid);
            short tableslot = game.addPlayer( &rep,psw );

            if ( tableslot >= 0 )//tableslots are always 0 or higher, negative tableslots are illegal, which is nice since we can use them as error codes.
            {
                //ok, kjør welcomeprotocol.
                game.welcomeProtocol(fd);

                //spiller må bli gjort oppmerksom på at forespørselen ble godkjent, slik at den kan switche til holdem
                int parcel = JOINSUCCESSNOTIFY;
                sender(fd,&parcel,sizeof(parcel));

                //vi må oppdatere samtlige klienters model, slik at de vet at denne klienten nå er i et spill.
                for (auto it = repmap.begin();it != repmap.end();it++)
                {
                    int playerjoinedParcel[3] = { PLAYERJOINEDROOMNOTIFY,fd,game.getGameId() };
                    sender(it->first,playerjoinedParcel,sizeof(playerjoinedParcel));
                }
                //Kjør initGame, spillet vil starte hvis det er minst 2 gyldige spillere tilkoblet, hvis ikke skjer ingenting annet enn en feilmelding til spilleren.
                game.processNewPlayer(fd,tableslot);
            }
            else if ( tableslot == HoldemConst::ROOMFULL)//betyr at det er for mange spillere i det rommet
            {
                int failureParcel = { INVALIDCOMMAND };
                sender(fd,&failureParcel,sizeof(failureParcel) );
            }
            else if ( tableslot == HoldemConst::FAULTYPSW )
            {
                //If usingPSW was true, then send a INVALIDROOMPSWNOTIFY, ie it was a login attempt with a faulty password. If false, then send a PSWPROTECTEDNOTIFY instead in order
                //to prompt the user to enter a password. In practice this means that a password dialog will be shown, and a new JOIN command will be sent, this time with a password
                //and usingPSW set to true
                if ( usingPSW )
                {
                    int wrongPSWParcel = {INVALIDROOMPSWNOTIFY};
                    sender(fd,&wrongPSWParcel,sizeof(wrongPSWParcel) );
                }
                else
                {
                    int PSWProtectedParcel[2] = {PSWPROTECTEDNOTIFY,gameid};
                    sender(fd,PSWProtectedParcel,sizeof(PSWProtectedParcel) );
                }
            }
            else //In case we get something totally meaningless back from addPlayer
            {
                cout<<"Fatal error in server::processCommand()->case JOIN"<<endl;
                abort();
            }
            break;
        }
        case CREATE:
        {
            //Declare mandatory fields first
            bool pswprotection;
            int nameCount;
            string gamename;

            //optionals
            int pswCount;
            string psw;

            //Then we load the room name
            nameCount = request[2];

            for (int i = 0; i < nameCount;i++)
                gamename.push_back( (char)request[i+3] );

            //Load pswprotection, which will be used to load the psw field if applicable.
            pswprotection = (bool)request[1];

            if (pswprotection )//If true, then the user specified that the room should be psw protected
            {
                pswCount = request[3 + nameCount];
                for (int i = 0; i < pswCount;i++)
                    psw.push_back((char)request[i + 4 + nameCount] );
            }

            //now we check if the length(s) are within the acceptable interval
            if ( !(gamename.length() >= Misc::MIN_NAMELENGTH && gamename.length() <= Misc::MAX_NAMELENGTH) )
            {
                int failureParcel = {INVALIDROOMNAMELENGTHNOTIFY};
                sender(fd,&failureParcel,sizeof(failureParcel) );
                break;
            }

            //checking if the gamename is already in use, if discovered, send a error, and then return
            for (auto& game : games)
            {
                if ( game.getGameName().compare(gamename) == 0 )
                {
                    int existsParcel = { INVALIDROOMNAMEEXISTSNOTIFY };
                    sender(fd,&existsParcel,sizeof(existsParcel) );
                    return;
                }
            }

            //similarily for psw, if applicable
            if (pswprotection)
            {
                if ( !(psw.length() >= Misc::MIN_PASSWORDLENGTH && psw.length() <= Misc::MAX_PASSWORDLENGTH) )
                {
                    int failureParcel = { INVALIDROOMPSWLENGTHNOTIFY };
                    sender(fd,&failureParcel,sizeof(failureParcel) );
                    break;
                }
            }

            //data loaded, now check if we've reached the maximum room count, if yes, send error message and break
            short id = addGame(gamename,psw);

            if (id == -1)//means that the request was denied.
            {
                int failureParcel = { MAXROOMSCREATEDNOTIFY };
                sender(fd,&failureParcel,sizeof(failureParcel) );
                break;
            }

            //everything is ok. Broadcast the the existance of the new room
            vector<int> newgameParcel;

            newgameParcel.push_back( NEWGAMENOTIFY );
            newgameParcel.push_back( id );
            newgameParcel.push_back( (int)gamename.size() );
            for( int i = 0; i < gamename.size();i++ )
                newgameParcel.push_back( gamename[i] );

            for(auto it = repmap.begin(); it != repmap.end();it++)
                sender(it->first,newgameParcel.data(),sizeof(int) * (int)newgameParcel.size() );
            break;
        }
        case LEAVEROOM:
        {
            HoldemPokerGame& game = getGameViaFd(fd);

            //håndter det faktum at spiller har forlatt et rom.
            game.handleLeavingPlayer( fd );

            //Send beskjed til samtlige klienter med beskjed om at spiller har forlatt rommet
            int playerLeftRoomParcel[4] = { PLAYERSTATECHANGEDNOTIFY,fd,game.getGameId(),(uint)false };
            for(auto it = repmap.begin();it != repmap.end();it++)
                sender(it->first,playerLeftRoomParcel,sizeof(playerLeftRoomParcel));
            break;
        }
        //vanlig kommunikasjon mellom spillere.
        case SHORTCHAT:
        {
            chatHelper(fd,request);
            break;
        }
        default:
        {
            cout<<"error: unknown opcode("<<opcode<<"), or data, in pipe. Content is: "<<endl;
            for(auto& val : request)
            {
                cout<<val<<"   ";
            }
            cout<<"content end."<<endl;
        }
    }
}

//brukes for å gjøre de øvrige klientene oppmerksom på at en klient har koblet fra
void Server::disconnectProtocol(int fd,bool silent)
{
    //det er mulig at klienten som er koblet til, men ikke logget inn,
    //for stygge feil hvis server prøver å logge ut en spiller som aldri var logget inn.
    try
    {
        if (!silent)
        {
            //hent rep objektet tilhørende den fildeskriptoren, sjekk om vedkommende er koblet til et spill eller ikke.
            playerRepresentation& rep = getRepViaFd(fd);//Hvis denne feiler, betyr det at det var snakk om en som koblet til, men aldri logget inn.

            //fjern så spiller fra repmap, og lukk fd
            if (rep.getGameId() != -1)//hvis det er snakk om en spiller, må ekstra grep tas.
            {
                HoldemPokerGame& game = getGameViaGameID( rep.getGameId() );
                game.handleLeavingPlayer( fd );
            }

            //Uansett må samtlige klienter bli gjort oppmerksom på at spiller ikke lenger er tilkoblet
            int playerleftparcel[4] = { PLAYERSTATECHANGEDNOTIFY,fd,rep.getGameId(),(bool)true };

            for(auto& mapping : repmap)
                sender(mapping.second.getSocketNr(),playerleftparcel,sizeof(playerleftparcel));
        }
        //cleanup kalles for å rydde opp etter den frakoblede brukeren, slik at systemet er konsistent.
        cleanupLock->lock();

        managerOwner->logout( getRepViaFd(fd) );
        repmap.erase(fd);

        cleanupLock->unlock();
    }
    catch (MapDoesNotContainObjectException& e) {}//Snakk om en spiller som koblet til, men ikke logget inn, ingen alvorlig tilstand.

    closesocket(fd);
}

void Server::error(const char *msg)
{
    perror(msg);
    exit(1);
}

void* Server::get_in_addr(sockaddr *sa)
{
    return &(((sockaddr_in*)sa)->sin_addr);
}

bool Server::isListening()
{
    char value;
    socklen_t len = sizeof(value);

    //as a note about the swich below: In our case, it means that the int known as listener hasn't even been associated with a socket descriptor yet
    //more generally a return of -1 indicates that the fd, listenr in my call here, isn't socket descriptor, or isn't a descriptor at all. This applies to unix only
    if ( getsockopt ( listener,SOL_SOCKET,SO_ACCEPTCONN,&value,&len) == -1)//fatal error, the listener is not a socket at all. Should never happen
        return false;
    else if ( value ) //is a socket, and is listening
        return true;
    else
        return false;
}

void Server::stopServer()
{
    //close down the listener, then kick it out of the master set. Then kick out any and all clients
    closesocket(listener);
    FD_CLR(listener,&master);

    //close all sockets.
    for (int fd = 0; fd < 1000 ;fd++)
    {
        if ( FD_ISSET(fd,&master) )
        {
            if ( fd != serversideTunnelSocket && fd != listener )
            {
                playerRepresentation& rep = getRepViaFd(fd);

                FD_CLR(fd,&master);
                disconnectProtocol(fd,true);
            }
        }
    }
    //players have now been properly logged out, and their sockets closed, time to destroy all rooms
    games.clear();
}

void Server::sockInit()
{
    if ( managerOwner.get() == nullptr)//must remember to have a profileManager before starting to listen, if not server is in an invalid state.
    {
        cout<<"Socket could not be initialized, please initialize the profile manager first."<<endl;
        return;
    }
    if ( this->isListening() )
    {
        cout<<"Socket is already listening"<<endl;
        return;
    }

    //get us a socket and bind it
   int wsaerr = WSAStartup(MAKEWORD(2,2), &wsd);//wsaerr is microsofts replacement for stderr
   if (wsaerr != 0)
   {
       cout<<"WSAstartup failed, error code was: \n"<<wsaerr<<endl;
       return;
   }

    memset( &hints,0,sizeof(hints) );
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    int rv = getaddrinfo( NULL,std::to_string(designatedListeningPort).c_str(),&hints,&result );
    if ( rv != 0 )
    {
        fprintf( stderr,"selectserver: &s\n",gai_strerror(rv) );
        WSACleanup();
        return;
    }

    //find a suitable address and bind.
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
    {
        listener = socket(ptr->ai_family,ptr->ai_socktype,ptr->ai_protocol);
        if (listener == INVALID_SOCKET )
        {
            cout<<"INVALID SOCKET"<<endl;
            continue;
        }

        //setsockopt( listener,SOL_SOCKET,SO_REUSEADDR,&ServConst::YES,sizeof(int) );
        if ( ::bind(listener,ptr->ai_addr,(int)ptr->ai_addrlen) == SOCKET_ERROR )
        {
            //common case where the port is already in use by an existing application, usually another instance of G_Holdem_Serv
            int err = WSAGetLastError();
            if ( err == WSAEADDRINUSE )
            {
                cout<<"unable to bind to socket, port already in use. Shutting down."<<endl;
                freeaddrinfo(ptr);
                WSACleanup();
                exit(0);
            }
            cout<<"failed to bind with error: "<<err<<endl;
            freeaddrinfo(ptr);
            WSACleanup();
            continue;
        }
        break;
    }
    freeaddrinfo(ptr);

    //tell socket to listen
    rv = listen(listener,10);
    if ( rv == SOCKET_ERROR )
    {
        cout<<"Unable to listen, posting error code and shutting down: "<<WSAGetLastError()<<endl;
        closesocket(listener);
        WSACleanup();
        return;
    }

    FD_ZERO(&read_fds);
    FD_SET(listener,&master);
    fdmax=listener;
    currentListeningPort = designatedListeningPort;

    //since we're on windows, we'll have to call the socketTunnel "hack" every time we start listening, or else the input won't be routed through
    //the tunnel.
    setupSocketTunnel();

    cout<<"Socket initalization was successful, accepting new connections on port "<<currentListeningPort<<endl;
}

void Server::storageInit()
{
    if ( managerOwner.get() == nullptr)
    {
        if (cfg_args[0] == "DB")
        {
            cout<<"Database storage specified. Values loaded are: IP: "<<cfg_args[1]<<", port: "<<cfg_args[2]<<", username: "
            <<cfg_args[3]<<", password: "<<cfg_args[4]<<"."<<endl;
            managerOwner = std::unique_ptr<ProfileManager>(new DBProfileManager(repmap,cfg_args[1],cfg_args[2],cfg_args[3],cfg_args[4]) );
        }
        else
        {
            cout<<"File storage specified."<<endl;
            managerOwner = std::unique_ptr<ProfileManager>(new FileProfileManager(repmap));
        }
    }
    else
        cout<<"Profile Manager already loaded, ignoring"<<endl;
}

void Server::processUserInput(string& inputstring)
{
    if ( inputstring == "exit")//øyeblikkelig avslutning av server, tilbakebetaler eventuelle penger i spill.
    {
        for (auto& game : games)
            game.reimburse();

        if ( managerOwner != nullptr )
            managerOwner->saveProfiles();
        ServVars::EXIT_CALLED = true;
    }
    else if ( inputstring == "list gamedata")
    {
        cout<<"listing game data:\n";
        for(auto& game : games)
        {
            cout<<"gameId: "<<game.getGameId()<<"    "<<"Rep count: "<<game.getReps().size()<<"\n";
        }
        cout<<"listing complete"<<endl;
    }
    else if ( inputstring == "list repsdata")
    {
        cout<<"listing reps data: "<<endl;
        for(auto it = repmap.begin();it != repmap.end();it++ )
        {
            playerRepresentation& rep = it->second;
            int gameid = rep.getGameId();
            std::string gameidstr;

            if ( gameid == -1)
                gameidstr = "L";
            else
                gameidstr = std::to_string(gameid);

            cout<<"fd: "<<rep.getSocketNr()<<"    fd in map: "<<it->first<<"   gameId: "
               <<gameidstr<<"   Name:"<<rep.getName()<<"    DCstate: "<<rep.getLeavingState()<<"\n";
        }
        cout<<"listing complete \tPS: A gameid equal to \"L\" Means the player is in the lobby, being a \"lobbyer\""<<endl;
    }
    else if ( inputstring == "start" )//combines setup and start manager
    {
        storageInit();
        sockInit();
    }
    else if ( inputstring == "listen" )
    {
        sockInit();
    }
    else if ( inputstring == "load" )
    {
        storageInit();
    }
    else if ( inputstring == "stop" )//if the listener is listening, then this will result in a call to stopServer()
    {
        if ( isListening() )
        {
            stopServer();
            cout<<"Server stopped and reset."<<endl;
        }
        else
            cout<<"Unable to comply, cannot stop server that is already stopped"<<endl;
    }
    else if ( inputstring == "status" )//lists the status of the server
    {
        cout<<"Server status:\n";
        if ( isListening() )
        {
            //cout<<"Server is currently listening."<<"\n";
            if ( designatedListeningPort == currentListeningPort)//designated port matches the one in use
                cout<<"Server currently listening at: "<<currentListeningPort<<endl;
            else
                cout<<"Server currently listening at: "<<currentListeningPort<<". However, the designated listening port is "<<designatedListeningPort<<"."<<endl;
        }
        else
        {
            cout<<"Server is currently NOT listening."<<"\n";
            cout<<"Designated listening port is: "<<designatedListeningPort<<endl;
        }

        cout<<"Number of connected peers is: "<<repmap.size()<<endl;
        cout<<"Number of rooms(games) created: "<<games.size()<<endl;

        ProfileManager* ptr = managerOwner.get();
        if ( managerOwner.get() != nullptr)
        {
            if ( typeid(*ptr) == typeid(FileProfileManager) )
                cout<<"ProfileManager is of type: fileProfileManager"<<endl;
            else if ( typeid(*ptr) == typeid(DBProfileManager) )
            {
                cout<<"ProfileManager is of type: DBProfileManager"<<"\n";
                cout<<"Variables used to connect to the database are as follows:\n";
                cout<<"DB IP: "<<cfg_args[1]<<"\n";
                cout<<"DB Port: "<<cfg_args[2]<<"\n";
                cout<<"DB Username: "<<cfg_args[3]<<"\n";
                cout<<"DB Password: "<<cfg_args[4]<<endl;
            }
        }
        else
        {
            cout<<"ProfileManager has not been initialized."<<endl;
        }

    }
    else if ( inputstring.substr(0,8 ) == "set port" )//set the listen port to whatever number the user provides.
    {
        //check if value entered actually makes any sort of sense
        if ( inputstring.size() == 8 )//user forgot to enter an argument.
        {
            cout<<"Unable to comply: No argument specified."<<endl;
            return;
        }

        string substring = inputstring.substr(9);

        if ( substring.find_first_not_of( "0123456789" ) == string::npos )//no non-numeric characters found
        {
            try
            {
                designatedListeningPort = std::stoi(substring.substr() );
                cout<<"Listening Port set to "<<designatedListeningPort<<endl;
            }
            catch ( std::out_of_range& e)
            {
                cout<<"Unable to comply: Argument length was to long"<<endl;
                return;
            }
            catch (std::invalid_argument& e)//happens if the user enters "set port ", that extra space causes trouble
            {
                cout<<"Unable to comply: Invalid argument specified"<<endl;
                return;
            }
        }
        else
        {
            cout<<"Unable to comply: Invalid argument specified"<<endl;
        }
    }
    else if ( inputstring == "restart")
    {
        if ( isListening() )
        {
            cout<<"Restarting server."<<endl;
            stopServer();
            sockInit();
        }
        else
            cout<<"Unable to comply, server already stopped."<<endl;
    }
    else if ( inputstring == "help")
    {
        cout<<ServConst::commandhelp<<endl;
    }
    else if ( inputstring == "clear" )
    {
        system("cls");
    }
    else if ( inputstring == "god")
    {
        cout<<"This unit recognizes only one god: Lord Kek"<<endl;
    }
    else//ugyldig input
        cout<<"\""<<inputstring<<"\" is not a valid command. Type \"help\" for assistance."<<endl;

    cout<<"Awaiting command-> "<<flush;
}

void Server::setupSocketTunnel()
{
    //Basically, we setup a regular socket, and connect to the server that's already running. Since the main process loop hasn't
    //begun yet, this connection will be the very first one, something we can use to treat this socket differently.
    //It's cumbersome and hackish, but the alternative was to completely redesign the class for windows, using events instead.

    //clean up old connection
    closesocket(tunnelSocket);

    first = true; //must reset this one every time a new tunnel is created, otherwise the server will be unable to tell the real connections from the fake one
    int iResult;
    // Resolve the server address and port
    iResult = getaddrinfo("127.0.0.1",std::to_string(designatedListeningPort).c_str(),&hints,&result);
    if ( iResult != 0 )
    {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        exit(0);
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next)
    {
        // Create a SOCKET for connecting to server
        tunnelSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

        if (tunnelSocket == INVALID_SOCKET)
        {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            exit(0);
        }

        // Connect to server.
        iResult = connect( tunnelSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR)
        {
            closesocket(tunnelSocket);
            tunnelSocket = INVALID_SOCKET;
            continue;
        }
    }

    freeaddrinfo(result);

    if (tunnelSocket == INVALID_SOCKET) {
        printf("Unable to create loopback tunnel to server!\n");
        WSACleanup();
        exit(0);
    }
}

void Server::mainProcessLoop()
{
    //her starter "main process loop". Den går for alltid, med mindre noe skjer.
    cout<<"Awaiting command-> "<<flush;

    //This detached thread replaces the STDIN "hack" we used in the Unix version. While functional, it is not as powerful as the STDIN one, as it means we lose syncronization.
    std::thread consoleThread( [=]
    {
        while(true)
        {
            //grab the console input, process it into a format the sender can understand
            string inputstring;
            getline(cin,inputstring);

            if ( this->isListening() )
            {
                vector<int> data;
                for (int i = 0;i < inputstring.size();i++)
                    data.push_back( (int)inputstring[i] );

                sender(tunnelSocket,data.data(),sizeof(int) * data.size() );
            }
            else
            {
                processUserInput(inputstring);
            }
        }
    } );
    consoleThread.detach();//since this thread is intended to keep working until the software is shut down, there is no need to manually free resources.
mutex test;

    while(!ServVars::EXIT_CALLED )
    {
        read_fds=master;
        if ( select(NULL,&read_fds,NULL,NULL,NULL) == SOCKET_ERROR )
        {
            cout<<"error: attempt to select on bad socket, WSAERROR: "<<WSAGetLastError()<<endl;
            abort();
        }

        //Hopefully this lock will solve my syncronization worries.
        //Truth be told, I don't even know if I actually have to worry about this, but the certainty of no complex simultaneous multithreading going on in this crucial
        //section does put the mind at ease.
        for( int fd = 0; fd<=fdmax;fd++)
        {
            if (FD_ISSET(fd,&read_fds) )//endringer i listen.
            {
                if ( fd == listener ) //betyr ny tilkobling
                {
                    sockaddr_storage remoteaddr;
                    int addrlen = sizeof(remoteaddr);
                    SOCKET newfd = accept(listener,(sockaddr*)&remoteaddr,&addrlen);

                    if (newfd != INVALID_SOCKET) //håndter ny tilkobling
                    {
                        if ( repmap.size() == ServConst::MAXCONN )
                        {
                            int reject = REJECTCONNECTIONNOTIFY;
                            cout<<(char)reject<<endl;
                            sender(newfd,&reject,sizeof(reject));
                            cout<<"New connection refused due to MAXCONN"<<endl;
                            closesocket(newfd);
                            continue;
                        }
                        FD_SET(newfd,&master);

                        if (newfd > fdmax)
                        {
                            fdmax = newfd;
                        }

                        char remoteIP[INET6_ADDRSTRLEN];
                        //sockaddr* test = ai->ai_addr;//for debugging
                        const char* localIP = inet_ntop(remoteaddr.ss_family,get_in_addr( (sockaddr*)&remoteaddr),remoteIP,INET_ADDRSTRLEN);

                        if (first)//first connection should always be the consoleTunnel.
                        {
                            std::lock_guard<std::mutex> lock(test);
                            first = false;
                            serversideTunnelSocket = newfd;
                            cout<<"adding to set: "<<newfd<<" as serversideTunnelSocket."<<endl;
                        }
                        else
                        {
                            cout<<"New connection received from "<<localIP<<" on socket "<<newfd<<endl;
                        }
                    }
                    else
                    {
                        int error = WSAGetLastError();
                        if (error == WSAENOTSOCK )//special case where the lister has been closed
                            continue;
                        cout<<"Error: new socket is invalid, WSAERR: "<<error<<endl;
                        abort();
                    }
                }
                else//mottar meldinger fra koblinger, må leses og prosesseres
                {
                    int buf[1000];
                    memset(&buf,0,100);
                    int nbytes = recv(fd,(char*)&buf,sizeof(buf),0);

                    if (nbytes <=0 )//noe er galt, eller klient lukket kobling.
                    {
                        if (nbytes == 0) //connection closed
                        {
                            if ( fd == serversideTunnelSocket) //I don't want to see that generic message when it is the tunnel that is shutting down.
                            {
                                cout<<"closing down the tunnel(fd: "<<fd<<")."<<endl;
                            }
                            else //actual client connections
                                cout<<"Socket "<<fd<<" hung up."<<endl;
                        }
                        else //nbytes = -1, altså error
                        {
                            cout<<"Recveive error on socket "<<fd<<": closing connection."<<endl;
                        }
                        FD_CLR(fd,&master); //kobling må fjernes fra master

                        thread dcthread(&Server::disconnectProtocol,this,fd,false);
                        dcthread.detach();
/*REDESIGN NOTE:
 * dissconnectProtocol kalles synkront.
 * utlogging og erasing i repmap flyttes ut til egen funksjon som kan kalles fra mainthread eller holdem ;
 * holdemthread får altså da muligheten til å fortelle server at en klient skal logges ut når holdem er ferdig med klient.
 * altså: dcthread utgår fullstendig, mens countdown thread redesignes slik at den tar over den nederste delen av paywinners
 * og legger ett oppdrag i holdem spillet som opprettet den.
 * showdownlockmanager utgår.
 * på denne måten kan vi kvitte oss med all den stygge threadingen som foregår nå.
 *
 * */
                    }
                    else //mottar pakke
                    {
                        //data from console
                        if (fd == serversideTunnelSocket)
                        {
                            vector<int> raw(buf,buf + nbytes/sizeof(int) );
                            string input;
                            for(int i = 0; i < raw.size(); i++)
                                input.push_back(raw[i] );

                            processUserInput(input);
                            continue;
                        }
                        else
                        {
                            cout<<"received package from fd "<<fd<<endl;

                            if (nbytes % Misc::SEGMENT_SIZE != 0)//client doesn't obey the protocol.
                            {
                                cout<<"client doesn't follow basic segmentation protocol, ignoring"<<endl;
                                continue;
                            }
                            vector<int> command(buf,buf + nbytes/sizeof(int) );
                            processRequest(command,fd);
                        }
                    }
                }
            }
        }
    }
}

int main()
{
    Server();
}
#endif //platform check
