#ifndef POKERGAME_H
#define POKERGAME_H

#include "server.h"
#include "deckofcards.h"
#include "card.h"
#include "pokerevaluatorengine.h"
#include "playerrepresentation.h"
#include "scoreobject.h"
#include "potdata.h"
#include "profileManager/profilemanager.h"
#include "clientnotifications.h"

#include <exception>
#include <vector>
#include <memory>

class Server;

class HoldemPokerGame
{
private:
    short holdemID;//lokalt holdemGame ID
    std::string gameName;//name of the room, set during it's creation by a player
    std::string psw;//password, only applicable if pswProtection is true. As of this writing, psw is set at construction, it can't be changed after that. May want to alter that design

    Server& server;
    const std::unique_ptr<ProfileManager>& manager;
    deckofcards deck;
    PokerEvaluatorEngine eng;
    std::stack<uint> AvailabletableSlots;//opprettes ved oppstart, holder 8 verdier fra og med 1 til og med 8.

    std::map<ushort,playerRepresentation*> localReps;//formerly a vector, now tableslots map directly to playerRepresentation pointers. The map hold the players currently playing.
    std::vector<card> community;

    //As a note, these five variables used to be a little wierd, being indexes into the localReps back when it was a vector. Now they are more logical, they are the tablesslots themselves.
    short currentPlayerSlot;
    short currentButtonSlot;
    short currentSmallBlindSlot;
    short currentBigBlindSlot;
    short lastPlayerToPlaySlot;

    int currentBid;
    int pot;
    bool halt;                          //spesiel variabel som brukes for å flagge for "halt state", dvs at partiet ikke kan fortsette.
    bool preflopRound;
    bool flopRound;
    bool turnRound;
    bool riverRound;
    bool lastPlayerHasPlayed;
    short foldCount;
    short raiseCount;

private:

    void newRound();
    void advanceGameState();
    void forcePayBlinds();                 //tvinger to spillere til å betale small og big blinds.
    void dealCardsToPlayers();

    //spillets gang. funksjonene representerer hver runde i et texasholdem spill
    void preflop();                     //Kan teknisk sett fint være del av startGame, men føles likevel riktig å ha den som en egen funksjon
    void flop();
    void turn();
    void river();
    void showdown();                    //sisteledd, oppretter en tråd som kaller payWinners.

    //mest en utvidelse av showdown, egen funksjon pga "showdownthread"
    void payWinners();

    //iteratorer, spesielt tilpasset spillets logikk.
    short nextActivePlayer(uint tableslot);           //Finner neste aktive spiller.
    short nextPlayer(uint tableslot);                 //Finner ut hvilken spiller som er neste, ignorerer activestate
    short formerPlayer(uint tableslot);                //Stikk motsatt av nextPlayer, bortsett fra.
    short formerActivePlayer(uint tableslot);          //motpart til nextActivePlayer().

    //spesialtilstander
    bool soleContenderCheck();                            //sjekker om alle spillere unntatt en har foldet
    bool betMatched();                            //Bekrefter at alle aktive har bidratt like mye til potten.
    bool AllinSpecialCase();                        //Hvis alle aktive er allin, vil nextActivePlayer gå i loop da allin spillere er spesielle, de er aktive, men samtidig ikke.

    //hjelpefunksjoner av ymse slag
    short findTableSlotViaSockNr(short socknr);
    int findSocketNrViaTableSlot(short tableslot);

public:
    /*muligens feil å ha disse tre public, men siden det er meningen at disse skal aksesseres, dvs leses og modifiseres,
     ubegrenset av en aktør utenfor klassen, ser jeg ingen logisk grunn til å lage egne getters og setters.
     God programmering? Ikke godt å si.*/
    std::unique_ptr<std::mutex> showdownLockManager;
    std::unique_ptr<std::mutex> DClockManager;
    bool do_not_process_requests;

    HoldemPokerGame(short holdemID, Server& servptr, const std::unique_ptr<ProfileManager>& managerPtr, std::string &nameRef, std::string &pswRef);
    void processNewPlayer(int fd);//brukes når en ny spiller blir med.

    //funksjoner knyttet til nye og utgående klienter.
    short addPlayer(playerRepresentation* rep, std::string& psw);//oppretter en ny Reps, assosierer denne med en tableslot hentet fra AvailabletableSlotStack,
    void removePlayer(playerRepresentation& ref);//legger opp til å fjerne en spiller, gjerne en som dissconnecter, imidlertid er the startGame som faktisk renser reps. Dette er for at spillere som
                                    //kobler fra kan gjøre dette midt i et spill, blir slitsomt hvis den spillerens bets bare skulle forsvinne til eksempel.
    //offentlige geters.
    playerRepresentation& findRepViaFd(short sockfd);
    std::map<ushort, playerRepresentation*>& getReps();
    short getGameId();
    std::string& getGameName();         //returns the rooms name, if applicable

    //nye og utgående spillere må håndteres riktig, disse funksjonene har den jobben.
    void handleLeavingPlayer(short sockfd);//brukes av server::dissconnectProtocol, merker gjeldende spiller som inaktiv, hvis spiller er currentPlayer, avanseres currentPlayer først.
    void welcomeProtocol(int newfd);//Brukes for initalisere ny spillers Rep objekt lokalt så vel som i de eksterne representasjonene i klientene.
                                                    //flyttet hit fra server, ga liten mening at dette ble gjort der.

    //Funksjoner spillere kan kalle. Vil alltid returnere false hvis spilleren ikke er currentPlayer.
    void callRequest(short tableslot);                  //spiller ber om å matche currentBid. lovlig så lenge man kan betale.
    void checkRequest(short tableslot);                 //spiller ber om å bli med videre uten å vedde. Lovlig så lenge ingen andre har lagt ned penger
    void raiseRequest(int raisevalue, short tableslot); //spiller ber om å høyne, lovlig så lenge man kan betale.
    void foldRequest(short tableslot);                  //Spiller kaster seg, alltid lovlig.
    void AllinRequest(short tableslot);                 //spesiel variant av call, spiller ber om en allin, gjør spiller istand til å være med videre uten å ha råd til å betale potten.

    //Øvrige kall ikke direkte knyttet til spillets gang.
    void reimburse();//tilbakebetaler spillernes veddemål. OBS: spillet er i en ugyldig state rett etterpå,
                    //er først og fremst ment å brukes til å avslutte spillet uten at klientene mister penger som alt er lagt i potten.

    void tranfer_From_Escrow_Request(short fd, uint refill);      //Spiller ber om å overføre penger fra sin escrow, til sin playerBalance.
    void increaseEscrowCheat(short fd, uint amount);              //Enn så lenge, eneste måten spillere kan øke sin escrow
};
#endif // POKERGAME_H
