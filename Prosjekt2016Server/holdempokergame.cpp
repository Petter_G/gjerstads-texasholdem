#include "holdempokergame.h"
#include "deckofcards.h"
#include "protocolenums.h"
#include "exceptions/communityemptyexception.h"

#include <algorithm>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <set>

//debug
#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

//Brukes hvis det er behov for å opprette playerReps på forhånd(f.eks hvis
HoldemPokerGame::HoldemPokerGame(short gameID, Server& servptr, const std::unique_ptr<ProfileManager> &managerPtr, string& nameRef, string& pswRef)
                                 : server(servptr),manager(managerPtr),holdemID(gameID),gameName(nameRef),psw(pswRef)
{
    showdownLockManager = std::unique_ptr<std::mutex> (new std::mutex);
    DClockManager = std::unique_ptr<std::mutex> (new std::mutex);
    do_not_process_requests = false;

    //fyller opp
    for(short i = HoldemConst::MAXSLOTS_AT_TABLE; i != 0; i--)
        AvailabletableSlots.push(i);

    halt=true;
}

short HoldemPokerGame::addPlayer(playerRepresentation* rep, string &psw)
{
    if (AvailabletableSlots.size() == 0 )
        return -1;

    if ( psw.compare(this->psw) != 0 )//not equal, ie password is incorrect
        return -2;//-2 is code for wrong password


    short tableslot = AvailabletableSlots.top();
    AvailabletableSlots.pop();
    rep->setGameId(this->getGameId() );
    rep->setTableSlot(tableslot);
    rep->setInActive();
    localReps.insert(std::pair<uint,playerRepresentation*>(tableslot,rep));

    cout<<"Game<"<<holdemID<<">: Added new player with socket "<<rep->getSocketNr()<<". associated with tableslot "<<tableslot<<endl;
    return tableslot;
}

void HoldemPokerGame::removePlayer(playerRepresentation &ref)
{
    auto it = localReps.find( ref.getTableSlot() );

    //something odd is afot if this ever is the case, make a report to clog for debugging.
    //basically that means that the tableslot isn't in use.
    if (it == localReps.end())
        clog<<"Error<"<< ref.getGameId()<<">: Attempt to remove player with tableslot "<<ref.getTableSlot()<<
        " failed. Ignoring"<<endl;
    else
    {
        clog<<"Game<"<<ref.getGameId()<<">: Removing player with socketNr "<<ref.getSocketNr()<<endl;
        AvailabletableSlots.push( ref.getTableSlot() );
        localReps.erase(it);
    }
}

void HoldemPokerGame::processNewPlayer(int fd)
{
/*I hvilken tilstand kan game være i når denne kalles?
    1 - for få spillere er alt koblet til, dvs reps.size = 0 eller 1.
    2 - ny spiller gjør at nok spillere er koblet til.
    3.1 - det er nok spillere, men for mange er invalid, dvs de kan ikke betale. Spillet forblir i halt state likevel
    3.2 - ny spiller entrer et spill som alt er i gang.
*/
    if ( localReps.size() < 2 )//state 1
    {
        ClientNotifications::NotifyHaltState_BCAll(server,localReps);
        return;
    }
    else if (halt )//spillet er av en eller annen grunn ikke i gang, og kan fint forsøkes restartet ved hjelp av å kjøre newRound.
    {
        newRound();//som en apropos, newRound vil reaktivere halt hvis det likevel er slik at det ikke er nok gyldige spillere
    }
    else//runde er alt i gang, ny spiller får pent vente. Vil at spiller kan se hva de andre driver med, får dermed en rekke notfies her.
        //da disse vanligvis skjer i preflop, som jo ikke blir kjørt.
    {
        ClientNotifications::NotifyExistingCommunityCards_BCSingle(server,fd,community);
        ClientNotifications::NotifyBalanceChanged_BCAll(server,localReps);
        ClientNotifications::NotifyBetChanged_BCAll(server,localReps);

        ClientNotifications::NotifyButton_BC(server,localReps,currentButtonSlot);
        ClientNotifications::NotifyHandSet_HAll(server,localReps);
        ClientNotifications::NotifyPot_BC(server,localReps,pot);
        ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);

        for ( auto it : localReps )
        {
            short tableslot = it.second->getTableSlot();
            ClientNotifications::NotifyPlayerFolded_BCAll( server,localReps,tableslot );
        }
    }
}

//Brukes for å rydde opp i i tilfelle spillere har koblet fra og lignende.
void HoldemPokerGame::newRound()
{

    //opprenskning fra forrige parti.
    currentBid=0;
    foldCount=0;

    preflopRound=false;
    flopRound=false;
    turnRound=false;
    riverRound=false;
    community.clear();
    raiseCount = 0;
    deck.shuffle();

    pot = 0;

    //vector::erase() har ulempen(avhengig av perspektiv) med at vectoren hele tiden er perfekt satt opp, elementet som fjernes legger ikke igjen noe.
    //Dette betyr at player pekeren inne i foreach løkka peker til et annet objekt etter at erase har fjernet det egentlige objektet.
    //løsning: legg fdene i en egen backlog, løp så etterpå igjennom denne og kall removePlayer for hver verdi.
    vector<uint> FDbacklog;
    //løper så igjennom på jakt etter spillere som har koblet fra, eller forlatt spillet;
    for (auto it : localReps )
    {
        playerRepresentation& rep = *it.second;
        if ( rep.getLeavingState() )//spiller har koblet fra, må renses
        {
            //int parcel[2] = { DELETEREP,rep.getTableSlot() };
            vector<int> parcel = {DELETEREP,rep.getTableSlot() };

            //legg fd i en egen backlog.
            FDbacklog.push_back( rep.getSocketNr() );
            //så gjør vi samtlige spillere klar over at spilleren ikke lenger er med i spillet.
            for (auto otherPlayer : localReps)
            {
                server.sender(otherPlayer.second->getSocketNr(),parcel.data(),sizeof(int) * parcel.size() );
            }

        }
    }
    for (auto& fd : FDbacklog)
    {
        playerRepresentation& rep = findRepViaFd(fd);
        removePlayer( rep );
        //ok, vi kan nå resete objektet slik at det er konsistent.
        rep.resetToLounger();
    }

    //prosessere spillere, og resette alt relevant
    for (auto it : localReps)
    {
        playerRepresentation& rep = *it.second;
        rep.resolvePendingBalance();
        rep.dumpCards();
        rep.resetBet();
        rep.resetTotalBet();
        rep.setAllin(false);

        if ( rep.getBalance() == 0)//bankrupt, må merkes inaktiv
        {
            rep.setInActive();
            ClientNotifications::NotifyPlayerFolded_BCAll(server,localReps,rep.getTableSlot());
            cout<<"player "<<rep.getTableSlot()<<"is bankrupt, marked as inactive."<<endl;
        }
        else
        {
            rep.setActive();
        }
    }

    //får trøbbel hvis spillet ved dette punkt kun har en eller færre aktive spillere. Da må spillet settes i en "halt" state frem til problemet er løst.
    uint validCount=0;
    for ( auto it : localReps )
    {
        if (it.second->getActiveState() )//hvis spillere er helt bankrupt før partiets start, kan de ikke spille.
             validCount++;
    }

    if(validCount < 2 )
    {
        halt = true;
                cout<<"DEBUG preflop 1"<<endl;
        ClientNotifications::NotifyHaltState_BCAll(server,localReps);
    }
    else
    {
        halt = false;
        preflop();
    }
}


//Denne funksjonen har til hensikt å avansere spillets status etter vellykket handling fra currentPlayer, dvs fold,check,raise eller call.
//Dette betyr deriblant å iterere currentplayer, såvel som hvilket nivå av spillet man er kommer til(flop,river osv).
void HoldemPokerGame::advanceGameState()
{
    if (soleContenderCheck() )
    {
        cout<<"passed solecontender"<<endl;
        showdown();
    }
    else if (AllinSpecialCase() )
    {
        cout<<"passed AllinSpecialCase()"<<endl;
        showdown();
    }

    else if (lastPlayerHasPlayed )//alle spillere har fått handle minst en gang.
    {
        if (betMatched() )//hvis alle aktive spillere har bidratt like mye, kan vi gå videre.
        {
            //cout<<"PASSED BETMATCHED"<<endl;

            if (preflopRound)
                flop();
            else if (flopRound)
                turn();
            else if (turnRound)
                river();
            else if (riverRound)
                showdown();
        }
        else //av en eller annen grunn, har ikke alle aktive bidratt like mye til potten, neste spiller.
        {
            cout<<"pot contribution invalid, nexting"<<endl;
            currentPlayerSlot=nextActivePlayer(currentPlayerSlot);
            ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);
        }
    }
    else //hvis vi innen en eller annen type runde(preflop,flop,turn osv) ikke har latt samtlige spillere spille enda, bare iterer currentplayer.
    {
        cout<<"option still unplayed"<<endl;

        currentPlayerSlot=nextActivePlayer(currentPlayerSlot);
        ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);
    }
}

void HoldemPokerGame::preflop()
{
    //Siden spillere nå kan koble fra uten at spillet automatisk slutter, får vi trøbbel hvis currentButton ved en tilfeldighet er den som koblet fra
    //i forrige runde. Løsning: Sjekk om currentButton er gyldig, hvis ja sett currentButton til første gyldige spiller ved eller ettter
    //0(siden spiller ved indeks 0 i teorien også kan være inaktiv).
    if ( currentButtonSlot >= localReps.size() )//aka button var sist i Reps, men har nå koblet fra.
    {
        currentButtonSlot = nextActivePlayer(0);
        cout<<"Invalid buttonIndex detected, correcting"<<endl;
    }
    currentButtonSlot= nextActivePlayer(currentButtonSlot);
    currentSmallBlindSlot = nextActivePlayer(currentButtonSlot);
    currentBigBlindSlot = nextActivePlayer(currentSmallBlindSlot);

    //Alt ok så langt, forbered preflop.
    currentPlayerSlot=nextActivePlayer(currentBigBlindSlot);
    lastPlayerToPlaySlot=formerActivePlayer(currentPlayerSlot);
    preflopRound = true;
    lastPlayerHasPlayed= false;
    ClientNotifications::NotifyTableReset_BC(server,localReps);
    ClientNotifications::NotifyButton_BC(server,localReps,currentButtonSlot);
    forcePayBlinds();
    dealCardsToPlayers();
    ClientNotifications::NotifyHandSet_HAll(server,localReps);
    ClientNotifications::NotifyPot_BC(server,localReps,pot);
    ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);
    //spillet er nå klart, første spiller er den til venstre for bigblind.
}

void HoldemPokerGame::flop()//det legges ned 3 communitycards.
{
    cout<<"entering flop"<<endl;
    flopRound=true;
    preflopRound=false;
    currentBid=0;
    lastPlayerHasPlayed=false;
    currentPlayerSlot=nextActivePlayer(currentButtonSlot);
    lastPlayerToPlaySlot=formerActivePlayer(currentPlayerSlot);

    //sette bets til 0 for alle spillere.
    for( auto player : localReps )
        player.second->resetBet();

    //legg ut tre kort
    for(int i = 0;i<3;i++)
    {
        community.push_back(deck.dealcard());
        ClientNotifications::NotifyCommunityCardAdded_BC(server,localReps,community.back() );
    }
    ClientNotifications::NotifyBetChanged_BCAll(server,localReps);
    ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);

}

void HoldemPokerGame::turn()
{
    cout<<"entering turn"<<endl;

    turnRound=true;
    flopRound=false;
    currentBid=0;
    lastPlayerHasPlayed=false;

    currentPlayerSlot = nextActivePlayer(currentButtonSlot);
    lastPlayerToPlaySlot = formerActivePlayer(currentPlayerSlot);

    for(auto player : localReps)
    {
        player.second->resetBet();
    }

    //Et nytt kort
    community.push_back(deck.dealcard());
    ClientNotifications::NotifyCommunityCardAdded_BC(server,localReps,community.back());

    ClientNotifications::NotifyBetChanged_BCAll(server,localReps);
    ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);
}

void HoldemPokerGame::river()
{
    cout<<"entering river"<<endl;

    riverRound=true;
    turnRound=false;
    currentBid=0;
    lastPlayerHasPlayed=false;

    currentPlayerSlot=nextActivePlayer(currentButtonSlot);
    lastPlayerToPlaySlot=formerActivePlayer(currentPlayerSlot);

    for( auto player : localReps )
        player.second->resetBet();

    //Et nytt kort
    community.push_back(deck.dealcard());
    ClientNotifications::NotifyCommunityCardAdded_BC(server,localReps,community.back());

    ClientNotifications::NotifyBetChanged_BCAll(server,localReps);
    ClientNotifications::NotifyCurrentPlayer_BCAll(server,localReps,currentPlayerSlot);
}

void HoldemPokerGame::showdown()
{
    cout<<"entering showdown"<<endl;
    ClientNotifications::NotifyHandSet_BCALL(server,localReps);

    using namespace std;
    //lager en tråd av payWinners, grunnen er at vi vil at forespørseler fra klienter etter at runden er over ikke skal prosesseres,
    //men samtidig  vil vi ikke at server skal være uresponsiv i 5 sekunder.
    thread countDown(&HoldemPokerGame::payWinners,this);

    if (server.getMainThreadId() == std::this_thread::get_id() )
    {
        countDown.detach();
        cout<<"countDown Thread started and detached by mainthread"<<endl;
    }
    else
    {
        countDown.join();
        cout<<"countDown thread was joined by DCthread"<<endl;
    }
}

void HoldemPokerGame::payWinners()
{
    cout<<"entering paywinners, showdown thread is active"<<endl;

    showdownLockManager->lock();
    do_not_process_requests=true;
    showdownLockManager->unlock();

    //Vi kgameLockManager løpe igjennom Reps på jakt etter aktive spillere alene, kan likesågjerne lage en hjelpevektor av pekere
    //samtidig trenger å vite hvor mye free money vi har, dvs bets lagt igjen av de som kastet kortene.
    vector<playerRepresentation*> actives;
    vector<playerRepresentation*> inActives;

    for( auto it : localReps)
    {
        if (it.second->getActiveState() )
            actives.push_back(it.second);
        else
            inActives.push_back(it.second);
    }
    //så kommer problemet med allins, vi sorter actives stigende basert på totalbet
    std::sort(actives.begin(),actives.end(),
        [](playerRepresentation* a, playerRepresentation* b) -> bool
        {
            return a->getTotalBet() < b->getTotalBet();
        });

    //hvilke potter har vi? Heldigvis er dette enkelt å avgjøre, antallet unike totalBet i actives er også lik antallet pots, sortert stigende.
    //Vi trenger bare å legge til et element i Pots for hver unike verdi,
    //eller inkrementere elementets contesters variabel hvis verdien alt er representert.
    vector<PotData> Pots;

    vector<playerRepresentation*> uniqueActives(actives.begin(),actives.end());
    uniqueActives.erase( unique(uniqueActives.begin(),uniqueActives.end(),
                            [] (playerRepresentation* a,playerRepresentation* b)->bool
                            {
                                return a->getTotalBet() == b->getTotalBet();
                            }),uniqueActives.end());

    int potIndex=0;
    for(uint i = 0;i<uniqueActives.size();i++)
    {
        int totalBet = uniqueActives[i]->getTotalBet();
        Pots.push_back( PotData(totalBet,potIndex++) );
        PotData* currentPotPtr=&Pots.back();

        //nå må vi løpe igjennom actives og kalle Pots getContestant for hver aktive spiller som har råd til potten
        for(uint j = 0;j<actives.size();j++)
        {
            if (actives[j]->getTotalBet() >= totalBet )
            {
                currentPotPtr->addContestant(actives[j] );
            }
        }

    }

    //eventuelle sidepotter skal ikke inkludere verdiene til potten før ved utregning av sum, hvert PotData objekt har en eksess variabel
    //som rett og slett er den foregående pottens betValue. På denne måten kan vi spore

    for(uint i = 1;i<Pots.size();i++)
    {
        uint excessPot=Pots[i-1].getBetValue();

        Pots[i].setExcess(excessPot);
    }

    //siste bit før vi bruker evalengine, vi må fordele eventuelle frie penger fra kastede hender til sine respektive potter.
    for(auto& player : inActives)
    {
        uint playerBet=player->getTotalBet();

        for(uint i = 0; i< Pots.size();i++)
        {
            uint betValue = Pots[i].getBetValue();

            if (playerBet >= betValue )//hver enkelt inaktivs veddemål fordeles over hver enkelt pot, hvor hver pot kun
            {
                playerBet -= betValue;
                Pots[i].incrementFreemoney(betValue);
            }
            else
            {
                Pots[i].incrementFreemoney(playerBet);
                break;
            }
        }
    }

    //Nå kan vi endelig bruke EvalEngine.
    vector<ScoreObject*> potWinners;
    for(auto& pot : Pots)
    {
        try
        {
            potWinners = eng.determineWinners(pot.getContesterPointers(),community);
        }
        catch( CommunityEmptyException& )
        {
            break;
        }

        uint payout = pot.calculateSum() / potWinners.size();
        for(auto& score : potWinners)
        {
            playerRepresentation& player = score->getRepPtr();
            cout<<"Increasing tablenr "<<player.getTableSlot()<<" with "<<payout<<endl;
            player.incrementBalance(payout);
            ClientNotifications::NotifyWinnerDataAdded_BCAll(server,localReps,score,payout,pot.getIndex());
        }

    }
    ClientNotifications::NotifyBalanceChanged_BCAll(server,localReps);
    ClientNotifications::NotifyPaintUI_BCAll(server,localReps);

#ifdef __unix__
    sleep(ServConst::AFTERGAMEWAITINGPERIOD);
#endif
#ifdef _WIN32
	Sleep(ServConst::AFTERGAMEWAITINGPERIOD);
#endif

    //hvis ServVars::SYNKRONIZED_EXIT_READY er true, har administrator bedt om en synkronisert avslutning av server, og ingenting skal skje her.
    if ( ServVars::SYNCRONIZED_EXIT_CALLED == true )
        exit(0);

    newRound();

    showdownLockManager->lock();
    do_not_process_requests=false;
    showdownLockManager->unlock();

    cout<<"showdown thread is complete"<<endl;
}

void HoldemPokerGame::dealCardsToPlayers()          //deler ut 2 kort til hver spiller.
{    
    for (auto player = localReps.begin();player != localReps.end();player++)
    {
        playerRepresentation& rep = *player->second;
        if (rep.getActiveState() )
        {
            rep.addCard(deck.dealcard() );
            rep.addCard(deck.dealcard() );
        }
    }
}

void HoldemPokerGame::forcePayBlinds()                 //tvinger to spillere til å betale small og big blinds.
{

    playerRepresentation* smallBlindPtr = localReps[currentSmallBlindSlot];
    playerRepresentation* bigBlindPtr = localReps[currentBigBlindSlot];

    uint smallBlindActual=0;
    uint bigBlindActual=0;

    //spiller har ikke råd til å betale hele blinden, greit nok, da punger man ut det man har.
    if (smallBlindPtr->getBalance() < HoldemConst::SmallBlind )
    {
        smallBlindActual = smallBlindPtr->getBalance();
    }
    else
        smallBlindActual = HoldemConst::SmallBlind;

    if (bigBlindPtr->getBalance() < HoldemConst::BigBlind)
    {
        bigBlindActual = bigBlindPtr->getBalance();
    }
    else
        bigBlindActual = HoldemConst::BigBlind;

    smallBlindPtr->incrementBet(smallBlindActual);
    smallBlindPtr->decrementBalance(smallBlindActual);
    bigBlindPtr->incrementBet(bigBlindActual);
    bigBlindPtr->decrementBalance(bigBlindActual);

    //Oppdatere klienter
    ClientNotifications::NotifyBalanceChanged_BCSingle(server,localReps,currentSmallBlindSlot);
    ClientNotifications::NotifyBalanceChanged_BCSingle(server,localReps,currentBigBlindSlot);

    ClientNotifications::NotifyBetChanged_BCSingle(server,localReps,currentSmallBlindSlot);
    ClientNotifications::NotifyBetChanged_BCSingle(server,localReps,currentBigBlindSlot);

    //oppdatere potten og currentBid.
    if (bigBlindActual > smallBlindActual)
    {
        currentBid = bigBlindActual;
    }
    else
    {
        currentBid = smallBlindActual;
    }
    pot += bigBlindActual;
    pot += smallBlindActual;

}

//hjelp

short HoldemPokerGame::nextActivePlayer(uint tableslot)
{
    uint nextvalid = nextPlayer(tableslot);
    while ( !localReps.at(nextvalid)->getActiveState() || localReps.at(nextvalid)->getAllin() )
    {
        nextvalid = nextPlayer(nextvalid);
    }
    return nextvalid;
}

short HoldemPokerGame::nextPlayer(uint tableslot)       //hjelpefunksjon, finner ut hvilken spiller som er neste og returnerer denne. Gjør ikke selv endringer.
{
    /*list of what this function needs to cater for:
     * nextPlayer does not care whether or not the next player is a valid player or not, that's what nextActivePlayer is for.
     * each call to nextPlayer should return the next tableslot. So if there is a player at slot 1 and 3, then a call with tabelslot 1
     * should return 3.
     * If the function is called with tableslot equal to MAXSLOTS_AT_TABLE, then the next slot would start from 1 again. But like the
     * earlier example, if there are no players at say slot 1, then it should return 2.
     * If the nextslot calculated is the lastPlayerToPlaySlot, then the bool lastPlayerHasPlayed needs to be set to true.
     */

    auto it = localReps.upper_bound(tableslot);
    uint nextSlot = -1;

    if ( it == localReps.end() )//nextSlot is temp.begin()
        nextSlot = localReps.begin()->second->getTableSlot();
    else                //regular iteration
        nextSlot = it->second->getTableSlot();

    if (nextSlot == lastPlayerToPlaySlot )
        lastPlayerHasPlayed=true;

    return nextSlot;
}
//this function is similar to the nextPlayer, except it works in the reverse, and it does not modify lastPlayerHasPlayed.
short HoldemPokerGame::formerPlayer(uint tableslot)
{

    auto it = localReps.find(tableslot);
    uint formerSlot;

    if ( it->first == localReps.begin()->first )//i.e the former element will be the last element in the map.
        formerSlot = localReps.end()->first;
    else
    {
        it--;
        formerSlot = it->first;
    }
    return formerSlot;
}

short HoldemPokerGame::formerActivePlayer(uint tableslot)
{
    short formervalid = formerPlayer(tableslot);
    while (!localReps[formervalid]->getActiveState() || localReps[formervalid]->getAllin() )
    {
        formervalid = formerPlayer(formervalid);
    }
    return formervalid;
}

//sjekker om alle spillere untatt en har kastet hånden.
bool HoldemPokerGame::soleContenderCheck()
{
    ushort foldcount = 0;

    for ( auto player : localReps )
    {
        if (!player.second->getActiveState() )
            foldcount++;
    }

    if (localReps.size()-1 == foldcount)
        return true;
    else
        return false;
}

//finner ut om alle har gitt like mye til potten.
bool HoldemPokerGame::betMatched()
{
    for ( auto player : localReps )
    {
        if ( player.second->getActiveState() && !player.second->getAllin() )
        {
            if (player.second->getBet() != currentBid)
                return false;
        }
    }
    return true;
}

bool HoldemPokerGame::AllinSpecialCase()
{
    ushort allinCount = 0;
    ushort inactiveCount = 0;
    for ( auto player : localReps )
    {
        if ( player.second->getAllin() )
            allinCount++;
        if( !player.second->getActiveState() )
            inactiveCount++;

    }
    cout<<"allinCount: "<<allinCount<<" activeCount: "<<inactiveCount<<endl;
    if( allinCount+inactiveCount == localReps.size() )//dvs vi har en spesiell situasjon
        return true;
    else
        return false;
}

//hjelpefunksjoner av ymse slag

short HoldemPokerGame::findTableSlotViaSockNr(short socknr)
{
    auto it = find_if(localReps.begin(),localReps.end(), [=](std::pair<int,playerRepresentation*> obj) -> bool
    {
        return obj.second->getSocketNr() == socknr;
    } );

    if (it == localReps.end() )//objektet finnes ikke
    {
        cout<<"Fatal Error in game "<<holdemID<<" at function findTableSlotViaSockFd: Socket("<<socknr<<") is not represented."<<endl;
        abort();
    }
    else
    {
        return it->second->getTableSlot();
    }
}

int HoldemPokerGame::findSocketNrViaTableSlot(short tableslot)
{
    auto it = find_if(localReps.begin(),localReps.end(), [=](std::pair<int,playerRepresentation*> obj) -> bool
    {
        return obj.second->getTableSlot() == tableslot;
    } );
    if (it == localReps.end() )
    {
        cout<<"Fatal Error in game "<<holdemID<<" at function findSocketNrViaTableSlot: Tableslot("<<tableslot<<") does not exist locally."<<endl;
        abort();
    }
    else
    {
        return it->second->getSocketNr();
    }
}

playerRepresentation& HoldemPokerGame::findRepViaFd(short sockfd)
{
    auto it = find_if(localReps.begin(),localReps.end(), [=]( std::pair<int,playerRepresentation*> obj) -> bool
    {
        return obj.second->getSocketNr() == sockfd;
    });
    if (it == localReps.end() )
    {
        cout<<"Fatal Error in game "<<holdemID<<" at function findRepViaFd: sockfd("<<sockfd<<") does not exist locally."<<endl;
        abort();
    }
    else
    {
        return it->second->getRep();
    }
}

std::map<ushort,playerRepresentation*>& HoldemPokerGame::getReps()
{
    return this->localReps;
}

short HoldemPokerGame::getGameId()
{
    return holdemID;
}

std::string& HoldemPokerGame::getGameName()
{
    return gameName;
}


//public

//funksjoner knyttet til spilleres muligheter(fold ect).

//TODO: Håndterer ikke tilfeller hvor spiller ikke har råd til å være med videre på riktig vis, må håndteres.
void HoldemPokerGame::callRequest(short tableslot)
{

    if ( halt )
    {
        int parcel = GAMEHALTEDNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel) );
        return;
    }

    if ( tableslot == currentPlayerSlot)
    {
        //I så fall kjøres en check istedet.
        if (currentBid == 0)
        {
            checkRequest(tableslot);
        }
        else
        {
            uint remainingBid = (currentBid - localReps[tableslot]->getBet() );   //hvor mye denne spilleren må punge ut for å fortsette i spillet.
            uint remaingingBalance = localReps[tableslot]->getBalance();
            if (remainingBid >= remaingingBalance ) //dvs allIn;
            {
                localReps[tableslot]->setAllin(true);
                pot+=remaingingBalance;
                localReps[tableslot]->incrementBet(remaingingBalance);
                localReps[tableslot]->decrementBalance(remaingingBalance);
                ClientNotifications::NotifyAllIn_BCSingle( server,localReps,tableslot );
            }
            else //vanlig tilstand.
            {
                pot+=remainingBid;
                localReps[tableslot]->incrementBet(remainingBid);
                localReps[tableslot]->decrementBalance(remainingBid);

            }
            //klienter må gjøres oppmerksomme på vellykket call.
            ClientNotifications::NotifyPot_BC(server,localReps,pot);
            ClientNotifications::NotifyBalanceChanged_BCSingle(server,localReps,tableslot);
            ClientNotifications::NotifyBetChanged_BCSingle(server,localReps,tableslot);

            advanceGameState();
        }
    }
    else
    {
        int parcel = NOTYOURTURNNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel) );
    }

}

void HoldemPokerGame::AllinRequest(short tableslot)
{

    if ( halt )
    {
        int parcel = GAMEHALTEDNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel) );
        return;
    }

    if ( tableslot == currentPlayerSlot )
    {

        int bet = localReps[tableslot]->getBet();
        int balance = localReps[tableslot]->getBalance();
        int total = bet + balance;

        if( total > currentBid )//Allin summen er større enn currentbid, altså må currentBid økes.
        {
            int increment = total - currentBid;
            currentBid+=increment;
        }


        localReps[tableslot]->setAllin(true);

        pot+=balance;
        localReps[tableslot]->incrementBet(balance);
        localReps[tableslot]->decrementBalance(balance);

        ClientNotifications::NotifyAllIn_BCSingle(server,localReps,tableslot);
        ClientNotifications::NotifyPot_BC(server,localReps,pot);
        ClientNotifications::NotifyBalanceChanged_BCSingle(server,localReps,tableslot);
        ClientNotifications::NotifyBetChanged_BCSingle(server,localReps,tableslot);

        advanceGameState();
    }
    else
    {
        int parcel = NOTYOURTURNNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel));
    }

}

void HoldemPokerGame::checkRequest(short tableslot)
{

    if ( halt )
    {
        int parcel = GAMEHALTEDNOTIFY;
        server.sender(localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel));
        return;
    }

    if ( tableslot == currentPlayerSlot )
    {

        //bekreft at det faktisk er mulig å checke
        if (currentBid == 0 )
        {

            //godkjent, avanser spillets status.
            advanceGameState();
        }
        else
        {
            int parcel = {CHECKINVALIDNOTIFY};
            server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel) );
            return;
        }

    }
    else
    {
        int parcel = NOTYOURTURNNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel) );
    }

}

void HoldemPokerGame::raiseRequest(int raisevalue, short tableslot)
{

    if ( halt )
    {
        int parcel = GAMEHALTEDNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel) );
        return;
    }

    if (tableslot == currentPlayerSlot )
    {

        int remainingBid = (currentBid - localReps[tableslot]->getBet() );
        int totalIncrement = remainingBid+raisevalue;

        if ( raisevalue < HoldemConst::BigBlind )
        {
            int parcel[2] = { MINIMUMRAISEFAILURENOTIFY,HoldemConst::BigBlind };
            server.sender(localReps[tableslot]->getSocketNr(),parcel,sizeof(parcel));
            return;
        }
        //Hvis man ikke har råd til å høyne, skal ingenting skje.
        else if ( totalIncrement > localReps[tableslot]->getBalance() )
        {
            int parcel = PAUPERFAILURENOTIFY;
            server.sender(localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel));
            return;
        }

        pot+=totalIncrement;
        currentBid+=raisevalue;
        localReps[tableslot]->incrementBet(totalIncrement);
        localReps[tableslot]->decrementBalance(totalIncrement);
        //Hvis spiller nå er bankrupt, merk allin
        if (localReps[tableslot]->getBalance() == 0 )
        {
            localReps[tableslot]->setAllin(true);
            ClientNotifications::NotifyAllIn_BCSingle(server,localReps,tableslot);
        }
        //Klienter må bli gjort oppmerksomme på at denne spilleren høynet.
        ClientNotifications::NotifyBalanceChanged_BCSingle(server,localReps,tableslot);
        ClientNotifications::NotifyBetChanged_BCSingle(server,localReps,tableslot);
        //potten er endret
        ClientNotifications::NotifyPot_BC(server,localReps,pot);

        advanceGameState();
    }
    else
    {
        int parcel = NOTYOURTURNNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel));
    }
}

//Folds er lovlig så lenge spiller har kontrollen.
void HoldemPokerGame::foldRequest(short tableslot)
{

    if ( halt )
    {
        int parcel = GAMEHALTEDNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel));
        return;
    }

    if ( tableslot == currentPlayerSlot )
    {
        foldCount++;
        localReps[tableslot]->setInActive();
        //gjøre alle oppmerksom på at denne spilleren har gitt seg.
        ClientNotifications::NotifyPlayerFolded_BCAll(server,localReps,tableslot);

        advanceGameState();
    }
    else
    {
        int parcel = NOTYOURTURNNOTIFY;
        server.sender( localReps[tableslot]->getSocketNr(),&parcel,sizeof(parcel));
    }
}

//øvrige offentlige funksjoner

void HoldemPokerGame::reimburse()
{
    //iterer gjennom Reps, tilbakebetaler totalBet til balance slik
    for(auto player : localReps)
    {
        uint playerTotalBet = player.second->getTotalBet();
        player.second->incrementBalance( playerTotalBet );
        pot -= playerTotalBet;
        player.second->resetTotalBet();
        player.second->resetBet();
    }
    //som en appropos, hva skjer hvis en spiller har forlatt spillet med penger i spill? Hvem får de pengene?
    //vanligvis regnes pengene som free money, dvs noe som fordeles på potene. Men siden spillet ikke rekker å ferdigstilles,
    //går de til "banken", slettes altså. Kall det en "house advantage".
    manager->saveProfiles();
}

void HoldemPokerGame::handleLeavingPlayer(short sockfd)
{
    this->DClockManager->lock();
    cout<<"STARTING handle"<<endl;
    short tableslot = findTableSlotViaSockNr(sockfd);

    //Spiller må merkes inaktiv
    localReps[tableslot]->setInActive();

    //Må så også merkes "leaving"//erstatter DCbacklog
    localReps[tableslot]->setLeavingState();
    //gjør lokale spillere oppmerksom på endringen.
    ClientNotifications::NotifyPlayerLeftLocalRoom_BCAll(server,localReps,tableslot);

    //Hvis spiller ved en tilfeldighet er currentPlayer
    //Den andre tilstanden er en spesialtilstand, hvis en av to spillere kobler fra, må advancegamestate kalles selv om den spilleren
    //ikke er current player.
    if ( currentPlayerSlot == tableslot || localReps.size() <= 2 )
        advanceGameState();

    cout<<"ENDING handle"<<endl;
    this->DClockManager->unlock();
}

//Gjør spiller såvell som andre spillere oppmerksomme på the newguy.
//nytt pga profilemanager: sender rett etterpå en namenotify da spillere må ha en brukerkonto med tilhørende navn.
void HoldemPokerGame::welcomeProtocol(int newfd)
{
    //vi må først legge til den nye spilleren i lokalt Reps register, henter samtidig den nye spillerens tableslot.
    playerRepresentation& rep = this->findRepViaFd(newfd);

    //newguys data må konverteres til riktig format før vi kan sende.
    vector<int> newguyNameNotifyParcel;
    string username = rep.getName();

    newguyNameNotifyParcel.push_back( TABLESLOTCHANGEDNAMENOTIFY );
    newguyNameNotifyParcel.push_back( rep.getTableSlot() );
    newguyNameNotifyParcel.push_back( (int)true );
    newguyNameNotifyParcel.push_back( username.size() );

    //slik bygger vi opp en fungerende namenotify melding.
    for(uint i = 0; i< username.size(); i++)
    {
        //newguyNameNotifyParcel[i+4] = username[i];
        newguyNameNotifyParcel.push_back( username[i] );
    }

    //Så gjør vi newguy klar over dens egne data//opcode-tableslot-trulyNew-local
    int newguyTableNotifyParcel[4] = { TABLESLOTNOTIFY,rep.getTableSlot(),(int)true,int(true) };
    server.sender(newfd,newguyTableNotifyParcel,sizeof(newguyTableNotifyParcel));
    server.sender(newfd,newguyNameNotifyParcel.data(),sizeof(int) * newguyNameNotifyParcel.size() );

    //we'll be reusing this parcel when we send it to the others later, but the last argument must be changed to false or we get displayerrors.
    newguyTableNotifyParcel[0] = TABLESLOTNOTIFY;
    newguyTableNotifyParcel[1] = rep.getTableSlot();
    newguyTableNotifyParcel[2] = (int) true;
    newguyTableNotifyParcel[3] = (int) false;

    //Så sender vi disse data til samtlige spillere i Reps, newguy inkludert. Må huske å sende en tableslotnotify først.
    for (auto player : localReps)
    {
        /*hvis socketNr er ulikt newfd, ser vi på en oldguy, dvs allerede innlogget spiller, da må vi gjøre newguy oppmerksom på dennes
        data. Disse må og tilpasses protocollen.*/
        if ( player.second->getSocketNr() == newfd )
            continue;

        //opcode-tableslot-silent-namelength-character(1-20)
        string oldguyname = player.second->getName();

        vector<int> oldguyNameNotifyParcel;
        oldguyNameNotifyParcel.push_back( TABLESLOTCHANGEDNAMENOTIFY );
        oldguyNameNotifyParcel.push_back( player.second->getTableSlot() );
        oldguyNameNotifyParcel.push_back( (int)true );
        oldguyNameNotifyParcel.push_back( oldguyname.size() );


        //This loop merely serves to push the old guys name onto the vector.
        for(uint i = 0; i < oldguyname.size(); i++)
            oldguyNameNotifyParcel.push_back( oldguyname[i] );

//THIS IS WHERE THE BUG OCCURED! COMMENTED CODE IS THE UNUPDATED CODE THAT CAUSED THE DEBUGGER MALFUNCTION
            /*for(uint i = 0; i < oldguyname.size();i++ )
                oldguyNameNotifyParcel[i+4] = oldguyname[i];//obviously this would cause a problem.*/
//This is a typical human brainfart, I simply forgot to update the loop to accomodate the new data type.

        //husk at newguy trenger info om oldguy's tableslot først, må konstrueres.
        int oldguyTableNotifyParcel[4] = { TABLESLOTNOTIFY,player.second->getTableSlot(),(int)false,(int)false };

        //send data til newguy om oldguy
        server.sender(newfd,oldguyTableNotifyParcel,sizeof(oldguyTableNotifyParcel));
        server.sender(newfd,oldguyNameNotifyParcel.data(),sizeof(int) * oldguyNameNotifyParcel.size() );

        //send data om newguy til oldguy
        server.sender(player.second->getSocketNr(),newguyTableNotifyParcel,sizeof(newguyTableNotifyParcel));
        server.sender(player.second->getSocketNr(),newguyNameNotifyParcel.data(),sizeof(int) * newguyNameNotifyParcel.size() );
    }
    //ok, da er samtlige i det minste klar over newguys eksistens, og newguys er klar over de andres, da må data sendes rundt angående newguy.
    //heldigvis har vi hjelpefunksjoner i holdem som gjør dette litt renere.
    ClientNotifications::NotifyBalanceChanged_BCSingle( server,localReps,rep.getTableSlot() );
    ClientNotifications::NotifyBetChanged_BCSingle( server,localReps,rep.getTableSlot() );
}

void HoldemPokerGame::tranfer_From_Escrow_Request(short fd,uint refill)
{
    //Denne funksjonen kan kalles når som helst, hvor som helst.
    playerRepresentation& rep = this->findRepViaFd(fd);
    bool success = rep.transfer_to_pendingBalance(refill);

    //I tillegg, hvis spillet for øyeblikket er i halt status, må startGame kalles.
    if ( halt )
    {
        if (success)
            newRound();
    }

    //Uansett, spiller må få beskjed om utkomme av forespørselen.
    if (success)
    {
        int parcel = VALIDCOMMAND;
        server.sender(fd,&parcel,sizeof(parcel));
    }
    else
    {
        int parcel = INVALIDCOMMAND;
        server.sender(fd,&parcel,sizeof(parcel));
    }

}

void HoldemPokerGame::increaseEscrowCheat(short fd,uint amount)
{
    playerRepresentation& rep = this->findRepViaFd(fd);
    rep.increment_Escrow(amount);
    int parcel = VALIDCOMMAND;
    server.sender(fd,&parcel,sizeof(parcel));
}
