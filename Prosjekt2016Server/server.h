#ifndef SERVER_H
#define SERVER_H

//common
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <sstream>
#include <fcntl.h>

#include <signal.h>

#include <map>
#include <vector>
#include <memory>
#include <thread>
#include <stack>
#include <list>

//linux only
#ifdef __unix__
    #include <unistd.h>
    #include <sys/socket.h>
    #include <sys/mman.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <sys/resource.h>
    #include <netdb.h>
#endif

//windows only
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#pragma comment(lib, "Ws2_32.lib")
#include <WinSock2.h>
#include <WS2tcpip.h>
#endif


#include "playerrepresentation.h"
#include "profileManager/profilemanager.h"

class HoldemPokerGame;

class Server
{

private:
    //variables used for networking.
    fd_set read_fds;
    fd_set master;
    int listener;
    int designatedListeningPort;//designated port. Usually also the one in use, can differ if the server was already listening when set port was called
    int currentListeningPort;//one currently in use
    int fdmax;
    addrinfo hints,*ptr,*result,*ai;
    #ifdef _WIN32
        WSADATA wsd;
        bool first;
        fd_set serversideTunnelSockets;
        SOCKET serversideTunnelSocket;//this is the socket the server knows for the "tunnel".
        SOCKET tunnelSocket;//this socket is what the client knows the server as. Imperative we keep these straight.
        std::mutex consoleLock;
    #endif

    //variables used to handle game logic, config loading, hard storage and threading.
    std::stack<int> AvailableGameSlotStack;

    std::vector<std::string> cfg_args;
    std::list<HoldemPokerGame> games;//erstatter gameptr. hvert enkelt spillers fd er registrert i repmap,
                                        //enten som -1(ikke assosiert med et bord), eller gameID >= 0(et aktivt bord).
                                        //hver enkelt pokergame har en ID, som også er indeks inn i games.
    std::map<short,playerRepresentation> repmap;//erstatter fdmap, nøkkel er fd.
    std::unique_ptr<ProfileManager> managerOwner;
    std::thread::id mainThreadID;
    std::unique_ptr<std::mutex> cleanupLock;


public:
    Server();

    void sender(int fd, int data[], uint bufferSize);//siden send() ikke er garantert å faktisk sende alle dataene med engang, må vi
                                                    //ha en funksjon som ettersender hvis OSetav en eller annen grunn bestemmer seg
                                                    //for å ettersende resten av dataene.

    short addGame(std::string &name, std::string &psw);//legger et nytt holdemgame til på games, henter først en holdemID fra AvailableGameSlotStack, returner holdemID til det nye spillet
    void removeGame(short gameid);//stikk motsatt

    const std::thread::id& getMainThreadId();//brukes i showdown for å avgjøre om det er mainthread som startet showdownthread eller om det var en DCthread.

private:
    //private getters
    playerRepresentation& getRepViaFd(short sockfd);//leter seg fram til en spesifik Rep ved å bruke fdmap kombinert med findRepViaFd i holdempokergame.
    HoldemPokerGame& getGameViaFd(short sockfd);//Finner et spesifikt holdemgame via en klients socketnr, vil feile hvis spilleren ikke er tilkoblet noe bord
    HoldemPokerGame& getGameViaGameID(short gameid);//samme som over,bortsett fra at den finner game direkte via gameiden

    void chatHelper(short sockfd,std::vector<int> command);//hjelpefunksjon, kalles to steder i processCommand

    //øvrige
    void stopServer();//function that can be used to stop the server, and reset it completely back to a default state, ie as if the server was never started in the first place
    void* get_in_addr(sockaddr *sa);
    bool isListening();//Returns true if the server is already listening for incoming connections.
    void sockInit();//initializes a listener socket so we can accept incoming connections.
    void storageInit();//initializes the storage medium, whatever type that may be. returntype denotes whether the profiler was already running or not
    void processUserInput(std::string &inputstring);        //function called from within the process loop to handle user input.
    void mainProcessLoop();
    void processRequest(std::vector<int> command, int fd);
    void disconnectProtocol(int fd,bool silent);   //protocol used to clean up once a client disconnects. The client can be in several valid states, such as logged in,
                                                            //in a game, or just lobbying. This protocol might also be called to forcibly evict clients in case of server shutdown

    //windows only
#ifdef _WIN32
    //used in relation to reimplementing syncronized console input on windows.
    void closeListenerSocket(SOCKET currentListener);
    void setupSocketTunnel();//Used to setup the socket connection enabling us to get console input that is syncronized with the select call.
#endif
};

#endif // SERVER_H
