#ifndef POTDATA_H
#define POTDATA_H

#include "protocolenums.h"
#include "playerrepresentation.h"

#include <vector>
#include <string>

class PotData
{
    uint LocalIndex;     //bokstavelig talt indexen Poten har i potWinners
    uint betValue; //mengden man ha spyttet inn for å være med.
    uint contesterCount; //antallet contesters, minimum 1
    uint excess;    //sidepotters summer skal ikke innholde verdiene til de mer signifikante pottene før dem, de skal altså trekkes fra.
                    //Underforstått: Mainpot har ingen excess.

    uint freemoney; //ekstra penger fordelt fra inaktive spillere.
    std::vector<playerRepresentation*> contesterPointers; //peker til spillerne som spiller om potten
public:
    PotData( uint bet,uint index );

    void addContestant(playerRepresentation* player);    //inkrementerer contesters, og legger sender pekeren videre til contesterPointers.

    uint getBetValue();         //henter ut betValue
    uint calculateSum();              //regner ut sum basert på betValue,excess og contesters
    uint getContestantCount();   //henter ut antall contesters.
    uint getFreeMoney();
    uint getIndex();

    void setExcess(uint ref);
    void incrementFreemoney(uint ref);

    std::vector<playerRepresentation*>& getContesterPointers();   //returnerer en peker vektoren med pekere(hold tunga rett i kjeften her).
};

#endif // POTDATA_H
