#include "configloading.h"

#include <iostream>
#include <fstream>
#include <stdio.h>

#include "protocolenums.h"

using namespace std;

std::vector<std::string> ConfigLoading::loadConfig()
{
    ifstream input;
    vector<string> config;
    //attempt to open config.cfg
    input.open("config.cfg");

    //check if config.cfg didn't already exist, create if that isn't the case and return empty vector to indicate the need for human intervention.
    if (!input.is_open() )
    {
        cout<<"config.cfg does not exist, creating with default values."<<endl;
        ofstream output("config.cfg");

        if (!output.is_open() )
        {
            cout<<"fatal error: cannot create file: config.cfg"<<endl;
            abort();
        }

        //These values are effectively placeholders, they will have to be manually set by the user by manipulating the config.cfg file.
        output <<"File"<<tabGenerator("File")<<DBConst::Storage_Type_description<<endl;
        output <<DBConst::defaultDB_IP<<tabGenerator(DBConst::defaultDB_IP)<<DBConst::IP_description<<endl;
        output <<DBConst::defaultDB_PORT<<tabGenerator(DBConst::defaultDB_PORT)<<DBConst::PORT_description<<endl;
        output <<DBConst::defaultDB_USER<<tabGenerator(DBConst::defaultDB_USER)<<DBConst::DBUSER_description<<endl;
        output <<DBConst::defaultDB_PASS<<tabGenerator(DBConst::defaultDB_PASS)<<DBConst::DBPASS_description<<endl;
        output <<"true"<<tabGenerator("true")<<DBConst::autostart_description<<endl;
        output.close();
        //input will be reused shortly, but it's currently not pointing to any file, reset it to so it points to the newly created file.
        input.open("config.cfg");
    }
    string temp;

    for (uint i = 0; i<6; i++)
    {
        std::getline(input,temp);
        config.push_back(temp);
    }

    input.close();

    //we need to sanitize the input first.
    for (uint i = 0; i < config.size(); i++ )
    {
        uint pos = config[i].find_first_of(" \t",0);
        config[i] = config[i].substr(0,pos);
    }

    //in case the user just runs the program again, thus the config file has the default values in it, then we must catch that.
    if (config[0] == "File" || config[0] == "DB")//If it's File, we don't care about the remaining values.
    {
        if (config[0] == "DB")
        {
            if (config[1] == "NONE")//Nice to catch if the user forgot to set the IP, it's a probable human error.
                                    //Other, more severe, errors are not catched.
            {
                cout<<"Please set the IP, port, database username and password and restart the program."<<endl;
                abort();
            }
        }
    }
    else//something other than DB or File has been specified as the storage type, tell the user off.
    {
        cout<<config[0] <<" is not a valid storage type. Only \"DB\" or \"File\" are allowed."<<endl;
        abort();
    }
    return config;
}

std::string ConfigLoading::tabGenerator(const std::string& str)
{
    if (str.size() < 8 )
        return "\t\t";
    else
        return "\t";
}
