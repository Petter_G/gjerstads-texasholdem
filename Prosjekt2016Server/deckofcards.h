#ifndef DECKOFCARD_H
#define DECKOFCARD_H

#include "card.h"

#include <vector>
#include <stack>

class deckofcards
{
    static const short decksize = 52;
    std::vector<card> cards;
    std::stack<card,std::vector<card> > cardstack;

public:
    deckofcards();
    void shuffle();
    card dealcard();
    int getStackSize();
    void printcards();
};

#endif
