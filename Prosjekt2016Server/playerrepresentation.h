#ifndef PLAYERREPRESENTATION_H
#define PLAYERREPRESENTATION_H

#include "card.h"

#include <vector>

class playerRepresentation
{

private:
    std::vector<card> playerhand;
    std::string playerName;
    short cookieid;
    short tableslot;//kunne brukt socketfd, men da hadde bord numrene blitt rare. Bedre om pokergame ikke bryr seg om underliggende protokoller overhodet.
    short gameid;
    short socketfd;
    uint bet;
    uint totalbet;
    uint playerBalance;
    uint pendingBalance;    //penger som er overført fra escrow, men som ikke tilgjengelig før neste parti.
    uint escrow;         //penger på spillers konto, men ikke brukt i spillerens buyin.

    bool active;//brukes ved folds
    bool leaving;//merkes true av dissconnectprotocol, brukes ved renskningen av Reps ved starten av hver runde.
    bool ready;//brukes ved oppstart av spill.
    bool allin;//Brukes når en spiller går allin.

public:

    playerRepresentation(int fd, uint cookie, std::string playername, uint balance);//standard, setter ikke navn, eller balanse, brukes under opprettelsen i server.

    void resetToLounger();  //brukes når en spiller forlater et rom. Reseter relevante felter.
    void setName(std::string name);
    std::string getName();
    void addCard(card x);
    void dumpCards();


    uint getCookieId();

    void resetBet();
    void resetTotalBet();
    void incrementBet(int inc);

    void setBalance(int balance);
    void decrementBalance(int dec);
    void incrementBalance(int inc);

    int getBet();
    int getTotalBet();
    int getBalance();

    void setAllin(bool state);
    bool getAllin();

    void setInActive();
    void setActive();
    bool getActiveState();

    void setReady();
    void clearReady();
    bool getReadyState();

    void setTableSlot(short tblnr);
    short getTableSlot();
    short getSocketNr();

    void setGameId(short id);
    short getGameId();

    bool getLeavingState();
    void setLeavingState();

    uint getEscrow();                               //vanlig get
    void increment_Escrow(uint money);              //inkrementerer escrow
    bool transfer_to_pendingBalance(uint amount);   //overfører penger fra escrow, til balance. returnerer true hvis handlingen var vellykket(hadde nok i escrow)
    void resolvePendingBalance();                   //Brukes av startGame, overfører alt i pendingBalance til playerBalance

    playerRepresentation& getRep();           //returnerer en constant referanse til objektet.
    std::vector<card>& getHand();
};

#endif // PLAYERREPRESENTATION_H
