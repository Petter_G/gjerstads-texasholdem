TEMPLATE = app

CONFIG += debug_and_release
#CONFIG += build_all

DEFINES+=SRCDIR=\\\"$$PWD\\\"

win32 {
    if(win32):CONFIG(debug,debug|release) {
        message(debug on windows)
		DESTDIR = $$PWD/debug
		TARGET = G_Holdem_Serv_d
    }
    else:CONFIG(release,debug|release) {
        message(release on windows)
		DESTDIR = $$PWD/release
		TARGET = G_Holdem_Serv
    }

}
unix {
	if(linux-g++*):CONFIG(debug,debug|release) {
        message(debug on Unix)
		DESTDIR = $$PWD/debug
		TARGET = G_Holdem_UNIXServ_d
    }
	else:CONFIG(release,debug|release) {
        message(release on Unix)
		TARGET = G_Holdem_UNIXServ
		DESTDIR = $$PWD/release
    }
}

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

unix: LIBS += -pthread
unix: LIBS += -lrt

unix: QMAKE_CXXFLAGS += -std=c++11
unix: QMAKE_CXXFLAGS += -std=c++11 -pthread

win32: INCLUDEPATH += $$PWD/include/cppconn
win32: DEPENDPATH += $$PWD/include

win32: LIBS += -L$$PWD/lib/ -lmysqlcppconn

SOURCES += \
    holdempokergame.cpp \
    pokerevaluatorengine.cpp \
    card.cpp \
    deckofcards.cpp \
    protocolenums.cpp \
    playerrepresentation.cpp \
    scoreobject.cpp \
    UNIXserver.cpp \
    WINserver.cpp \
    potdata.cpp \
    exceptions/mapdoesnotcontainobjectexception.cpp \
    exceptions/communityemptyexception.cpp \
    profileManager/profilemanager.cpp \
    profileManager/profile.cpp \
    profileManager/fileprofilemanager.cpp \
    profileManager/dbprofilemanager.cpp \
    clientnotifications.cpp \
    configloading.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    server.h \
    holdempokergame.h \
    pokerevaluatorengine.h \
    card.h \
    deckofcards.h \
    protocolenums.h \
    playerrepresentation.h \
    potdata.h \
    scoreobject.h \
    exceptions/mapdoesnotcontainobjectexception.h \
    exceptions/communityemptyexception.h \
    profileManager/profilemanager.h \
    profileManager/profile.h \
    profileManager/fileprofilemanager.h \
    profileManager/dbprofilemanager.h \
    clientnotifications.h \
    configloading.h
