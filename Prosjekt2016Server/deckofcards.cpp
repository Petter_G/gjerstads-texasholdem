#include "deckofcards.h"
#include "card.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <random>
#include <ctime>

using namespace std;

    vector<card> cards;
    stack<card,vector<card> > cardstack;

    deckofcards::deckofcards()									//stokket kortstokk.
    {
        for (int i = 0; i < decksize/4; ++i)
        {
            cards.emplace_back(card(0,i+1));
            cards.emplace_back(card(1,i+1));
            cards.emplace_back(card(2,i+1));
            cards.emplace_back(card(3,i+1));
        }

        srand(unsigned ( time(NULL) ) );
        random_shuffle(cards.begin(),cards.end());

        for (int i = 0; i < decksize; ++i)
            cardstack.push(cards[i]);

    }

    void deckofcards::shuffle()										//stokker om, gjør at man kan gjenbruke hele objektet framfor å lage et nytt.
    {																//Uten denne får vi (omsider)segfault.
        while (cardstack.empty() != true)
            cardstack.pop();

        random_shuffle(cards.begin(),cards.end());
        for (int i = 0; i < decksize; ++i)
            cardstack.push(cards[i]);

    }

    card deckofcards::dealcard()									//trekker et kort fra kortstokken. og sender det til en hånd.
    {
        card card = cardstack.top();
        cardstack.pop();
        return card;
    }

    int deckofcards::getStackSize()
    {
        return cardstack.size();
    }

    void deckofcards::printcards()								//debugger metode. Gjør kortstokken ubrukelig!
    {
        stack<card,vector<card> > tempstack;
        for (int i = 0; i < 52; ++i)
        {
            card& tempcard = cardstack.top();
            cardstack.pop();
            cout<<i<<"\t"<<"type: "<<tempcard.getType()<<"\tvalue: "<<tempcard.getValue()<<endl;
            tempstack.push(tempcard );
        }
    }


