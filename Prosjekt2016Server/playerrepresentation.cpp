#include "playerrepresentation.h"
#include "protocolenums.h"

#include <string>
#include <stdio.h>
#include <iostream>

using namespace std;

playerRepresentation::playerRepresentation(int fd, uint cookie,std::string playername,uint balance)
{
    cookieid = cookie;
    socketfd=fd;
    //tablenr=tablenrtemp;
    gameid=-1;//default verdi, endres når klient entrer ett spill.
    tableslot=-1;
    //static short unknownnamecounter = 0;
    playerName=playername;
    //unknownnamecounter++;
    bet = 0;
    totalbet=0;
    escrow=0;
    pendingBalance=0;
    playerBalance=balance;

    leaving=false;
    ready=false;
    active= false;
    allin=false;

}

void playerRepresentation::resetToLounger()
{
    gameid=-1;
    tableslot=-1;
    bet=0;
    totalbet=0;

    leaving=false;
    ready=false;
    active=false;
    allin=false;
}

void playerRepresentation::setName(string name)
{
    playerName = name;
}

string playerRepresentation::getName()
{
    return playerName;
}

void playerRepresentation::addCard(card x)
{
    playerhand.push_back(x);
}
void playerRepresentation::dumpCards()
{
    playerhand.clear();
}

uint playerRepresentation::getCookieId()
{
    return cookieid;
}

void playerRepresentation::resetBet()
{
    bet = 0;
}

void playerRepresentation::resetTotalBet()
{
    totalbet=0;
}

void playerRepresentation::incrementBet(int inc)
{
    bet += inc;
    totalbet += inc;
}

void playerRepresentation::setBalance(int balance)
{
    playerBalance = balance;
}

void playerRepresentation::decrementBalance(int dec)
{
    playerBalance -= dec;
}

void playerRepresentation::incrementBalance(int inc)
{
    playerBalance += inc;
}

int playerRepresentation::getBet()
{
    return bet;
}

int playerRepresentation::getTotalBet()
{
    return totalbet;
}

int playerRepresentation::getBalance()
{
    return playerBalance;
}

void playerRepresentation::setAllin(bool state)
{
    allin=state;
}

bool playerRepresentation::getAllin()
{
    return allin;
}

void playerRepresentation::setInActive()
{
    active=false;
}

void playerRepresentation::setActive()
{
    active = true;
}

bool playerRepresentation::getActiveState()
{
    return active;
}

void playerRepresentation::setReady()
{
    ready= true;
}

void playerRepresentation::clearReady()
{
    ready= false;
}

bool playerRepresentation::getReadyState()
{
    return ready;
}

short playerRepresentation::getSocketNr()
{
    return socketfd;
}

void playerRepresentation::setGameId(short id)
{
    if (id > ServConst::MAXTABLES)//noe er riv ruskende galt
    {
        cout<<"Fatal error in setGameId: id("<<id<<") is higher than ServVars::MAXTABLES"<<endl;
        abort();
    }
    gameid = id;
}

short playerRepresentation::getGameId()
{
    return gameid;
}

void playerRepresentation::setTableSlot(short tblnr)
{
    tableslot=tblnr;
}

short playerRepresentation::getTableSlot()
{
    if (tableslot < -1 || tableslot > 8) //glemt å initilisere sannsynligvis, uansett en bug.
    {
        cout<<"fatal error in getTableSlot: tableslot("<<tableslot<<") has nonsensical value"<<endl;
        abort();
    }
    return tableslot;
}

bool playerRepresentation::getLeavingState()
{
    return leaving;
}

void playerRepresentation::setLeavingState()
{
    leaving=true;
    /*if (leaving)//funksjonen har blitt kalt en gang før, noe er galt
    {
        cout<<"fatal error: this user "<<this->getName()<<" is already marked as disconnecting"<<endl;
        abort();
    }
    else
        leaving=true;*/
}

uint playerRepresentation::getEscrow()
{
    return escrow;
}

void playerRepresentation::increment_Escrow(uint money)
{
    escrow += money;
}

bool playerRepresentation::transfer_to_pendingBalance(uint amount)
{
    if (amount > escrow)
        return false;

    pendingBalance += amount;
    escrow -= amount;
    return true;
}

void playerRepresentation::resolvePendingBalance()
{
    playerBalance += pendingBalance;
    pendingBalance = 0;
}

playerRepresentation& playerRepresentation::getRep()
{
    return *this;
}

std::vector<card> &playerRepresentation::getHand()
{
    return playerhand;
}
