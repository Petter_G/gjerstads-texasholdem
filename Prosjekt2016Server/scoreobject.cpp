#include "scoreobject.h"

ScoreObject::ScoreObject(playerRepresentation& repPtr) : ref(repPtr)
{
    primary=0;
    secondary=0;
    tertiary=0;
    quaternary=0;
}

void ScoreObject::setPrimary(short val)
{
    primary=val;
}

void ScoreObject::setSecondary(short val)
{
    secondary = val;
}

void ScoreObject::setTertiary(short val)
{
    tertiary = val;
}

void ScoreObject::setQuaternary(short val)
{
    quaternary=val;
}

short ScoreObject::getPrimary()
{
    return primary;
}

short ScoreObject::getSecondary()
{
    return secondary;
}

short ScoreObject::getTertiary()
{
    return tertiary;
}

short ScoreObject::getQuaternary()
{
    return quaternary;
}

playerRepresentation& ScoreObject::getRepPtr()
{
    return ref;
}

