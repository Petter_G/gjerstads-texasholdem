#ifdef __unix__

//This implementation file is for use by unix compilers only. Tested with GCC on ubuntu.

#include "server.h"
#include "protocolenums.h"
#include "holdempokergame.h"
#include "profileManager/profilemanager.h"
#include "profileManager/fileprofilemanager.h"
#include "configloading.h"
#include "exceptions/mapdoesnotcontainobjectexception.h"

#include <mutex>
#include <algorithm>
#include <exception>

using namespace std;

Server::Server()
{
    //set up listeningport. Will as of this writing mean nothing more than the copying of the default port(ie 3500);
    designatedListeningPort = ServConst::DEFAULT_PORT;

    //fyller opp AvailableGameSlotStack
    for(short i = ServConst::MAXTABLES; i != 0; i--)
        AvailableGameSlotStack.push(i);

    //load config.cfg. variables loaded govern whether the server will start up and accept connections by itself or not
    cfg_args = ConfigLoading::loadConfig();

    //trenger trådIDer, kan ordne "mainthread" allerede her
    mainThreadID = std::this_thread::get_id();

    cleanupLock = std::unique_ptr<std::mutex> (new std::mutex);

    FD_ZERO(&master);
    FD_ZERO(&read_fds);
    fdmax=0;

    if (cfg_args[5] == "true")
    {
        cout<<"Autostart is on, starting server."<<endl;
        storageInit();
        sockInit();

    }
    else
        cout<<"AutoListen is NOT on, manual server start required."<<endl;

    mainProcessLoop();
}

void Server::sender(int fd, int buffer[], uint bufferSize)
{
    uint total = 0;
    uint bytesleft = bufferSize;
    uint count;

    while( total < bufferSize )//gjenta til alt er sendt.
    {
        count = send(fd,buffer+total,bytesleft,MSG_NOSIGNAL );
        if (count == -1)//betyr i praksis MSG_NOSIGNAL, bryt.
            break;

        total += count;
        bytesleft -= count;
    }
}

short Server::addGame(string& name,string& psw)
{
    if ( AvailableGameSlotStack.size() == 0)
        return -1;

    short id = AvailableGameSlotStack.top();
    AvailableGameSlotStack.pop();

    games.push_back( HoldemPokerGame( id,*this,managerOwner,name,psw ) );
    return id;
}

void Server::removeGame(short gameid)
{
    AvailableGameSlotStack.push(gameid);

    auto it = find_if(games.begin(),games.end(),[=]( HoldemPokerGame& obj) -> bool
    {
        return obj.getGameId() == gameid;
    });

    if (it == games.end() )//objektet finnes ikke
    {
        cout<<"Fatal error removing game with id "<<gameid<<": game doesn't exist."<<endl;
        abort();
    }
    else
    {
        games.erase(it);
    }
}

const std::thread::id& Server::getMainThreadId()
{
    return mainThreadID;
}

playerRepresentation& Server::getRepViaFd(short sockfd)
{
    auto it = repmap.find(sockfd);
    if (it != repmap.end() )
        return it->second;
    else
        throw MapDoesNotContainObjectException(sockfd);
}

HoldemPokerGame& Server::getGameViaFd(short sockfd)
{
    auto it = repmap.find(sockfd);

    if (it != repmap.end() )
    {
        playerRepresentation& rep = it->second;
        if (rep.getGameId() != -1)
        {
            return getGameViaGameID(rep.getGameId() );
        }
        else//error, sockfd finnes, men er ikke assosiert med noe spill
        {
            cout<<"fatal error in function getGameViaFd: klient's descriptor("<<sockfd<<") is registered, "
                "but does not seem to be associated with any game"<<endl;
            abort();
        }
    }
    else//error, sockfd er ikke registerert overhodet.
    {
        cout<<"fatal error in function getGameViaFd: Sockfd is not registered."<<endl;
        abort();
    }

}

HoldemPokerGame& Server::getGameViaGameID(short gameid)
{
    for (auto& game : games)
    {
        if (game.getGameId() == gameid )
            return game;
    }

    //kommer vi hit, er noe riv ruskende galt.
    cout<<"fatal error in function getGameViaGameID: the game specified("<<gameid<<") does not seem to exist."<<endl;
    abort();
}

void Server::chatHelper(short sockfd,std::vector<int> command)
{
    //husk, det som kommer inn her er en SHORTCHAT
    playerRepresentation& rep = getRepViaFd(sockfd);

    //lag en pakke
    vector<int> parcel;
    parcel.push_back(CHAT);
    parcel.push_back( rep.getGameId());
    parcel.push_back( sockfd);
    parcel.push_back( (int)command.size() - 1);

    for (uint i = 1;i<command.size();i++)
    {
        parcel.push_back(command[i]);
    }

    //finn ut hvorvidt det er en lounger eller ikke
    if (rep.getGameId() == -1)//aka lounger
    {
        parcel[1] = -1;
        //finn alle som er i lounge, og send til dem.
        for( auto it : repmap )
        {
            if (it.second.getGameId() == -1)
                sender(it.first,parcel.data(),sizeof(int) * (int)parcel.size() );
        }
    }
    else //aka in play
    {
        parcel[1] = rep.getTableSlot();
        //finn de som er same game, og send til dem
        for( auto it : repmap )
        {
            if (it.second.getGameId() == rep.getGameId() )
                sender(it.first,parcel.data(),sizeof(uint) * (int)parcel.size() );
        }
    }
}

//alle kommandoer kommer innom her.
void Server::processRequest(vector<int> request,int fd)
{
    int opcode = request[0];
    cout<<"opcode is: "<<opcode<<endl;
    auto it  = repmap.find(fd);

    //This first test is has the purpose of denying the player if he tries an action that is temporarily denied due to timeouts.
    if (it != repmap.end() )//player is in the repmap, ie already logged in.
    {
        playerRepresentation& rep = getRepViaFd(fd);
        if ( rep.getGameId() != -1 )//dvs klienten er med i et spill
        {
            HoldemPokerGame& game = getGameViaFd(fd);
            //Hvis server i skrivende stund er i pause modus, send en notis tilbake, returner.
            game.showdownLockManager->lock();
            if( game.do_not_process_requests )
            {
                if (request[0] == SHORTCHAT )//untak fra noprocess regelen
                {
                    chatHelper(fd,request);
                }
                else
                {
                    int dataparcel[1] = { GAMEPAUSEDNOTIFY };
                    sender(fd,dataparcel,sizeof(dataparcel));
                }
                game.showdownLockManager->unlock();
                return;
            }
            game.showdownLockManager->unlock();
        }
    }
    else //fd er ikke registrert i repmap, da er det enten snakk om en som prøver å logge inn, eller en bug.
    {
        if ( !(opcode == LOGIN || opcode == REGISTER) )
            cout<<"Error: Unlogged peer attempts to communicate via secured or undefined protocols. Ignoring"<<endl;

        switch (opcode)
        {
        case LOGIN:
        {
            ushort userlength = request[1];
            ushort passwordlength = request[2];

            //les så data inn i lokale variabler
            string username;
            string password;

            //username
            for(int i = 0; i< userlength; i++)
                username.push_back( request[3+i] );

            //password
            for(int i = 0; i< passwordlength; i++)
                password.push_back( request[3+i+userlength] );

            //forsøke å logge inn
            uint cookieid = managerOwner->loginAttempt(username,password);

            if ( cookieid != -1)//hvis cookieid = -1, misslykket, hvis ikke suksess.
            {
                //fyr av et authenticationsuccess signal
                cout<<"authentication success"<<endl;
                vector<int> success = { AUTHENTICATIONSUCCESSNOTIFY,(int)cookieid };
                sender(fd,success.data(),sizeof(int) * success.size() );

                //Ny klient må bli gjort oppmerksom på eksisterende games, if any. Dette gjøres tidlig da eksisterende spillere alt kan være
                //assosiert med dem, dvs, NEWPLAYERNOTIFY kan måtte trenge å henvise til disse spillene.
                for(auto& game : games)
                {
                    string gamename = game.getGameName();

                    vector<int> newgameParcel;
                    newgameParcel.push_back(NEWGAMENOTIFY);
                    newgameParcel.push_back( game.getGameId() );
                    newgameParcel.push_back( gamename.size() );

                    for( int i = 0; i < gamename.size();i++ )
                        newgameParcel.push_back( gamename[i] );

                    sender(fd,newgameParcel.data(),sizeof(int) * newgameParcel.size() );
                }

                //må fortelle samtlige klienter at en ny klient er å finne i lounge området.
                //int newguyparcel[4 + username.length()];
                vector<int> newguyparcel;

                newguyparcel.push_back( NEWPLAYERNOTIFY );
                newguyparcel.push_back( Misc::NEWGUY );
                newguyparcel.push_back( fd );
                newguyparcel.push_back( userlength );
                for( int i = 0; i < userlength; i++ )
                    newguyparcel.push_back( username[i] );

                //vi må huske å oppdatere rep, kan opprette newguys sin rep allerede her.
                //vi setter balance her også.
                string name = managerOwner->getUsername(cookieid);
                uint balance = managerOwner->getBalance(cookieid);
                playerRepresentation rep(fd,cookieid,name,balance);

                //send så til alle
                for(auto it = repmap.begin(); it != repmap.end();it++)
                {
                    //send newguyparcel til oldguy, og oldguy parcel til newguy
                    playerRepresentation& oldguy = getRepViaFd(it->first);
                    string oldguyname = oldguy.getName();
                    //pakk oldguyparcel full av data.
                    vector<int> oldguyparcel;

                    oldguyparcel.push_back( NEWPLAYERNOTIFY );
                    oldguyparcel.push_back( oldguy.getGameId() );
                    oldguyparcel.push_back( oldguy.getSocketNr() );
                    oldguyparcel.push_back( oldguyname.length() );
                    for ( uint i = 0; i < oldguyname.length(); i++ )
                        oldguyparcel.push_back( oldguyname[i] );

                    //send så til begge parter
                    sender(it->first,newguyparcel.data(),sizeof(int) * newguyparcel.size());
                    sender(fd,oldguyparcel.data(),sizeof(int) * oldguyparcel.size() );
                }

                //så sender vi en pakke om newguy, til newguy.
                sender(fd,newguyparcel.data(),sizeof(int) * newguyparcel.size() );
                //nå som vi er ferdig med å oppdatere de gamle, bind fd til en splitter ny rep
                repmap.insert(std::pair<short,playerRepresentation>(fd,rep));
            }
            else
            {
                cout<<"authentication failure"<<endl;
                int failure = AUTHENTICATIONFAILURENOTIFY;
                sender(fd,&failure,sizeof(failure));
            }
            break;
        }
        case REGISTER:
        {
            ushort userlength = request[1];
            ushort passwordlength = request[2];

            //les så data inn i lokale variabler
            string username;
            string password;

            //lets get the username and password in more appropriate containers
            for(int i = 0; i< userlength; i++)
                username.push_back( request[3+i] );

            for(int i = 0; i< passwordlength; i++)
                password.push_back( request[3+i+userlength]);

            //forsøker her å legge til ny profil, hvis den alt eksisterer, dvs brukernavn finnes alt, returneres nullptr
            bool result = managerOwner->addProfile(username,password);

            if (!result)//IE profile already exists, or length is invalid.
            {
                cout<<"profile creation failed, already exists or wrong length."<<endl;
                int failureParcel = INVALIDPROFILECREATION;
                sender(fd,&failureParcel,sizeof(failureParcel));
            }
            else //opprettelsen var vellykket, send tilbakemelding
            {
                //great om bruker får bekreftelse på at opprettelsen faktisk var vellykket.
                int success = VALIDPROFILECREATION;
                sender(fd,&success,sizeof(success));
            }
            break;
        }
        }
    }

    //Bruker er logget inn, og kan kommunisere via øvrige protokoller.
    switch (opcode)
    {
        case SETREADY:
            {
                playerRepresentation& rep = this->getRepViaFd(fd);
                rep.setReady();
                cout<<"setready called by: "<<fd<<endl;
                break;
            }
        case CLEARREADY:
            {
                playerRepresentation& rep = this->getRepViaFd(fd);
                rep.clearReady();
                cout<<"ClearReady called by: "<<fd<<endl;
                break;
            }
        case SETNAME:
            {
                string newName;
                for(int i = 1; i < request.size(); i++)
                    newName.push_back((char)request[i] );

                playerRepresentation& rep = getRepViaFd(fd);
                bool result = managerOwner->changeUsername(newName,rep.getName() );

                if ( !result )//Name change was invalid due to invalid characters/or someone else already using it.
                {
                    int invalid = NAMECHANGEINVALIDNOTIFY;
                    sender( fd,&invalid,sizeof(invalid) );
                }
                else
                {
                    rep.setName(newName);

                    //Fylle opp og informere samtlige klienter om navnebytte
                    vector<int> dataparcel;
                    dataparcel.push_back( NAMECHANGEDNOTIFY );
                    dataparcel.push_back( rep.getSocketNr() );
                    dataparcel.push_back( (int)false);
                    dataparcel.push_back( rep.getTableSlot() );
                    dataparcel.push_back( newName.size() );

                    for(uint i = 0; i< newName.size();i++)
                        dataparcel.push_back( newName[i] );

                    for (auto it : repmap)
                        sender(it.first,dataparcel.data(),sizeof(int) * dataparcel.size() );

                    //i tillegg må rommet klienten er i, om noen, gjøres oppmerksom på at spilleren har skiftet navn.
                    if ( rep.getTableSlot() == -1 )//dvs er en lounger.
                        break;
                    else
                    {
                        //gjør klar pakka
                        vector<int> holdemparcel;
                        holdemparcel.push_back( TABLESLOTCHANGEDNAMENOTIFY );
                        holdemparcel.push_back( rep.getTableSlot() );
                        holdemparcel.push_back( (int)false );
                        holdemparcel.push_back( newName.size() );

                        for(uint i = 0; i< newName.size();i++)
                            holdemparcel.push_back( newName[i] );

                        //finn riktig spill, send så til alle reps i denne
                        HoldemPokerGame& game = getGameViaFd(fd);

                        for (auto& rep : game.getReps() )
                            sender( rep.second->getSocketNr(), holdemparcel.data(),sizeof(int) * holdemparcel.size() );
                    }
                }
                break;
            }
        case REFILL:
            {
                uint refill = request[1];
                getGameViaFd(fd).increaseEscrowCheat(fd,refill);
                break;
            }
        case TRANSFER:
            {
                uint transferamount = request[1];
                getGameViaFd(fd).tranfer_From_Escrow_Request(fd,transferamount);
                break;
            }
        case CHECK:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).checkRequest(tablenr);
                break;
            }
        case RAISE:
            {
                int raisevalue= request[1];
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).raiseRequest(raisevalue,tablenr);
                break;
            }
        case FOLD:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).foldRequest(tablenr);
                break;
            }
        case CALL:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).callRequest(tablenr);
                break;
            }
        case ALLIN:
            {
                short tablenr = this->getRepViaFd(fd).getTableSlot();
                getGameViaFd(fd).AllinRequest(tablenr);
                break;
            }
        case JOIN://opcode-gameid-usingPSW-pswCount-psw
        {
            //Sjekk om spiller alt er registrert ved et spill, i så fall send feilmelding.
            //spiller kan ikke gå inn i et nytt rom, før det forrige har ferdigbehandlet spillerens status.
            //alternativt er at koden knyttet til aktive med frakoblede spillere fjernes, noe som blant annet
            //vil si at hvis en disconnect etter å ha gått allin, så mister du hele veddemålet automatisk.

            cout<<"ENTERED JOIN: id:"<<request[1]<<endl;
            playerRepresentation& rep = getRepViaFd(fd);

            //Mandatory fields
            short gameid = request[1];
            bool usingPSW = (bool)request[2];

            //Optional fields
            short pswCount;
            string psw;

            //DEBUG
            cout<<"Entered JOIN, gameid:  "<<gameid<<"\tusingPSW: "<<usingPSW<<endl;

            //if usingPSW is true, load optional value(the password, or psw for short)
            if ( usingPSW )
            {
                //Load pswCount, then use it to load psw itself
                pswCount = request[3];

                for( int i = 0;i < pswCount;i++ )
                    psw.push_back( (char)request[i + 4] );
            }

            if (rep.getGameId() != -1) //dvs spiller er fortsatt formelt inne i et annet rom.
            {
                int joinfailureparcel = JOINFAILURENOTIFY;
                sender(fd,&joinfailureparcel,sizeof(joinfailureparcel));
                break;
            }

            cout<<"further JOIN data: "<<pswCount<<"\tpsw: "<<psw<<endl;
            HoldemPokerGame& game = getGameViaGameID(gameid);
            short tableslot = game.addPlayer( &rep,psw );

            if ( tableslot >= 0 )//tableslots are always 0 or higher, negative returns here are legal for -1 and -2, IE error messages related to faulty password or the room is full.
            {
                //ok, kjør welcomeprotocol.
                game.welcomeProtocol(fd);

                //spiller må bli gjort oppmerksom på at forespørselen ble godkjent, slik at den kan switche til holdem
                int parcel = JOINSUCCESSNOTIFY;
                sender(fd,&parcel,sizeof(parcel));

                //vi må oppdatere samtlige klienters model, slik at de vet at denne klienten nå er i et spill.
                for (auto it : repmap)
                {
                    int playerjoinedParcel[3] = { PLAYERJOINEDROOMNOTIFY,fd,game.getGameId() };
                    sender(it.first,playerjoinedParcel,sizeof(playerjoinedParcel));
                }
                //Kjør initGame, spillet vil starte hvis det er minst 2 gyldige spillere tilkoblet, hvis ikke skjer ingenting annet enn en feilmelding til spilleren.
                game.processNewPlayer(fd);
            }
            else if ( tableslot == HoldemConst::ROOMFULL)//betyr at det er for mange spillere i det rommet
            {
                int failureParcel = { INVALIDCOMMAND };
                sender(fd,&failureParcel,sizeof(failureParcel) );
            }
            else if ( tableslot == HoldemConst::FAULTYPSW )
            {
                //If usingPSW was true, then send a INVALIDROOMPSWNOTIFY, ie it was a login attempt with a faulty password. If false, then send a PSWPROTECTEDNOTIFY instead in order
                //to prompt the user to enter a password. In practice this means that a password dialog will be shown, and a new JOIN command will be sent, this time with a password
                //and usingPSW set to true
                if ( usingPSW )
                {
                    int wrongPSWParcel = {INVALIDROOMPSWNOTIFY};
                    sender(fd,&wrongPSWParcel,sizeof(wrongPSWParcel) );
                }
                else
                {
                    int PSWProtectedParcel[2] = {PSWPROTECTEDNOTIFY,gameid};
                    sender(fd,PSWProtectedParcel,sizeof(PSWProtectedParcel) );
                }
            }
            else //In case we get something totally meaningless back from addPlayer
            {
                cout<<"Fatal error in server::processCommand()->case JOIN"<<endl;
                abort();
            }
            break;
        }
        case CREATE:
        {
            //Declare mandatory fields first
            bool pswprotection;
            int nameCount;
            string gamename;

            //optionals
            int pswCount;
            string psw;

            //Then we load the room name
            nameCount = request[2];

            for (int i = 0; i < nameCount;i++)
                gamename.push_back( (char)request[i+3] );

            //Load pswprotection, which will be used to load the psw field if applicable.
            pswprotection = (bool)request[1];

            if (pswprotection )//If true, then the user specified that the room should be psw protected
            {
                pswCount = request[3 + nameCount];
                for (int i = 0; i < pswCount;i++)
                    psw.push_back((char)request[i + 4 + nameCount] );
            }

            //now we check if the length(s) are within the acceptable interval
            if ( !(gamename.length() >= Misc::MIN_NAMELENGTH && gamename.length() <= Misc::MAX_NAMELENGTH) )
            {
                int failureParcel = {INVALIDROOMNAMELENGTHNOTIFY};
                sender(fd,&failureParcel,sizeof(failureParcel) );
                break;
            }

            //checking if the gamename is already in use, if discovered, send a error, and then return
            for (auto& game : games)
            {
                if ( game.getGameName().compare(gamename) == 0 )
                {
                    int existsParcel = { INVALIDROOMNAMEEXISTSNOTIFY };
                    sender(fd,&existsParcel,sizeof(existsParcel) );
                    return;
                }
            }

            //similarily for psw, if applicable
            if (pswprotection)
            {
                if ( !(psw.length() >= Misc::MIN_PASSWORDLENGTH && psw.length() <= Misc::MAX_PASSWORDLENGTH) )
                {
                    int failureParcel = { INVALIDROOMPSWLENGTHNOTIFY };
                    sender(fd,&failureParcel,sizeof(failureParcel) );
                    break;
                }
            }

            //data loaded, now check if we've reached the maximum room count, if yes, send error message and break
            short id = addGame(gamename,psw);

            if (id == -1)//means that the request was denied.
            {
                int failureParcel = { MAXROOMSCREATEDNOTIFY };
                sender(fd,&failureParcel,sizeof(failureParcel) );
                break;
            }

            //everything is ok. Broadcast the the existance of the new room
            vector<int> newgameParcel;

            newgameParcel.push_back( NEWGAMENOTIFY );
            newgameParcel.push_back( id );
            newgameParcel.push_back( (int)gamename.size() );
            for( int i = 0; i < gamename.size();i++ )
                newgameParcel.push_back( gamename[i] );

            for(auto it = repmap.begin(); it != repmap.end();it++)
                sender(it->first,newgameParcel.data(),sizeof(int) * (int)newgameParcel.size() );
            break;
        }
        case LEAVEROOM:
        {
            HoldemPokerGame& game = getGameViaFd(fd);

            //håndter det faktum at spiller har forlatt et rom.
            game.handleLeavingPlayer( fd );

            //Send beskjed til samtlige klienter med beskjed om at spiller har forlatt rommet
            int playerLeftRoomParcel[4] = { PLAYERSTATECHANGEDNOTIFY,fd,game.getGameId(),(uint)false };
            for(auto it = repmap.begin();it != repmap.end();it++)
                sender(it->first,playerLeftRoomParcel,sizeof(playerLeftRoomParcel));
            break;
        }
        //vanlig kommunikasjon mellom spillere.
        case SHORTCHAT:
        {
            chatHelper(fd,request);
            break;
        }
        default:
        {
            cout<<"error: unknown opcode("<<opcode<<"), or data, in pipe. Content is: "<<endl;
            for(auto& val : request)
            {
                cout<<val<<"   ";
            }
            cout<<"content end."<<endl;
        }
    }
}

//brukes for å gjøre de øvrige klientene oppmerksom på at en klient har koblet fra
void Server::disconnectProtocol(int fd,bool silent ) //silent denotes whether or not the disconnectProtocol is used to force disconnects or not.
{
    //det er mulig at klienten som er koblet til, men ikke logget inn,
    //for stygge feil hvis server prøver å logge ut en spiller som aldri var logget inn.
    try
    {
        if (!silent)
        {
            //hent rep objektet tilhørende den fildeskriptoren, sjekk om vedkommende er koblet til et spill eller ikke.
            playerRepresentation& rep = getRepViaFd(fd);//Hvis denne feiler, betyr det at det var snakk om en som koblet til, men aldri logget inn.

            //fjern så spiller fra repmap, og lukk fd
            if (rep.getGameId() != -1)//hvis det er snakk om en spiller, må ekstra grep tas.
            {
                HoldemPokerGame& game = getGameViaGameID( rep.getGameId() );
                game.handleLeavingPlayer( fd );
            }

            //Uansett må samtlige klienter bli gjort oppmerksom på at spiller ikke lenger er tilkoblet
            int playerleftparcel[4] = { PLAYERSTATECHANGEDNOTIFY,fd,rep.getGameId(),(bool)true };

            for(auto& mapping : repmap)
                sender(mapping.second.getSocketNr(),playerleftparcel,sizeof(playerleftparcel));
        }

        //cleanup kalles for å rydde opp etter den frakoblede brukeren, slik at systemet er konsistent.
        cleanupLock->lock();

        managerOwner->logout( getRepViaFd(fd) );
        repmap.erase(fd);

        cleanupLock->unlock();
    }
    catch (MapDoesNotContainObjectException& e) {}//Snakk om en spiller som koblet til, men ikke logget inn, ingen alvorlig tilstand.

    close(fd);
}

void* Server::get_in_addr(sockaddr *sa)
{
    return &(((sockaddr_in*)sa)->sin_addr);
}

bool Server::isListening()
{
    int value;
    socklen_t len = sizeof(value);

    //as a note about the swich below: In our case, it means that the int known as listener hasn't even been associated with a socket descriptor yet
    //more generally a return of -1 indicates that the fd, listenr in my call here, isn't socket descriptor, or isn't a descriptor at all. This applies to unix only
    if ( getsockopt ( listener,SOL_SOCKET,SO_ACCEPTCONN,&value,&len) == -1)
        return false;
    else if ( value ) //is a socket, and is listening
        return true;
    else
        return false;
}

void Server::stopServer()
{
    /*
    This function needs to do the following:
    close any and all sockets except the STDIN(if we do close STDIN, we lose the input).
    shut down any and all games currently running.

    */

    //close the listener first
    close(listener);
    FD_CLR(listener,&master);

    //close all sockets, except stdin
    for (int fd = 0; fd <1000;fd++)
    {
        if ( FD_ISSET(fd,&master) && fd != STDIN_FILENO )//will close the sockets
        {
            cout<<"processing fd: "<<fd<<endl;
            playerRepresentation& rep = getRepViaFd(fd);
            cout<<"done processing fd"<<fd<<endl;

            FD_CLR(fd,&master);
            FD_CLR(fd,&read_fds);
            disconnectProtocol(fd,true);
        }
        //players have now been properly logged out, and their sockets closed, time to destroy all rooms
        games.clear();
    }
}

void Server::sockInit()
{
    if ( managerOwner.get() == nullptr)//must remember to have a profileManager before starting to listen, if not server is in an invalid state.
    {
        cout<<"Socket could not be initialized, please initialize the profile manager first."<<endl;
        return;
    }
    if ( this->isListening() )
    {
        cout<<"Socket is already listening"<<endl;
        return;
    }

    //get us a socket and bind it
    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    //int status = getaddrinfo(SERVERIP,SERVERLISTENPORT,&hints,&servinfo);

    int rv = getaddrinfo( NULL,std::to_string( designatedListeningPort ).c_str(),&hints,&ai );
    if (rv != 0)
    {
        fprintf( stderr,"selectserver: &s\n",gai_strerror(rv) );
        exit(1);
    }

    for (ptr = ai; ptr != NULL; ptr = ptr->ai_next)
    {

        listener = socket(ptr->ai_family,ptr->ai_socktype,ptr->ai_protocol);
        if (listener < 0 )//valid unix code, not a good idea for windows
            continue;

        setsockopt( listener,SOL_SOCKET,SO_REUSEADDR,&ServConst::YES,sizeof(int) );
        if ( ::bind(listener,ptr->ai_addr,(int)ptr->ai_addrlen) < 0 )
        {
            close (listener);
            continue;
        }
        break;
    }

    if(ptr == NULL)
    {
        fprintf(stderr,"selectserver: failed to bind\n");
        exit(2);
    }

    if ( listen(listener,10) == -1 )
    {
        perror("listen");
        exit(3);
    }
    freeaddrinfo(ai);//NOTE: sannsynligvis derfor ai er en peker, getaddrinfo gjør sannsynligvis en dynamisk allokering

    FD_ZERO(&read_fds);
    FD_SET(listener,&master);
    fdmax=listener;
    currentListeningPort = designatedListeningPort;
    cout<<"Socket initalization was successful, accepting new connections on port "<<this->designatedListeningPort<<endl;
}

void Server::storageInit()
{
    if ( managerOwner.get() == nullptr)
    {

        if (cfg_args[0] == "DB")
        {
            /*cout<<"Database storage specified. Values loaded are: IP: "<<cfg_args[1]<<", port: "<<cfg_args[2]<<", username: "
            <<cfg_args[3]<<", password: "<<cfg_args[4]<<"."<<endl;
            managerOwner = std::unique_ptr<ProfileManager>(new DBProfileManager(repmap,cfg_args[1],cfg_args[2],cfg_args[3],cfg_args[4]) );*/
            cout<<"Database storage specified, currently not working. Aborting"<<endl;
            abort();
        }
        else
        {
            cout<<"File storage specified."<<endl;
            managerOwner = std::unique_ptr<ProfileManager>(new FileProfileManager(repmap));
        }
    }
    else
        cout<<"Profile Manager already loaded, ignoring"<<endl;
}

void Server::processUserInput(string& inputstring)
{
    if ( inputstring == "exit" )//øyeblikkelig avslutning av server, tilbakebetaler eventuelle penger i spill.
    {
        for (auto& game : games)
            game.reimburse();

        if ( managerOwner != nullptr )
            managerOwner->saveProfiles();
        ServVars::EXIT_CALLED = true;
    }
    else if ( inputstring == "list gamedata")
    {
        cout<<"listing game data:\n";
        for(auto& game : games)
        {
            cout<<"gameId: "<<game.getGameId()<<"    "<<"Rep count: "<<game.getReps().size()<<"\n";
        }
        cout<<"listing complete"<<endl;
    }
    else if ( inputstring == "list repsdata")
    {
        cout<<"listing reps data:\n";
        for(auto it = repmap.begin();it != repmap.end();it++ )
        {
            playerRepresentation& rep = it->second;
            int gameid = rep.getGameId();
            std::string gameidstr;

            if ( gameid == -1)
                gameidstr = "L";
            else
                gameidstr = std::to_string(gameid);

            cout<<"fd: "<<rep.getSocketNr()<<"    fd in map: "<<it->first<<"   gameId: "
               <<gameidstr<<"   Name:"<<rep.getName()<<"    DCstate: "<<rep.getLeavingState()<<endl;
        }
        cout<<"listing complete \tPS: A gameid equal to \"L\" Means the player is in the lobby, being a \"lobbyer\""<<endl;
    }
    else if ( inputstring == "start" )//combines setup and start manager
    {
        storageInit();
        sockInit();
    }
    else if ( inputstring == "listen" )
    {
        sockInit();
    }
    else if ( inputstring == "load" )
    {
        storageInit();
    }
    else if ( inputstring == "stop" )//if the listener is listening, then this will result in a call to stopServer()
    {
        if ( isListening() )
        {
            stopServer();
            cout<<"Server stopped and reset."<<endl;
        }
        else
            cout<<"Unable to comply, cannot stop server that is already stopped"<<endl;
    }
    else if ( inputstring == "status" )
    {
        if ( isListening() )
        {
            if ( designatedListeningPort == currentListeningPort)//designated port matches the one in use
                cout<<"Server currently listening at: "<<currentListeningPort<<endl;
            else
                cout<<"Server currently listening at: "<<currentListeningPort<<". However, the designated listening port is "<<designatedListeningPort<<"."<<endl;
        }
        else
        {
            cout<<"Server is currently NOT listening."<<"\n";
            cout<<"Designated listening port is: "<<designatedListeningPort<<endl;
        }

        cout<<"Number of connected peers is: "<<repmap.size()<<endl;
        cout<<"Number of rooms(games) created: "<<games.size()<<endl;

        ProfileManager* ptr = managerOwner.get();
        if ( managerOwner.get() != nullptr)
        {
            if ( typeid(*ptr) == typeid(FileProfileManager) )
                cout<<"ProfileManager is of type: fileProfileManager"<<endl;
//            else if ( typeid(*ptr) == typeid(DBProfileManager) )
//            {
//                cout<<"ProfileManager is of type: DBProfileManager"<<"\n";
//                cout<<"Variables used to connect to the database are as follows:\n";
//                cout<<"DB IP: "<<cfg_args[1]<<"\n";
//                cout<<"DB Port: "<<cfg_args[2]<<"\n";
//                cout<<"DB Username: "<<cfg_args[3]<<"\n";
//                cout<<"DB Password: "<<cfg_args[4]<<endl;
//            }
        }
        else
        {
            cout<<"ProfileManager has not been initialized."<<endl;
        }

    }
    else if ( inputstring.substr(0,8 ) == "set port" )//set the listen port to whatever value the user provides.
    {
        //check if value entered actually makes any sort of sense
        if ( inputstring.size() == 8 )//user forgot to enter an argument.
        {
            cout<<"Unable to comply: No argument specified."<<endl;
            return;
        }

        string substring = inputstring.substr(9);

        if ( substring.find_first_not_of( "0123456789" ) == string::npos )//no non-numeric characters found
        {
            try
            {
                designatedListeningPort = std::stoi(substring.substr() );
                cout<<"Listening Port set to "<<designatedListeningPort<<endl;
            }
            catch ( std::out_of_range& e)
            {
                cout<<"Unable to comply: Argument length was to long"<<endl;
                return;
            }
            catch (std::invalid_argument& e)//happens if the user enters "set port ", that extra space causes trouble
            {
                cout<<"Unable to comply: Invalid argument specified"<<endl;
                return;
            }
        }
        else
        {
            cout<<"Unable to comply: Invalid argument specified"<<endl;
        }
    }
    else if ( inputstring == "restart")
    {
        if ( isListening() )
        {
            cout<<"Restarting server."<<endl;
            stopServer();
            sockInit();
        }
        else
            cout<<"Unable to comply, server already stopped."<<endl;
    }
    else if ( inputstring == "help" )
    {
        cout<<ServConst::commandhelp<<endl;
    }
    else if ( inputstring == "clear" )
    {
        system("clear");
    }
    else if ( inputstring == "god")
    {
        cout<<"This unit recognizes only one god: Lord Kek"<<endl;
    }
    else//ugyldig input
        cout<<"\""<<inputstring<<"\" is not a valid command. Type \"help\" for assistance."<<endl;

    cout<<"Awaiting command-> "<<flush;
}

void Server::mainProcessLoop()
{
    //large tracts of this function, along with sockInit, is inspired by Beej's guide over at beej.us/guide/bgnet
    FD_SET(STDIN_FILENO,&master);//på denne måten får vi enkel og synkronisert admin kontroll.
    if (fdmax == 0)
        fdmax = STDIN_FILENO;

    //her starter "main process loop". Den går for alltid, med mindre noe skjer.
    cout<<"Awaiting command-> "<<flush;

    while(!ServVars::EXIT_CALLED )
    {
        read_fds=master;
        if ( select(fdmax+1,&read_fds,NULL,NULL,NULL) == -1 )
        {
            cout<<"eror: attempt to select on bad socket, Errno is: "<<errno<<endl;
            abort();
        }
        for( int fd = 0; fd<=fdmax;fd++ )
        {
            if ( FD_ISSET(fd,&read_fds) )//endringer i listen.
            {
                if ( fd == STDIN_FILENO )//stdin
                {
                    string inputstring;
                    getline(cin,inputstring);

                    processUserInput(inputstring);
                }
                else if (fd == listener) //betyr ny tilkobling
                {
                    sockaddr_storage remoteaddr;
                    unsigned int addrlen = sizeof(remoteaddr);
                    int newfd = accept(listener,(sockaddr*)&remoteaddr,&addrlen);

                    if (newfd ==-1)
                    {
                        perror("accept");
                    }
                    else //håndter ny tilkobling
                    {
                        if ( repmap.size() == ServConst::MAXCONN )
                        {
                            int reject = REJECTCONNECTIONNOTIFY;
                            cout<<(char)reject<<endl;
                            sender(newfd,&reject,sizeof(reject));
                            cout<<"New connection refused due to MAXCONN"<<endl;
                            close(newfd);
                            continue;
                        }
                        FD_SET(newfd,&master);

                        if (newfd > fdmax)
                        {
                            fdmax = newfd;
                        }

                        char remoteIP[INET6_ADDRSTRLEN];
                        //sockaddr* test = ai->ai_addr;//for debugging
                        const char* localIP = inet_ntop(remoteaddr.ss_family,get_in_addr( (sockaddr*)&remoteaddr),remoteIP,INET_ADDRSTRLEN);
                        //unsigned int localPort = ntohs(((sockaddr_in*)ai->ai_addr)->sin_port);
                        cout<<"New connection received from "<<localIP<<" on socket "<<newfd<<endl;
                    }
                }
                else//mottar meldinger fra koblinger, må leses og prosesseres
                {
                    int buf[1000];
                    memset(&buf,0,100);
                    int nbytes = recv(fd,buf,sizeof(buf),0);

                    if (nbytes <=0 )//noe er galt, eller klient lukket kobling.
                    {
                        if (nbytes == 0) //connection closed
                        {
                            cout<<"Socket "<<fd<<" hung up."<<endl;
                        }
                        else //nbytes = -1, altså error
                        {
                            cout<<"Recveive error on socket "<<fd<<": closing connection."<<endl;
                        }
                        FD_CLR(fd,&master); //kobling må fjernes fra master

                        thread dcthread(&Server::disconnectProtocol,this,fd,false);
                        dcthread.detach();
/*REDESIGN NOTE:
 * dissconnectProtocol kalles synkront.
 * utlogging og erasing i repmap flyttes ut til egen funksjon som kan kalles fra mainthread eller holdem ;
 * holdemthread får altså da muligheten til å fortelle server at en klient skal logges ut når holdem er ferdig med klient.
 * altså: dcthread utgår fullstendig, mens countdown thread redesignes slik at den tar over den nederste delen av paywinners
 * og legger ett oppdrag i holdem spillet som opprettet den.
 * showdownlockmanager utgår.
 * på denne måten kan vi kvitte oss med all den stygge threadingen som foregår nå.
 *
 * */
                    }
                    else //mottar pakke
                    {
                        cout<<"received package from fd "<<fd<<endl;

                        if (nbytes % Misc::SEGMENT_SIZE != 0)//client doesn't obey the protocol.
                        {
                            cout<<"client doesn't follow basic segmentation protocol, ignoring"<<endl;
                            continue;
                        }
                        vector<int> command(buf,buf + nbytes/sizeof(int) );
                        processRequest(command,fd);
                    }
                }
            }
        }
    }
}

int main()
{
    //we'll need to reset the CWD here, otherwise symlinks will cause the profile loading to fail
    //as it will look for profiles.sav in the directory of the link, not the executable.
    char wd[1024];
    std::stringstream symlink;

    symlink <<"/proc/"<<getpid()<<"/exe";

    int len = readlink(symlink.str().c_str(),wd,sizeof(wd)-1);

    if (len == -1)
        cout<<"error setting CWD, will use CWD as it is."<<endl;
    else
    {

        //now we need to trim the file name away, so we are left with the directory path alone
        string wdstr = string(wd);
        int test = wdstr.find_last_of('/');
        wdstr = wdstr.substr(0,test+1);

        int error = chdir(wdstr.substr().data());

        if ( error == -1)
        {
            cout<<"error changing CWD, aborting"<<endl;
            abort();
        }
    }
    //the potential case of CWD breaking symlinks has now been dealt with.
    Server();
}
#endif //platform check
