#ifndef CONFIGLOADER_H
#define CONFIGLOADER_H

#include <vector>
#include <string>

namespace ConfigLoading
{
    //loads the parameter related to the database, if any, from the options.cfg file.
    //If no options are present, a empty vector will be returned, prompting the user to manually
    //set the options.cfg values so a connection can be established with the DB.
    std::vector<std::string> loadConfig();

    //I didn't like that DBconst description strings were indented differently depending on the length of the preceding value.
    std::string tabGenerator(const std::string& str);
}

#endif // CONFIGLOADER_H
