#include "communityemptyexception.h"
#include <exception>

CommunityEmptyException::CommunityEmptyException()
{
    error.append("Exception: Communitycards are empty.");
}

const std::string& CommunityEmptyException::what()
{
    return error;
}
