#include "mapdoesnotcontainobjectexception.h"

MapDoesNotContainObjectException::MapDoesNotContainObjectException(int sockfd)
{
    error.append("exception in function getRepViaFd: Clients socket(");
    error.append(std::to_string(sockfd));
    error.append(") does not seem to be associated with any rep.");
}

const char* MapDoesNotContainObjectException::what()
{
    return error.c_str();
}
