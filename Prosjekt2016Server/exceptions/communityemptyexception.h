#ifndef COMMUNITYEMPTYEXCEPTION_H
#define COMMUNITYEMPTYEXCEPTION_H

#include <exception>
#include <string>

class CommunityEmptyException : public std::exception
{
    std::string error;

    public:
        CommunityEmptyException();

        const std::string& what();
};

#endif // COMMUNITYEMPTYEXCEPTION_H
