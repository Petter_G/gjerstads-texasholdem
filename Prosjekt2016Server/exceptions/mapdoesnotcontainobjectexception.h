#ifndef MAPDOESNOTCONTAINOBJECTEXCEPTION_H
#define MAPDOESNOTCONTAINOBJECTEXCEPTION_H

#include <string>

class MapDoesNotContainObjectException
{
    std::string error;

    public:
        MapDoesNotContainObjectException(int sockfd);

        const char* what();
};

#endif // MAPDOESNOTCONTAINOBJECTEXCEPTION_H
