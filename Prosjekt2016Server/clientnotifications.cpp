#include "clientnotifications.h"



using namespace std;
    void ClientNotifications::NotifyTableReset_BC(Server& serverRef, std::map<ushort, playerRepresentation *>& reps)
    {
        int tableReset[1] = {TABLERESET};
        for ( auto player : reps)
        {
            serverRef.sender(player.second->getSocketNr(),tableReset,sizeof(tableReset));
        }

        for ( auto player : reps )
        {
            int resetActive[3] = {ACTIVEUPDATE,player.second->getTableSlot(),player.second->getActiveState()};

            //Info for hver enkelt spiller skal sendes til alle spillere
            for( auto otherPlayer : reps )
            {
                serverRef.sender(otherPlayer.second->getSocketNr(),resetActive,sizeof(resetActive));
            }
        }
    }

    void ClientNotifications::NotifyHandSet_HAll(Server& serverRef, std::map<ushort, playerRepresentation *>& reps)
    {
        for (auto player : reps )
        {
            playerRepresentation& rep = *player.second;
            if ( rep.getActiveState() )
            {
                vector<card> hand = rep.getHand();
                int dataParcel[6] = {HANDUPDATE,rep.getTableSlot(),
                                    hand[0].getType(),hand[0].getValue(),hand[1].getType(),hand[1].getValue() };
                //sende til eier.
                serverRef.sender(rep.getSocketNr(),dataParcel,sizeof(dataParcel));
            }
        }
    }

    void ClientNotifications::NotifyHandSet_BCALL(Server& serverRef, std::map<ushort, playerRepresentation *>& reps)
    {
        for (auto player : reps)
        {
            playerRepresentation& rep = *player.second;

            if (rep.getActiveState() )
            {
                vector<card> hand = rep.getHand();

                //a little problem arose when a player went bankrupt, the server would drop the players hands before the next round began
                //and subsequently invoke this function, which in turn would result in a fatal error when accessing the non-existant
                //cards in the hand vector.
                if ( hand.size() == 0)//if 0, do not send update.
                    return;

                int dataParcel[6] = {HANDUPDATE,rep.getTableSlot(),
                    hand[0].getType(),hand[0].getValue(),hand[1].getType(),hand[1].getValue() };

                //Kringkaste til alle.
                for (auto otherPlayer : reps)
                {
                    serverRef.sender(otherPlayer.second->getSocketNr(),dataParcel,sizeof(dataParcel));
                }
            }
        }
    }

    void ClientNotifications::NotifyBalanceChanged_BCSingle(Server &serverRef,std::map<ushort, playerRepresentation *>& reps,int tableslot)
    {

        playerRepresentation* player = reps.at(tableslot);
        int balanceParcel[3] = {BALANCEUPDATE,player->getTableSlot(),player->getBalance()};

        //Nå må vi løpe igjennom Reps for å sende beskjed til samtlige klienter
        for ( auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),balanceParcel,sizeof(balanceParcel));
        }
    }

    void ClientNotifications::NotifyBalanceChanged_BCAll(Server &serverRef,std::map<ushort, playerRepresentation *>& reps)
    {
        for ( auto player : reps )
        {
            int balanceParcel[3] = {BALANCEUPDATE,player.second->getTableSlot(),player.second->getBalance()};
            //Bankbalansen til hver enkelt spiller skal sendes til alle.
            for( auto otherPlayer : reps )
            {
                serverRef.sender(otherPlayer.second->getSocketNr(),balanceParcel,sizeof(balanceParcel));
            }
        }
    }

    void ClientNotifications::NotifyBetChanged_BCSingle(Server &serverRef, std::map<ushort, playerRepresentation *>& reps, uint tableslot)
    {
        playerRepresentation* player = reps.at(tableslot);
        int betParcel[4] = {BETUPDATE,player->getTableSlot(),player->getBet(),player->getTotalBet()};

        for ( auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),betParcel,sizeof(betParcel));
        }
    }

    void ClientNotifications::NotifyBetChanged_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *>& reps)
    {
        for ( auto player : reps )
        {
            int betParcel[4] = {BETUPDATE,player.second->getTableSlot(),player.second->getBet(),player.second->getTotalBet() };

            //må så sende denne spillerens info til alle, inkludert spilleren selv.
            for ( auto otherPlayer : reps )
            {
                serverRef.sender(otherPlayer.second->getSocketNr(),betParcel,sizeof(betParcel));
            }
        }
    }

    void ClientNotifications::NotifyExistingCommunityCards_BCSingle(Server& serverRef,int fd, std::vector<card>& community)
    {
        int dataparcel[3];
        dataparcel[0] = COMMUNITYCARDADDED;

        for(auto& card : community)
        {
            dataparcel[1] = card.getType();
            dataparcel[2] = card.getValue();
            serverRef.sender(fd,dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyCommunityCardAdded_BC(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,card& addedCard)
    {
        int dataparcel[3] = { COMMUNITYCARDADDED,addedCard.getType(),addedCard.getValue() };
        for ( auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyPot_BC(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,int pot)
    {
        int dataparcel[2] = {POTNOTIFY,pot};
        for(auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyCurrentPlayer_BCAll(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,short currentPlayerSlot)
    {
        int dataparcel[2] = { CURRENTPLAYERNOTIFY,reps[currentPlayerSlot]->getTableSlot() } ;
        for (auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyPlayerFolded_BCAll(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,uint tableslot)
    {
        playerRepresentation* player = reps.at(tableslot);
        int dataparcel[3] = { ACTIVEUPDATE,player->getTableSlot(),player->getActiveState() };

        for( auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyPlayerLeftLocalRoom_BCAll(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,uint tableslot)
    {
        playerRepresentation* rep = reps[tableslot];

        //først sendes en pakke til samtlige medlemmer av spillers rom, om noen.
        int dataparcel[2] = { PLAYERLEFTLOCALROOMNOTIFY,rep->getTableSlot() };
        for (auto player : reps)
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel) );
        }
    }

    void ClientNotifications::NotifyButton_BC(Server& serverRef, std::map<ushort, playerRepresentation *>& reps, short currentButtonSlot)
    {
        int dataparcel[2] = { CURRENTBUTTONNOTIFY,currentButtonSlot };
        for(auto player : reps)
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyAllIn_BCSingle(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,uint tableslot)
    {
        int dataparcel[2] = { ALLINUPDATE, static_cast<int>(tableslot) };
        for(auto player : reps)
        {
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyHaltState_BCAll(Server& serverRef, std::map<ushort, playerRepresentation *>& reps)
    {
        int dataparcel = GAMEHALTEDNOTIFY;
        for( auto it : reps )
        {
            serverRef.sender( it.second->getSocketNr(),&dataparcel,sizeof(dataparcel));
        }
    }

    void ClientNotifications::NotifyWinnerDataAdded_BCAll(Server& serverRef, std::map<ushort, playerRepresentation *>& reps,ScoreObject* score,int payout,int potIndex)
    {
        cout<<"SENDING WINNERDATA"<<"  PAYOUT: "<<payout<<"   POTINDEX"<<potIndex<<endl;
        playerRepresentation& rep = score->getRepPtr();//opcode-winnertableslot-payout-potIndex-
        int dataparcel[] = { WINNERINFONOTIFY,rep.getTableSlot(),payout,potIndex };

        for(auto player : reps)
            serverRef.sender(player.second->getSocketNr(),dataparcel,sizeof(dataparcel));
    }

    //Repaint
    void ClientNotifications::NotifyPaintUI_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps)
    {
        int dataparcel = REPAINT;
        for ( auto player : reps )
        {
            serverRef.sender(player.second->getSocketNr(),&dataparcel,sizeof(dataparcel));
        }
    }
