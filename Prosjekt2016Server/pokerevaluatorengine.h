#ifndef POKEREVALUATORENGINE_H
#define POKEREVALUATORENGINE_H

#include <vector>
#include <exception>
#include <iostream>

#include "card.h"
#include "scoreobject.h"
#include "playerrepresentation.h"
#include "protocolenums.h"


//Lagde dette som egen klasse da det begynnte å bli uoversiktelig i pokergame. Denne klassen er og et ypperlig eksempel på noe som kan være modulært.
//forresten fornuftig da AIPlayer, som jeg ikke rakk, uansett ville hatt behov for å sjekke håndens verdi.
class PokerEvaluatorEngine
{
    //regler: www.europoker.com/download/quickguide/quickguide_en.pdf

private:
    std::vector<card> combinedHand;
    std::vector<card> bestHand;     //Rimelig rett frem, bestHand er de fem av syv kort som tjener spilleren best.

    //div hjelp
    short straightHighcardindex;
    short flushHighCardIndex;

    //poengsystemet representeres av opptil 5 systemer, avhengig av hånden. royalflush har bare en verdi, primary, mens high card har hele fem.
    short primary;//Texas holdem har 10 forskjellige "hands", hvor Royal flush(andre steder kalt royalstraightflush om jeg ikke tar feil) er den beste, og highcard den dårligste.
    short secondary;//typisk høyeste kort i en straight eller flush. Untak for RoyalFlush, som ikke har noen ekstra verdier overhodet.
    short tertiary; //Brukes i f.eks hus er denne paret huset er bygd opp av.
    short quaternary;

    //flag
    bool royalflush;
    bool straightflush;
    bool four;  //fourofakind
    bool house;
    bool flush;
    bool straight;
    bool three; //threeofakind
    bool dpair; //to par
    bool pair;

    //diverse funksjoner brukt i evaluateHand for å finne ut hva man har.
    bool rawFlushCheck();//Brukes for å sjekke om du har en vanlig flush.
    bool flushCheck(std::vector<card> hand);//analyser en hånd på 5(ikke mer) kort for flush.
    void RoyalStraightFlushAnalyzer();   //analyser combinedHand, flagger for eventuelle royalflush,straightflush og vanlige straights.

    bool fourCheck();
    bool houseCheck();
    bool threeCheck();
    bool dpairCheck();
    bool pairCheck();
    void highcardDeterminator();

    ScoreObject evaluateHand(std::vector<card>& community, playerRepresentation& player);
public:
    //Tar imot et (sub)sett med av spillets spillere som spiller om en pot, finner ut hvem som er vinneren/vinnere
    //og sender tilbake en vektor som består av pekere til vinneren/vinnere. Ansvar for utbetaling gjøres ikke her.
    std::vector<ScoreObject*> determineWinners(std::vector<playerRepresentation*>& Reps, std::vector<card>& communityCards);
};

#endif // POKEREVALUATORENGINE_H
