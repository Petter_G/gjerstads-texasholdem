#include "potdata.h"

PotData::PotData(uint bet, uint index)
{
    betValue=bet;
    contesterCount=0;
    excess=0;
    freemoney=0;

    LocalIndex=index;
}

void PotData::addContestant(playerRepresentation* player)    //inkrementerer contesters, og legger sender pekeren videre til contesterPointers.
{
    contesterCount++;
    contesterPointers.push_back(player);
}

uint PotData::getBetValue()     //henter ut betValue
{
    return betValue;
}

uint PotData::calculateSum()          //regner ut sum basert på betValue,excess og contesters
{
    return ((betValue-excess) * contesterCount)+freemoney;
}

uint PotData::getContestantCount()
{
    return contesterCount;
}

uint PotData::getFreeMoney()
{
    return freemoney;
}

uint PotData::getIndex()
{
    return LocalIndex;
}

void PotData::setExcess(uint ref)
{
    excess = ref;
}

void PotData::incrementFreemoney(uint ref)
{
    freemoney += ref;
}

std::vector<playerRepresentation*>& PotData::getContesterPointers()
{
    return contesterPointers;
}
