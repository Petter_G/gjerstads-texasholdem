#ifndef FILEPROFILEMANAGER_H
#define FILEPROFILEMANAGER_H

#include "profilemanager.h"
#include "protocolenums.h"
#include "playerrepresentation.h"

//FileProfileManager extends ProfileManager and implements it's abstract functions. It operates on a file(profiles.sav) in order to store profile data.
class FileProfileManager : public ProfileManager
{

public:
    //Opens profiles.sav, creates the file if it doesn't exist, parses the content of pushes said profiles on the "profiles" vector.
    FileProfileManager(std::map<short,playerRepresentation>& repmapsref);

    int loginAttempt(std::string username, std::string password);
    //profile* getProfile(uint cookieid);

    std::string getUsername(uint cookieid);
    uint getBalance(uint cookieid);
    //bool getUserLoggedState(uint cookieid);

    void logout(playerRepresentation& rep);

    //first updates profiles vector with uptodate data from repmap, then overwrites profile.sav with said data.
    void saveProfiles();
    bool addProfile(std::string username,std::string password);
    bool changeUsername(std::string newUsername,std::string oldUsername);

    void listProfiles();
};

#endif // FILEPROFILEMANAGER_H
