#ifndef PROFILEMANAGER_H
#define PROFILEMANAGER_H

#include "protocolenums.h"
#include "playerrepresentation.h"
#include "profile.h"

#include <iostream>
#include <map>

/*#include <include/cppconn/connection.h>
#include <include/cppconn/prepared_statement.h>
#include <include/cppconn/driver.h>
#include <include/cppconn/exception.h>
#include <include/cppconn/resultset.h>*/


//interface for profileManagers. Each function's high level intent is descibed in comments, for any implementation specific
//details(if applicable), see subclasses for additional information.

class ProfileManager
{
protected:
    std::vector<profile> profiles; //Holds the profiles loaded/fetched from profiles.sav or database.
    std::map<short,playerRepresentation>& Repsmapref;//reference to repmap owned by server, used by saveProfiles.

public:
    //Prepares the manager for use, ie instantiation.
    ProfileManager(std::map<short,playerRepresentation>& repmapsref);

    //Checks whether username and password match anything in the "profiles" vector, if yes, gives that profile a ID(IE a cookie) and returns it.
    //If the login attempt fails, -1 is returned. Users already logged in also result in a -1.
    virtual int loginAttempt(std::string username, std::string password) = 0;

    //returns profile with matching cookieId. if profile cannot be found, returns nullptr;
    //virtual profile* getProfile(uint cookieid) = 0;//deprecated

    //these three replaces getProfile(). all three functions have in common that not finding anything is a fatal error.
    virtual std::string getUsername(uint cookie) = 0;
    virtual uint getBalance(uint cookieid) = 0;
    //virtual bool getUserLoggedState(uint cookieid) = 0;

    //Checks whether username exists, is logged in, and if such is the case: Change corresponding profile's loggedIn value to false,
    //resets it's cookie ID to -1 and updates it's balanceLocal value for later hard storage.
    virtual void logout(playerRepresentation& rep) = 0;

    //saves profiles to hard storage
    virtual void saveProfiles() = 0;

    //checks if username is valid(ie not already in use), and if yes adds it to storage.
    virtual bool addProfile(std::string username,std::string password) = 0;

    //checks if newUsername is valid, and if oldUsername exists. If yes changes the oldUsername to the new.
    virtual bool changeUsername(std::string newUsername,std::string oldUsername) = 0;

    //Debug, lists all profiles.
    virtual void listProfiles() = 0;
};

//this function is used several places in this file
bool checkValidity(std::string text);
bool checkPasswordValidity(std::string password);
#endif // PROFILEMANAGER_H
