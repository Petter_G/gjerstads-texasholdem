#include "profile.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

profile::profile(string username, string password, string balance) : idcookie(-1)//500 er standard for nye spillere.
{
    usernameLocal = username;
    passwordLocal = password;
    loggedIn=false;

    try
    {
        balanceLocal = std::stoi(balance);
    }
    catch (invalid_argument& e)
    {
        cout<<"profile corrupt, balance not parsable."<<endl;
        abort();
    }
}

int profile::getCookieId()
{
    return idcookie;
}

string profile::getPassword()
{
    return passwordLocal;
}

string profile::getUsername()
{
    return usernameLocal;
}

uint profile::getBalance()
{
    return balanceLocal;
}

bool profile::getLoggedin()
{
    return loggedIn;
}

void profile::setUsername(std::string newUsername)
{
    usernameLocal = newUsername;
}

void profile::setPassword(std::string newPassword)
{
    passwordLocal = newPassword;
}

void profile::setBalance(uint newBalance)
{
    balanceLocal = newBalance;
}

bool profile::setLoggedOut()
{
    if (loggedIn == false)//dvs ikke gyldig
        return false;
    else
    {
        idcookie=-1;
        loggedIn=false;
        return true;
    }
}

bool profile::setLoggedIN(uint cookie)
{
    if (loggedIn == true)//dvs allerede innlogget
        return false;
    else
    {
        idcookie = cookie;
        loggedIn=true;
        return true;
    }
}
