//#include "dbprofilemanager.h"

//#include <iostream>
//#include <sstream>
//#include <fstream>
//#include <string>

//using namespace std;
//DBProfileManager::DBProfileManager(std::map<short,playerRepresentation>& repmapsref, string DB_IP, string DB_PORT,
//    string DB_USER, string DB_PASS) : ProfileManager(repmapsref),
//    databaseIP(DB_IP),databasePort(DB_PORT),databaseUsername(DB_USER),databasePassword(DB_PASS)
//{
//    try
//    {
//        cout<<"Attempting connection...."<<endl;
//        driver = get_driver_instance();

//        stringstream stream;
//        stream <<"tcp://"<<databaseIP<<":"<<databasePort;
//        conn = driver->connect(stream.str(),databaseUsername,databasePassword);
//        cout<<"Connection established."<<endl;
//        conn->setAutoCommit(true);
//        conn->setSchema(DBConst::SCHEMANAME);

//        //in case the server didn't properly exit for whatever reason, each individual user needs to have its logged attribute set to false
//        stringstream SQLUpdatecreator;

//        SQLUpdatecreator <<"UPDATE "<<AMA(DBConst::TABLENAME_user)
//                         <<" SET "<<AMA(DBConst::TABLE_user_logged) <<" = false,"
//                         <<AMA(DBConst::TABLE_user_cookie)<<" = NULL";

//        sql::PreparedStatement* updateStatement = conn->prepareStatement(SQLUpdatecreator.str() );

//        int returncode = updateStatement->executeUpdate();

//        updateStatement->close();
//        delete updateStatement;
//    }
//    catch(sql::SQLException &ex)
//    {
//        cout<<"Unable to connect to database. SQL error code is: "<<ex.getErrorCode()<<endl;
//        std::abort();
//    }
//}

//DBProfileManager::~DBProfileManager()
//{
//    conn->close();
//    delete conn;
//}

//using namespace sql;
//int DBProfileManager::loginAttempt(std::string username, std::string password)
//{
//    int returnvalue = -1;
//    static uint id = 100;
//    sql::PreparedStatement* updateStatement;

//    stringstream SQLupdatecreator;
//    SQLupdatecreator<<"UPDATE "<<AMA(DBConst::TABLENAME_user)
//                    <<" SET "<<AMA(DBConst::TABLE_user_logged)<<" = true,"
//                    <<AMA(DBConst::TABLE_user_cookie)<<" = "<<id
//                    <<" WHERE "<<AMA(DBConst::TABLE_user_Username_PK)<<" = "<<QMA(username)
//                    <<" AND "<<AMA(DBConst::TABLE_user_password)<<" = "<<QMA(password)
//                    <<" AND "<<AMA(DBConst::TABLE_user_logged)<<" = false";

//cout<<SQLupdatecreator.str()<<endl;
//    updateStatement = conn->prepareStatement( SQLupdatecreator.str() );

//    //statement and resultset prepared, now make the query.
//    try
//    {
//        int SQLState = updateStatement->executeUpdate();

//        if (SQLState == 1)//everything went as planned
//            returnvalue = id++;
//        else if (SQLState > 1)//something went terribly wrong as more than one row were changed.
//        {
//            cout<<"Fatal Error: More than one row was affected."<<endl;
//            abort();
//        }
//        //if SQLSTATE = 0, then either the username doesn't exist, and/or the password was wrong.
//        //either way the default returnvalue is returned, ie -1.

//    }
//    catch (SQLException& exception)
//    {
//        cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//        abort();
//    }
//    updateStatement->close();

//    delete updateStatement;
//    return returnvalue;
//}

///*profile* DBProfileManager::getProfile(uint cookieid)//deprecated
//{

//}*/

//string DBProfileManager::getUsername(uint cookie)
//{
//    std::string username;
//    sql::PreparedStatement* selectStatement;
//    sql::ResultSet* result;

//    stringstream SQLquerycreator;
//    SQLquerycreator <<"SELECT "<<AMA(DBConst::TABLE_user_Username_PK)
//                    <<" FROM "<< AMA(DBConst::TABLENAME_user)
//                    <<" WHERE "<< AMA(DBConst::TABLE_user_cookie)<<" = "<<cookie;

//    selectStatement = conn->prepareStatement(SQLquerycreator.str() );

//    try
//    {
//        result = selectStatement->executeQuery();

//        if ( result->rowsCount() > 1)//fatal error, abort program.
//        {
//            cout<<"Fatal error: RowCount mismatch. More results returned than is legal."<<endl;
//            abort();
//        }
//        else if ( result -> rowsCount() == 0)
//        {
//            cout<<"Fatal error: RowCount mismatch. No results returned."<<endl;
//            abort();
//        }

//        //annoyingly, at first the iterator inside the resultset point to nothing,
//        //so you'll call next in order to access the first(and only) row.
//        //I won't deny that this cost me alot of time,grief in the form of sanity checking.
//        result->next();
//        username = result->getString("Username");

//    }
//    catch (SQLException& exception)
//    {
//        //for some idiotic reason a successful query sometimes causes the API to throw an exception with "error" code 0.
//        //Thus far I have been unable to find a solution that doesn't look like a hack.

//        cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//        abort();

//    }

//    selectStatement->close();
//    result->close();
//    delete result;
//    return username;
//}

//uint DBProfileManager::getBalance(uint cookieid)
//{
//    uint balance = 0;
//    sql::Statement* selectStatement;
//    sql::ResultSet* result;

//    stringstream SQLquerycreator;
//    SQLquerycreator <<"SELECT "<<AMA(DBConst::TABLE_user_balance)
//                    <<" FROM "<<AMA(DBConst::TABLENAME_user)
//                    <<" WHERE "<<AMA(DBConst::TABLE_user_cookie)<<" = "<<cookieid;

//    selectStatement = conn->createStatement();

//    try
//    {
//        result = selectStatement->executeQuery(SQLquerycreator.str() );

//        if ( result->rowsCount() > 1)//fatal error, abort program.
//        {
//            cout<<"Fatal error: RowCount mismatch. More results returned than is legal."<<endl;
//            abort();
//        }
//        else if ( result -> rowsCount() == 0)
//        {
//            cout<<"Fatal error: RowCount mismatch. No results returned."<<endl;
//            abort();
//        }

//        result->next();
//        balance = result->getInt(DBConst::TABLE_user_balance);
//    }
//    catch (SQLException& exception)
//    {
//        cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//        cout<<"SQLSTATE: "<<exception.getSQLState()<<endl;
//        abort();
//    }

//    selectStatement->close();
//    result->close();
//    delete result;
//    delete selectStatement;
//    return balance;
//}

//void DBProfileManager::logout(playerRepresentation& rep)
//{
//    sql::Statement* selectStatement;
//    sql::ResultSet* result;

//    stringstream SQLquerycreator;
//    SQLquerycreator <<"SELECT *"
//                    <<" FROM "<<AMA(DBConst::TABLENAME_user)
//                    <<" WHERE "<<AMA(DBConst::TABLE_user_Username_PK)<< " = "
//                    <<QMA(rep.getName() );

//    selectStatement = conn->createStatement();

//    //query the db and confirm the user actually exists is identical on both sides,
//    //if yes update that users logged state to false as well as setting cookie to -1.
//    //NOTE: I am unsure if this checking is truly necesary, as any call to logout will be from users
//    //already logged in and with a valid cookie. May opt to simply drop all the checking and simply
//    try
//    {
//        result = selectStatement->executeQuery(SQLquerycreator.str() );

//        result->next();

//        if ( result->rowsCount() == 1)
//        {
//            //check to see if rep cookieID and DB cookieID are identical, if not something is gone wrong
//            if ( result->getInt(DBConst::TABLE_user_cookie) != rep.getCookieId() )
//            {
//                cout<<"Fatal error: local data not consistant with external data."<<endl;
//                abort();
//            }
//            else//user exists and is consistent, update away.
//            {
//                //we want to reset all variables and update balance.
//                stringstream SQLupdatecreator;
//                SQLupdatecreator    <<"UPDATE "<<AMA(DBConst::TABLENAME_user)
//                                    <<" SET "<<AMA(DBConst::TABLE_user_logged)<<" = false,"
//                                    <<AMA(DBConst::TABLE_user_cookie)<<" = false,"
//                                    <<AMA(DBConst::TABLE_user_balance)<<" = "<<std::to_string(rep.getBalance())
//                                    <<" WHERE "<<AMA(DBConst::TABLE_user_Username_PK)<< " = "<<QMA(rep.getName() );

//                sql::Statement* updateStatement = conn->createStatement();
//                string debug = SQLupdatecreator.str();
//                updateStatement->executeUpdate(SQLupdatecreator.str() );

//                updateStatement->close();
//                delete updateStatement;
//            }
//        }
//        else if ( result->rowsCount() > 1)//fatal error, abort program.
//        {
//            cout<<"Fatal error: RowCount mismatch. More results returned than is legal."<<endl;
//            abort();
//        }

//    }
//    catch (SQLException& exception)
//    {
//        cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//        abort();
//    }

//    result->close();
//    delete result;
//    selectStatement->close();
//    delete selectStatement;
//}

//void DBProfileManager::saveProfiles()
//{
//    //We'd prefer to work with transactions here, as we are potentially talking about large amounts of data.
//    //which means we'll do things a little differently.
//    sql::PreparedStatement* updatestatement;
//    stringstream SQLupdatecreator;


//    SQLupdatecreator<<"UPDATE "<<AMA(DBConst::TABLENAME_user)
//                        <<" SET "
//                        <<AMA(DBConst::TABLE_user_balance)<<" = ?"
//                        <<" WHERE "<<AMA(DBConst::TABLE_user_Username_PK)<<" = ?";

//    updatestatement = conn->prepareStatement(SQLupdatecreator.str() );

//    try
//    {
//        //if autocommit is on, each statement gets sent as an independant transaction. Since we are potentially dealing with
//        //large amounts of statements at once, it's better if they are safely collected into a transaction
//        conn->setAutoCommit(false);
//        //We've prepared a statement, now just fill in the blanks.
//        for(auto it = Repsmapref.begin();it != Repsmapref.end();it++ )
//        {
//            updatestatement->setInt(1,it->second.getBalance() );
//            updatestatement->setString(2,it->second.getName() );
//            updatestatement->executeUpdate();
//        }
//        //commit and reactivate autocommit. Not sure if the latter is necessary, but anyway.
//        conn->commit();
//        conn->setAutoCommit(true);
//    }
//    catch (SQLException& exception)
//    {
//        cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//        abort();
//    }

//    updatestatement->close();
//    delete updatestatement;
//}

//bool DBProfileManager::addProfile(std::string username,std::string password)
//{
//    if ( !checkValidity(username) || !checkPasswordValidity(password) )
//        return false;

//    bool success = true;//false = profile was not added.
//    sql::Statement* insertStatement;
//    stringstream SQLinsertcreator;

//    SQLinsertcreator<<"INSERT INTO "<<AMA(DBConst::TABLENAME_user)
//                    <<"("<<AMA(DBConst::TABLE_user_Username_PK)<<","
//                    <<AMA(DBConst::TABLE_user_password)<<","
//                    <<AMA(DBConst::TABLE_user_balance)<<","
//                    <<AMA(DBConst::TABLE_user_logged)<<","
//                    <<AMA(DBConst::TABLE_user_cookie)<<")"
//                    <<"VALUES"<<"("
//                    <<QMA(username)<<","
//                    <<QMA(password)<<","
//                    <<"0"<<","
//                    <<"false"<<","
//                    <<"0"<<");";

//    insertStatement = conn->createStatement();

//    try
//    {
//        insertStatement->executeUpdate(SQLinsertcreator.str());
//    }
//    catch (SQLException& exception)
//    {
//        //it is possible the user accidentally tries to register with an existing profile name for whatever reason, the DB will then return
//        //a SQL error 1062, IE primary key already in use. This is not a fatal error, the exception should just be ignored.
//        if (exception.getErrorCode() != 1062)
//        {
//            cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//            abort();
//        }

//        //this gets executed if the error code is indeed 1062, IE the profile already exists.
//        success = false;
//    }

//    insertStatement->close();
//    delete insertStatement;
//    return success;
//}

//bool DBProfileManager::changeUsername(std::string newUsername,std::string oldUsername)
//{
//    if ( !checkValidity(newUsername) )
//        return false;

//    bool success = false;
//    sql::Statement* updateStatement;
//    stringstream SQLupdatecreator;

//    SQLupdatecreator<<"UPDATE "<<AMA(DBConst::TABLENAME_user)
//                    <<" SET "
//                    <<AMA(DBConst::TABLE_user_Username_PK)<<" = "<<QMA(newUsername)
//                    <<" WHERE "
//                    <<AMA(DBConst::TABLE_user_Username_PK)<<" = "<<QMA(oldUsername);

//    updateStatement = conn->createStatement();

//    try
//    {
//        int changeCount = updateStatement->executeUpdate(SQLupdatecreator.str());
//        if (changeCount == 1)//succesful change, set success to true.
//            success = true;
//        else if (changeCount > 1)//something went terribly wrong.
//        {
//            cout<<"Fatal error: RowCount mismatch. More results returned than is legal."<<endl;
//            abort();
//        }
//    }
//    catch (SQLException& exception)
//    {
//        //Like addProfile above, if the user tries to use a username that someone else is already using,
//        //then the server returns a 1062 and the API throws an exception. While the DB moans about it, we don't mind.
//        //Any other error codes are still fatal however.
//        if (exception.getErrorCode() != 1062)
//        {
//            cout<<"Failure to query database, SQL error code is: "<<exception.getErrorCode()<<endl;
//            abort();
//        }
//    }

//    updateStatement->close();
//    delete updateStatement;
//    return success;
//}

//void DBProfileManager::listProfiles()
//{
//    //not ideal, but the function must be implemented due to subtyping.
//}

//AMA = apostrophe mark Adder
//std::string DBProfileManager::AMA(const std::string target)
//{
//    stringstream copycreator;
//    copycreator<<"`"<<target<<"`";
//    return copycreator.str();
//}
