#include "fileprofilemanager.h"

#include "profilemanager.h"
#include "profile.h"
#include "protocolenums.h"

#include <fstream>

using namespace std;

FileProfileManager::FileProfileManager(std::map<short,playerRepresentation>& repmapsref) : ProfileManager(repmapsref)
{
    ifstream input;

    //aller første vi gjør er å sjekke om filen alt eksisterer
    input.open("profiles.sav");//local path

    if (!input.is_open() )
    {
        //da får vi opprette en.
        cout<<"profiles.sav does not exist, creating."<<endl;
        ofstream filecreator("profiles.sav");

        //I tilfelle fatal feil, avslutt hele servern.
        if (!filecreator.is_open() )
        {
            cout<<"fatal error: cannot create file"<<endl;
            abort();
        }
        filecreator.close();
    }
    else
    {
        //fil er nå åpen, nå laster vi innholdet over i interne buffer
        cout<<"profiles.sav detected, loading profiles."<<endl;
        while (!input.eof() )// != -1 )
        {
            string username;
            string password;
            string balance;

            input >> username;
            input >> password;
            input >> balance;

            if (username.size() != 0)
                profiles.push_back(profile(username,password,balance));
        }
        cout<<"loading complete, number of profiles: "<<profiles.size()<<endl;
    }

    //lukk input
    input.close();

}

void FileProfileManager::saveProfiles()
{
    //forstår ikke hva dette var, å skrive rett til file ser ikke ut til å fungere, man må bruke en egen outputstream, so be it.
    ofstream output("profiles.sav");

    //profil objektene har alle data om brukernavn og passord, men bankbalansene er utdatert. Vi må gå igjennom Reps,
    //finne tilsvarende profil, oppdatere dennes balance felt, før vi omsider skriver dette til disk.
    for(auto it = Repsmapref.begin();it != Repsmapref.end();it++)
    {
        for (auto& profile : profiles)
        {
            if (profile.getUsername() == it->second.getName() )//Reps som svarer til profil.
            {
                profile.setBalance( it->second.getBalance() );
                break;
            }
        }
    }

    //skriv så profilene til disk(overskriver innholdet)
    for(auto& profile : profiles)
    {
        output << profile.getUsername()<<endl;
        output << profile.getPassword()<<endl;
        output << profile.getBalance()<<endl;
    }

    //Til slutt lukkes output.
    output.close();
}

int FileProfileManager::loginAttempt(string username,string password)
{
    static uint id = 100;//unik innloggings id, gjenbrukes ikke;

    //løp igjennom profiles, returner true hvis vi finner riktig objekt, hvis ikke false;
    for(profile& pro : profiles)
    {
        //hvis brukernavn og passord matcher argumenter, returner true. Sjekk først om setLoggedIN returnerer true.
        if (pro.getUsername() == username)
        {
            if (pro.getPassword() == password)
            {
                bool result = pro.setLoggedIN(id);//er bruker alt logget inn? Hvis ikek skal idcookie settes.
                if (!result)//altså klient er allerede logget inn
                {
                    cout<<"invalid login attempt: peer attempted to login with already logged user("<<username<<")."<<endl;
                    return -1;
                }
                else
                {
                    cout<<"Login attempt of user ("<<username<<") was successful."<<endl;
                    return id++;
                }
            }
            else
                return -1;//vi skiter i om brukernavnet var riktig, en potensiel hacker skal ikke få vite at brukernavnet eksisterer.
        }
    }
    //hvis vi til nå ikke fant en match, finnes ikke profil objektet, returner false
    return -1;
}

string FileProfileManager::getUsername(uint cookieid)
{
    for(auto& profile : profiles)
    {
        if (profile.getCookieId() == cookieid)
            return profile.getUsername();
    }

    throw std::logic_error("Fatal error: Function \"FileProfileManager::getUsername(uint cookieid)\" could not find "
                           "user corresponding to cookie.");
    abort();
}

uint FileProfileManager::getBalance(uint cookieid)
{
    for(auto& profile : profiles)
    {
        if (profile.getCookieId() == cookieid)
            return profile.getBalance();
    }
    throw std::logic_error("function \"FileProfileManager::getBalance(uint cookieid)\" could not find "
                           "user corresponding to cookie.");;
    abort();
}

/*bool FileProfileManager::getUserLoggedState(uint cookieid)
{
    for(auto& profile : profiles)
    {
        if (profile.getCookieId() == cookieid)
            return profile.getLoggedin();
    }
    throw std::logic_error("function \"FileProfileManager::getUserLoggedState(uint cookieid)\" could not find "
                           "user corresponding to cookie.");;
    abort();
}*/

void FileProfileManager::logout(playerRepresentation& rep)
{
    //løper igjennom profiles på jakt etter profilen
    for (profile& pro : profiles)
    {
        if (pro.getCookieId() == rep.getCookieId())
        {
            bool result = pro.setLoggedOut();
            if (!result)//dvs ikke allerede logget inn, så er kommandoen ugyldig. Brukes til debugging
            {
                cout<<"invalid request, user("<<pro.getUsername()<<") is not already logged in."<<endl;
                return;
            }
            else
            {
                //profil må oppdateres
                pro.setBalance(rep.getBalance());

                cout<<"user("<<pro.getUsername()<<") logged out successfully."<<endl;
                return;
            }
        }
    }

    //hvis vi har kommet så langt, betyr det at profilen ikke eksisterer, med andre ord er det noe tull på gang, aborterer ikke.
    //Får se om det er fornuftig.
    cout<<"Error: Profile does not exist, ignoring request."<<endl;
    return;
}

bool FileProfileManager::addProfile(string username,string password)
{
    if ( !checkValidity(username) )
        return false;

    //løper igjennom profiles på jakt etter det samme brukernavnet.
    for(auto& profile : profiles)
    {
        if (profile.getUsername() == username)//ie finnes alt
        {
            return false;
        }
    }

    //hvis vi kom så langt, betyr det at brukernavnet er unikt.
    profiles.push_back(profile(username,password) );
    return true;
}

bool FileProfileManager::changeUsername(std::string newUsername,std::string oldUsername)
{
    if ( !checkValidity(newUsername) )
        return false;

    profile* oldUser;

    //først finner vi fram til det gamle brukernavnet
    for(auto& profile : profiles)
    {
        if (profile.getUsername() == oldUsername)//match
        {
            oldUser = &profile;
            break;
        }
    }

    //hvis vi ikke fant oldUser, så er noe helt fullstendig galt, aborter.
    if (oldUser == nullptr)
    {
        cout<<"Fatal erorr: Unable to change username "<<oldUsername<< "\" as no such username exists, aborting."<<endl;
        abort();
    }

    //nå må vi finne ut om det nye brukernavnet er unikt
    for (auto& profile : profiles)
    {
        if (profile.getUsername() == newUsername)//i så fall er det ugyldig
            return false;

    }

    //Hvis vi kom så langt, kan vi endre brukernavnet.
    oldUser->setUsername(newUsername);
    return true;
}

void FileProfileManager::listProfiles()
{
    for(auto& profile : profiles)
    {
        cout<<profile.getUsername()<<" "<<profile.getPassword()<<" "<<profile.getBalance()<<endl;
    }
}
