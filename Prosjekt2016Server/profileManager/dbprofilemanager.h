#ifndef DBPROFILEMANAGER_H
#define DBPROFILEMANAGER_H

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <map>

/*#include <include/cppconn/connection.h>
#include <include/cppconn/prepared_statement.h>
#include <include/cppconn/driver.h>
#include <include/cppconn/exception.h>
#include <include/cppconn/resultset.h>*/

//DBProfileManager extends ProfileManager and implements its abstract functions. Unlike FileProfileManager however, this one deals with a database in order to do it's work.
//class DBProfileManager : public ProfileManager
//{
//private:
//    std::string databaseIP;
//    std::string databasePort;
//    std::string databaseUsername;
//    std::string databasePassword;

//    sql::Driver* driver;
//    sql::Connection* conn;

//public:
//    DBProfileManager(std::map<short,playerRepresentation>& repmapsref, std::string DB_IP, std::string DB_PORT,
//        std::string DB_USER, std::string DB_PASS);
//    ~DBProfileManager();

//    int loginAttempt(std::string username, std::string password);
//    //profile* getProfile(uint cookieid);

//    std::string getUsername(uint cookie);
//    uint getBalance(uint cookieid);
//    //bool getUserLoggedState(uint cookieid);

//    void logout(playerRepresentation& rep);

//    void saveProfiles();

//    bool addProfile(std::string username,std::string password);
//    bool changeUsername(std::string newUsername,std::string oldUsername);

//    void listProfiles();

//private:
//    //adds two apostrophe/quotation marks to a copy of the target. Then returns that copy.
//    //convenience functions.
//    std::string AMA(const std::string target);
//    std::string QMA(const std::string target);
//};

#endif // DBPROFILEMANAGER_H
