#ifndef PROFILE_H
#define PROFILE_H

#include <string>

class profile
{
private:
    int idcookie; //-1 = N/A
    std::string usernameLocal;
    std::string passwordLocal;
    uint balanceLocal;
    bool loggedIn;

public:
    profile(std::string username,std::string password,std::string balance = "500");//instansierer et profil objekt.

    int getCookieId();

    std::string getUsername();
    std::string getPassword();
    uint getBalance();
    bool getLoggedin();

    void setUsername(std::string newUsername);
    void setPassword(std::string newPassword);
    void setBalance(uint newBalance);

    bool setLoggedOut();
    bool setLoggedIN(uint cookie);
};

#endif // PROFILE_H
