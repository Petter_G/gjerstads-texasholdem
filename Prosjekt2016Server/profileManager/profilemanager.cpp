#include "profilemanager.h"
#include "profile.h"
#include "protocolenums.h"



using namespace std;

ProfileManager::ProfileManager(std::map<short, playerRepresentation> &repmapsref) : Repsmapref(repmapsref)
{

}

//functions from here on are local only.
bool checkValidity(std::string text)
{
    //if size is longer than MAX_NAMESIZE, or shorter than MIM_NAMESIZE, return false straight away
    if ( text.size() > Misc::MAX_NAMELENGTH || text.size() < Misc::MIN_NAMELENGTH )
        return false;

    //Then there was the problem of illegal characters. Only english alphanumerics will be allowed.
    for (int i = 0; i < text.size();i++ )
    {
        if ( !( isalnum(text[i] )) )
        {
            return false;
        }
    }
    return true;
}

bool checkPasswordValidity(std::string password )
{
    //Check if password is of valid size.
    if ( !(password.size() < Misc::MIN_PASSWORDLENGTH) || !(password.size() > Misc::MAX_PASSWORDLENGTH) )
    {
        return false;
    }
    return true;
}
