#ifndef PROTOCOLENUMS
#define PROTOCOLENUMS

#include <string>
#include <thread>
#include <vector>
#include <mutex>

typedef unsigned int uint;
typedef unsigned short ushort;

//const QString resourceBase(":/images/"); //kun her av årsaker knyttet til konsekventhet. Brukes ikke i server

namespace ServVars
{
    //brukes av holdem og server for å kommunisere hvorvidt spillet er klar for å avslutte normalt.
    extern bool EXIT_CALLED;//brute force exit, brukes i servers mainprocessloop for å tvinge en avslutning når admin taster inn "exit"
    extern bool SYNCRONIZED_EXIT_CALLED;//sammen med ISREADY, brukes for å synkronisere avslutning av server og lagring av profil endringer.
}

namespace ServConst
{
    const int DEFAULT_PORT = 3500;
    const int AFTERGAMEWAITINGPERIOD = 5;
    const int MAXCONN = 1000;//grensen for POSIX select() er visst 1024, safern.
    const int MAXTABLES = 20;

    const std::string commandhelp = "Available commands:\nexit \t\t Terminates the program immediately.\n"
                            "start \t\t Initializes a socket in order to listen for incoming connections as well as initializing the storage functionality(database or file).\n"
                            "load \t\t Initializes the profileManager. The manager type used is dependant on the configuration set in config.cfg before runtime.\n"
                            "listen \t\t Sets the server up to listen for and accept incoming connections.\n"
                            "stop \t\t Stops the server, any connected clients are immediately disconnected, and games are forcibly shut down.\n"
                            "status \t\t Lists information about the current state of the server, if it's' running, listening port, that sort of thing.\n"
                            "set port N \t Sets the listening port to whatever is specified. N must be a number.\n"
                            "restart \t Restarts the server. Result is the same as if calling stop and then listen.\n"
                            "list repsdata \t Originally a debug command, left it in as it can be quite useful. Lists some metadata for all reps"
                                    "\n\t\t (player representation objects) currently connected to the server. \n"
                            "list gamedata \t Similar to the above, this command dumps metadata about any and all existing rooms on the server.\n"
                            "clear \t\t Clear the screen.\n"
                            "help \t\t Shows this message.";
    const int YES = 1;
}

namespace Misc
{
    const int MIN_PASSWORDLENGTH = 3;
    const int MAX_PASSWORDLENGTH = 20;
    const int MAX_MESSAGESIZE=2000;
    const int MIN_NAMELENGTH = 3;
    const int MAX_NAMELENGTH=20;
    const int NEWGUY = -1;
    const int SEGMENT_SIZE = 4; //regardless of the real size of the data being transmitted, the package will be in increments of 4(as of this writing) bytes.
                                //CALLED brukes i server, holdem::startgame sjekker hvorvidt variabelen er satt og handler deretter.
    //extern std::mutex DClock;//Brukes for å forhindre at flere DCtråder entrer kritisk sektor(dissconnectProtocol) samtidig.
}

namespace HoldemConst
{
    const int MAXSLOTS_AT_TABLE = 8;

    const int BigBlind = 20;
    const int SmallBlind = 10;
    const int raiseCap = 3;

    const bool DEBUG=true;
    const bool RELEASE=false;

    const int ROOMFULL = -1;
    const int FAULTYPSW = -2;
}

namespace DBConst
{
    const std::string defaultDB_IP = "NONE";
    const std::string defaultDB_PORT = "3306";
    const std::string defaultDB_USER = "admin";
    const std::string defaultDB_PASS = "password";

    const std::string Storage_Type_description = "Decides whether to use file storage or database storage. Valid values are \"DB\" and \"File\"";
    const std::string IP_description = "The IP used to connect to the database server. Has to be an IPv4 address.";
    const std::string PORT_description = "The port used by the database to accept connections.";
    const std::string DBUSER_description = "Username used by the game server to log into a database, only used if the storage type is DB";
    const std::string DBPASS_description = "Password used by the game server to log into a database, only used if the storage type is DB";
    const std::string autostart_description = "Decides whether the server automatically sets itself up to listen for connects from clients, and initilalize the profileManager. "
                                               "\"true\" is the default, while anything else will interpreted as false";
    //names of various entities and attributes used in our database follows from here.
    const std::string SCHEMANAME = "holdempokerdb";

    //table user
    const std::string TABLENAME_user = "user";
    const int TABLE_user_Username_LENGTH = 30;

    const std::string TABLE_user_Username_PK = "Username";
    const std::string TABLE_user_password = "password";
    const std::string TABLE_user_balance = "balance";
    const std::string TABLE_user_logged = "logged";
    const std::string TABLE_user_cookie = "cookie";
}

//0-99, kommandoer innkommende fra klient
enum SERVERCOMMANDS
{
    SETREADY = 10,
    CLEARREADY = 2,
    CHECK = 3,
    CALL = 4,
    FOLD = 5,
    RAISE = 6,
    ALLIN = 8,

    SETNAME=7,

    RESET=9,
    REFILL=11,       //opcode-amount //Brukes for å fylle opp escrow konto.
    TRANSFER=12,     //opcode-amount //Ber om en overføring av penger fra escrow til konto. I praksis blir ikke overføringen fullført før neste parti.

    LOGIN = 13,       //opcode-userlength-passlength-username-password
    REGISTER = 14,    //opcode-userlength-passlength-username-password
    JOIN = 15,        //opcode-gameid-usingPSW-pswCount-psw     //pawCount and psw are optional.
    CREATE = 16,         //opcode-pswprot-namecount-name-pswcount-psw //pswprot is a boolean. If true then pswcount and psw exists.
                                    //namecount and name is always present however.
    LEAVEROOM = 17      //opcode

};

//100-199, beskjeder fra server. Kommentarene viser hvordan pakkene ser ut(hver post er en int).
enum CLIENTENUMS
{
    INVALIDCOMMAND = 100, //opcode
    TABLESLOTNOTIFY = 101,//opcode-tableslot-trulyNew-local
                        //trulyNew is a bool that denotes if the player is new to the game, or just that particular client.
                        //Used exclusively to create the proper console area notes.
                        //Local denotes if this notification relates to the local player.
    PLAYERSNOTREADY = 102,//opcode
    VALIDCOMMAND = 103,//opcode
    NAMECHANGEDNOTIFY = 104,//opcode-serversidefd-silent-tableslot-messagelength-character(1-20) variabel lengde //messagelength inkluderer kun meldingen selv.
                            //silent brukes til å styre hvorvidt det skal gis en "is now known as" melding i konsol vinduer.
    TABLESLOTCHANGEDNAMENOTIFY =141,//opcode-tableslot-silent-namelength-character(1-20)//brukes for å fortelle ett roms pillere at en av dem har skiftet navn.
    REPAINT = 105,//opcode

    HANDUPDATE = 106,//opcode-tableslot-card1type-card1value-card2type-card2value
    DELETEREP = 107,//opcode-tableslot

    DUMPHANDUPDATE = 108,//opcode-tableslot
    BETUPDATE = 109,//opcode-tableslot-bet-totalbet
    BALANCEUPDATE = 110,//opcode-tableslot-balance
    ACTIVEUPDATE = 111,//opcode-tableslot-activestate
    UPDATEREADY = 112,//opcode-tableslot-activestate
    ALLINUPDATE = 123,//opcode-tableslot    //kalles aldri for å gjøre en allin tilstand false, ved communityreset skjer dette automatisk.

    NAMETOLONGNOTIFY = 113,//opcode
    TABLERESET = 114,//opcode
    COMMUNITYCARDADDED = 115,//opcode-card1type-card1value

    POTNOTIFY = 116, //opcode-potvalue
    CURRENTPLAYERNOTIFY = 117, //opcode-currentplayer
    CURRENTBUTTONNOTIFY = 118, //opcode-currentButton

    MINIMUMRAISEFAILURENOTIFY = 119, //opcode-bigblind
    PAUPERFAILURENOTIFY = 120, //opcode
    NOTYOURTURNNOTIFY = 121, //opcode
    CHECKINVALIDNOTIFY = 122, //opcode
    REJECTCONNECTIONNOTIFY = 124,//opcode
    GAMEHALTEDNOTIFY = 125,  //opcode
    GAMEPAUSEDNOTIFY = 128, //opcode

    WINNERINFONOTIFY = 126,  //opcode-winnertableslot-payout-potIndex-
    JOINSUCCESSNOTIFY = 127, //opcode
    JOINFAILURENOTIFY = 142, //opcode

    //AUTHENTICATEREQUIREDNOTIFY = 129, //opcode  //obsolete
    NAMECHANGEINVALIDNOTIFY = 130, //opcode
    AUTHENTICATIONFAILURENOTIFY = 131, //opcode
    AUTHENTICATIONSUCCESSNOTIFY = 132, //opcode-cookieid

    INVALIDPROFILECREATION = 133,    //opcode-exists
    VALIDPROFILECREATION =134,       //opcode
    SERVERSHUTDOWNNOTIFY = 135,         //opcode
    NEWGAMENOTIFY = 136,             //opcode-gameid-roomnameCount-roomname
    NEWPLAYERNOTIFY = 137,           //opcode-gameid-serversidefd-messagelength-character(1-20)
                                    //serverside fd brukes som ekstra data, slik at det er lettere å finne objektet hos klientene
    //CLIENTDISSCONNECTEDNOTIFY = 138,           //opcode-serversidefd-gameid
    PLAYERJOINEDROOMNOTIFY = 139,      //opcode-serversidefd-gameid
    PLAYERLEFTLOCALROOMNOTIFY = 140,      //opcode-tableslot//brukes når spiller trekker seg fra et lokalt rom.
    PLAYERSTATECHANGEDNOTIFY = 143,    //opcode-serversidefd-gameid-dcstate //Erstatter CLIENTDISCONNECTNOTIFY. Brukes mot samtlige klienter etter at
    //spiller har enten har koblet fra, eller forlatt et rom. DCstate = true betyr at klient har koblet fra server. gameid = -1 betyr at gameid ikke er relevant(lounger)
    MAXROOMSCREATEDNOTIFY = 144,            //opcode
    INVALIDROOMNAMELENGTHNOTIFY = 155,             //opcode
    INVALIDROOMNAMEEXISTSNOTIFY = 159,          //opcode
    INVALIDROOMPSWLENGTHNOTIFY = 156,              //opcode
    INVALIDROOMPSWNOTIFY = 157,              //opcode   //NOTE: signals that the password supplied when trying to join is wrong
    PSWPROTECTEDNOTIFY = 158                //opcode-gameid    //NOTE: Signals that the room the player is trying to join is password protected, IE try again with password.
};
//200-255 unused.
enum message
{
    SHORTCHAT  = 201, //opcode-character(1-2000)//variant brukt ved kommunikasjon sendt fra klient til server. Implementert for letthetens skyld
    CHAT = 200 //opcode-tableslot-fd-bytecount-character(1-2000) variabel lengde
};

enum cardValuesEnums
{
    ACELOW = 1,
    TWO = 2,
    THREE=3,
    FOUR=4,
    FIVE=5,
    SIX=6,
    SEVEN=7,
    EIGHT=8,
    NINE=9,
    TEN=10,
    JACK=11,
    QUEEN=12,
    KING=13,
    ACEHIGH = 14
};

enum cardTypesEnums
{
    CLUBS=0,
    DIAMONDS=1,
    HEARTS=2,
    SPADES=3
};

enum scoreValuesEnums
{
    ROYALFLUSH=10,
    STRAIGHTFLUSH=9,
    FOUROFAKIND=8,
    HOUSE=7,
    FLUSH=6,
    STRAIGHT=5,
    THREEOFAKIND=4,
    TWOPAIR=3,
    PAIR=2,
    HIGHCARD=1
};

enum BlockEuphemisms
{
    ONE_BLOCK = 1,
    TWO_BLOCKS = 2,
    THREE_BLOCKS = 3,
    FOUR_BLOCKS = 4,
    FIVE_BLOCKS = 5,
    SIX_BLOCKS = 6
};

#endif // PROTOCOLENUMS
