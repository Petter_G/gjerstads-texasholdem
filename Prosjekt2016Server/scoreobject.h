#ifndef SCOREOBJECT_H
#define SCOREOBJECT_H

#include "playerrepresentation.h"

//selvstendig objekt brukt av determineWinners i EvalEngine.
class ScoreObject
{
    short primary;
    short secondary;
    short tertiary;
    short quaternary;

    playerRepresentation& ref;          //referanse til rep objektet scoreObjekt beskriver.

public:
    ScoreObject(playerRepresentation& repPtr);

    void setPrimary(short val);
    void setSecondary(short val);
    void setTertiary(short val);
    void setQuaternary(short val);

    short getPrimary();
    short getSecondary();
    short getTertiary();
    short getQuaternary();

    playerRepresentation& getRepPtr();
};

#endif // SCOREOBJECT_H
