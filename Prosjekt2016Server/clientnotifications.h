#ifndef CLIENTNOTIFICATIONS_H
#define CLIENTNOTIFICATIONS_H

#include "server.h"
#include "playerrepresentation.h"
#include "scoreobject.h"

namespace ClientNotifications
{
    //funksjoner knyttet til oppdatering av klienter.
    //_BCAll betyr at alle samtlige klienters data blir sendt til samlige klienter, eller hvis det gjelder bordets data som f.eks communitycard
    //_BCSingle derimot betyr at en spesifik klients data blir sendt til alle, inkludert klienten selv.
    //_HALL betyr at hver enkelts data IKKE kringkastes til alle, men bare til hver enkelt
    //_BC betyr at det er en ren data sending, ingen informasjon om noen spillere.
    //BC=BroadCast,H=Hidden,SC=SingleCast
    void NotifyTableReset_BC(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                                    //brukes for å gjøre klientene oppmerksomme på at bordet er resatt(ny runde)
    void NotifyBalanceChanged_BCSingle(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, int tableslot);           //gjør alle klienter oppmerksomme på en enkelt spillers bankbalanse.
    void NotifyBalanceChanged_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                             //Gjør alle klienter oppmerksomme på alles bankbalanser.
    void NotifyBetChanged_BCSingle(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, uint tableslot);              //gjør alle klienter oppmerksomme på en enkelt spillers innsats. Dette gjelder og totalBet som sendes samtidig som egen opcode
    void NotifyBetChanged_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                                 //gjør alle klienter oppmerksomme på hverandres innsatser. Dette gjelder og totalBet som sendes samtidig som egen opcode
    void NotifyHandSet_HAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                                     //gjør alle spillere oppmerksom på at deres individuelle hender har blitt satt. Dvs, motspilleres hender forblir usynnlige
    void NotifyHandSet_BCALL(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                                    //gjør alle oppmerksomme på alle andres kort.
    void NotifyExistingCommunityCards_BCSingle(Server &serverRef, int fd, std::vector<card> &community);                            //gjør en spesifik klient oppmerksom på samtlige eksisterende community kort.
    void NotifyCommunityCardAdded_BC(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, card &addedCard);           //Sender oppdatering til alle klienter om nytt community kort.
    void NotifyPot_BC(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, int pot);                                  //gjør alle oppmerksomme på hva potten er.
    void NotifyCurrentPlayer_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, short currentPlayerSlot);     //tilsvarende for currentplayer.
    void NotifyPlayerFolded_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, uint tableslot);               //Gjør alle oppmerksom på at en spiller kastet kortene
    void NotifyPlayerLeftLocalRoom_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, uint tableslot);        //gjør alle(i det lokale rommet) oppmerksom på at spilleren har forlatt spillet.
    void NotifyButton_BC(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, short currentButtonSlot);               //tilsvarende NotifyCurrentPlayer for button.
    void NotifyAllIn_BCSingle(Server &serverRef, std::map<ushort, playerRepresentation *> &reps,uint tableslot);                    //Gjør alle oppmerksomme på en spillers status som allin.
    void NotifyHaltState_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                                  //forklarer alle at spillet ikke kan fortsette
    void NotifyWinnerDataAdded_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps, ScoreObject* score, int payout, int potIndex);  //Sender resolusjonsinfo til klientene.
    void NotifyPaintUI_BCAll(Server &serverRef, std::map<ushort, playerRepresentation *> &reps);                                    //gir beskjed til klientene om å tegne grensesnittet på nytt.
};

#endif // CLIENTNOTIFICATIONS_H
