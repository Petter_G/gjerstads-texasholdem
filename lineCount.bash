#! /bin/bash

local="$HOME/Dropbox/GHoldemCPP"

serverHcount=$(find "./Prosjekt2016Server" -name  '*.h' | grep -v 'moc'| grep -v 'qrc'| xargs wc -l | awk 'NR==0; END{print}'| awk '{print $1}')
serverCPPcount=$(find "./Prosjekt2016Server" -name '*.cpp' | grep -v 'moc'| grep -v 'qrc'| xargs wc -l | awk 'NR==0; END{print}' | awk '{print $1}')

#echo "server .h count: $serverHcount"
#echo "server .cpp count: $serverCPPcount"

serverSum=$(( serverHcount + serverCPPcount ))
echo "Combined server lines: $serverSum"

