# Welcome to Gjerstads Texas Holdem

## What is it?

Gjerstads Texas Holdem is a Texas holdem poker game that consists of two parts: A server and a client. They communicate using standard socket protocols on the TCP layer, and a custom protocol on the application layer. Multiple clients can connect and disconnect without causing the server to crash, indeed the clients can disconnect or leave a table without causing the game's internal logic to go haywire(as you'd expect). 

The client portion has a GUI element, handwritten using the Qt framework, where as the server only uses the standard template library and is console(IE text) only. The server uses primitive threading in order to remain responsive when individual rooms are paused.

Players have to create profiles before logging in. These profiles store the username,password and bank balances of that player, and they are stored either locally in the form of the profiles.sav file, or externally in the form of a mysql database that can, if one wants, run on a completely different host. As of this writing both storage methods are supported. Since the database storage functionality was added later and in sync with the old functionality, it is extremely primitive as databases go.

Also see my ongoing java port of the client part over at: https://bitbucket.org/Petter_G/gjerstads-texas-holdem-java-client

## Why?
The project began after getting a horrendous D after taking a "effective c and c++" course at HiOA. The original plan was to take the course again, however the idea of going back to collage was a difficult one. Instead the project grew and grew, eventually outgrowing the original, failed, project by leaps and bounds.

## Revision history and patching:
As the project was and is developed privately, the project wasn't always hosted on gitbucket. As a consequence, the revision history is incomplete. Major changes made will be documented in the project log file, unoriginally named projectlog.md. The versioning scheme is as follows: Most significant digit denoting major revision(currently we're still at 0), while the second most significant one tracks additions, while the least most significant one tracks small changes such as minor bug fixes or just refactoring. Also note that the the more significant digits reset the less significant digits. For instance if we increment the second most significant digit from 1 to 2, then the least significant digit becomes 0 again, irrespective of what it was previously.

## Lack of design documentation:
You may wonder why there are no design documents, well I considered it impractical for the following reasons: 

- The project was developed by one man. The need for a design document as a means of communication didn't exist.
- The experience needed wasn't there, IE I couldn't structure my program in any meaningful way, I didn't know how to use the QT library yet. One could argue I could've made design documents for the GUI portion, but again, the project consisted of one man, no communication was necessary.
- Originally the project wasn't intended to be this big, another reason why I considered documentation impractical. Now that the project has become so big(about 12000 lines as of this writing), I may have to create some none the less.

When that's said however, I did make a objectives and priorities list, detailed in the ProsjektPlan.txt. As of this writing the plan is mostly in my native norwegian, the newer objectives are in english however. One of these days I'll have to get down to it and actually do the translation. Then again, anyone who reads this is likely to be fluent
in both norwegian and english.

Admittedly, the projectplan is arguably obsolete at this point, made so by the issue tracker provided by bitbucket. 

## Licensing?
This software is provided as is, I make no copyright claims of any kind. Do note that this software uses libraries provided by others, such as the mysql c++ connector. 

## Installation instructions:
In order to use the program, I am happy to say that I've implemented support for Unix(tested with ubuntu 14.04) and windows platforms. Now, I hear that binary compatibility between linux distros isn't guaranteed, so if the binaries won't work, you'll have to recompile the whole mess, and that is outside the scope of this document.

Assuming compatibility isn't a problem, all you need to do is to download the release folder, and then launch either G_holdem_serv if on linux, or G_Holdem_Serv.exe if on windows. The server shouldn't require any configuration by default. Once started it will attempt to read the profiles.sav and config.cfg files if present, and if not create them with default values. 

Bear in mind that the windows version is somewhat limited as the database support, while implemented, is untested. In theory, it should work out just fine. While setting one up is outside the scope of this document, do n

So what about the client? It is sadly obsolete now that my Java client is available. Please drop by https://bitbucket.org/Petter_G/gjerstads-texas-holdem-java-client for a up-to-date client to use. Using it is explained there. Unlike the server, it should run on pretty much any platform invented by man.

## So how do you actually play?
Click the link at the buttom, follow instructions over there.

## Future plans:

Many of the plans that used to be detailed in this document have been completed by the time of this writing, however a long term target that remians is to replace the current event handling in Server with a proper active object pattern. Details can be found under "Proper server-side event handling" in the project plan.

I've also decided that all plans and commentaries be translated into english. I originally wrote the project plan in norwegian because the project was intended to be turned in as a school project. Since this project has been made publically available, it's seems reasonable that the project is fully translated. When I'll actually get around to it is another matter entirely, it's pretty low on my list due the obscenely unlikely scenario that someone who doesn't also read norwegian drops by.

As of this writing though the most pressing concern is probably to finish the windows port. The current version lacks database(ie DBProfileManager) support and console input has been disabled due to incompatibility between the windows and Unix worlds.

That said though, I do consider the project in a relatively complete state, think Steam style "early access", just free. I may add new options when I feel like it, but I make no guarantees.

## Critique:

Should you be interested in reviewing my code and critique it, by all means do so. One downside of being on a "one man development team" is a sort of tunnel vision that may inhibit your ability to see flaws in your own code and design. I can be reached at petter_hvit2@hotmail.com.

##Java client:
As noted previously, I no longer support the Qt based client, which has become deprecated by now. Instead take a look at the swing based Java client over here: https://bitbucket.org/Petter_G/gjerstads-texas-holdem-java-client

If you'd still want to take a look at the Qt client, you can find it over at: https://bitbucket.org/Petter_G/gjerstads-texas-holdem-qt-client 

Again, that client is deprecated, it will not work as intended with the current version of the server.
